import os

# Source
#   App
#      Graphics
#   Logic

# for root, dirs, files in os.walk('../Source/'):
#     for file in files:
#         bad_name = os.path.join(root, file)
#         fixed_name = bad_name.replace('\\', '/')

#         os.path.relpath()
#         print(fixed_name)


# Open the file
# Walk through it line-by-line
# Throw out all commented code (TODO: /* */)
# Look for ExposeToLua
# Parse the function below

class Argument:
    def __init__(self, raw_string):
        space_index = raw_string.rfind(' ')
        self.type = raw_string[:space_index]
        self.name = raw_string[space_index + 1:]

class Function:
    def __init__(self, raw_string):
        self.name = None
        self.args = list()
        self.return_type = None
        self.raw_args = ''

        self.parse(raw_string)

    def parse(self, raw_string):
        # Function name (TODO: Handle stupid stuff like std::function<void()>)
        open_paren_index = raw_string.find('(')
        space_before_name_index = raw_string.rfind(' ', 0, open_paren_index)
        self.name = raw_string[space_before_name_index + 1:open_paren_index]
        # print("Parsed Function Name: {0}".format(self.name))

        # Return type
        self.return_type = raw_string[:space_before_name_index]

        # Arguments
        close_paren_index = raw_string.rfind(')')
        if open_paren_index + 1 == close_paren_index:
            # No arguments
            return
        
        # Get the arguments sub-string
        arg_string = raw_string[open_paren_index + 1:close_paren_index]
        self.raw_args = arg_string

        first_comma_index = raw_string.find(',')
        if first_comma_index != -1:
            # 2+ arguments
            args = arg_string.split(',')
            for arg in args:
                arg = arg.strip(' \r\n\t')
                self.args.append(Argument(arg))
            return
        
        # Otherwise, we have a single argument
        self.args.append(Argument(arg_string))


class Class:
    def __init__(self, name):
        self.name = name
        self.functions = list()
    
    def add_function(self, function):
        self.functions.append(function)
    
    def __str__(self):
        result = 'Class: ' + self.name + '\n'
        for func in self.functions:
            result += '\tFunction: ' + func.name + '\n'
            for arg in func.args:
                result += '\t\tArg: ({0}) {1}\n'.format(arg.type, arg.name)
        
        return result
    
    def generate_header(self):
        header = open(self.name + '.gen.h', 'w')

        header.write('#pragma once\n\n')
        header.write('namespace Lua\n')
        header.write('{\n')
        
        for func in self.functions:
            header.write('\tint {0}_{1}(lua_State* L);\n'.format(self.name, func.name))
        
        header.write('}')

        header.close()
    
    def generate_source(self):
        source = open(self.name + '.gen.cpp', 'w')

        source.write('#include "{0}.gen.h"\n\n'.format(self.name))

        for func in self.functions:
            source.write('int Lua::{0}_{1}(lua_State* L)\n'.format(self.name, func.name))
            
            source.write('{\n')

            # +1 is for the 'this' pointer
            table_index = len(func.args) + 1

            # Get the 'this' pointer and cast it correctly
            source.write('\t{0}* p{0} = static_cast<{0}*>(lua_touserdata(L, {1}));\n'.format(self.name, -table_index))
            table_index -= 1

            # Get all arguments and cast them correctly

            # TODO: Don't declare this in a tight loop, like a n00b
            lua_conversion_table = {
                'u32' : 'lua_tointeger',
                'const char*' : 'lua_tostring',
                'const std::string&' : 'lua_tostring'
            }

            for arg in func.args:
                conversion_func = None
                if arg.type in lua_conversion_table.keys():
                    conversion_func = lua_conversion_table[arg.type]
                else:
                    conversion_func = 'lua_touserdata'
                
                source.write('\t{0} _{1} = static_cast<{0}>({2}(L, {3}));\n'.format(arg.type, arg.name, conversion_func, -table_index))
                table_index -= 1

            source.write('\n\tlua_pop(L, {0});\n\n'.format(len(func.args) + 1))

            # Call the member function
            source.write('\tp{0}->{1}('.format(self.name, func.name))
            
            arg_count = 0
            for arg in func.args:
                source.write('_{0}'.format(arg.name))
                if arg_count < len(func.args) - 1:
                    source.write(', ')
                arg_count += 1
            
            source.write(');\n')
            source.write('\treturn 0;\n}\n\n')


            # TODO: Return values?

        source.close()


header = open('Test.h')
my_class = None
found_identifier = False

lines = header.readlines()
for line in lines:

    # Strip whitespace
    line = line.strip(' \r\n\t')

    # Continue on empty lines
    if line == '':
        continue
    
    # Ignore Line comments
    if line[0] == '/' and line[1] == '/':
        continue

    # Throw away comments appended to lines of code
    comment_index = line.find('//')
    if comment_index != -1:
        line = line[0:comment_index]
    
    # Parse this line (it's a function)
    if found_identifier:
        found_identifier = False

        # This does the heavy lifting for our parsing
        my_function = Function(line)

        my_class.add_function(my_function)
        continue
    
    # Get the class name
    if 'class' in line and ';' not in line:
        # [startIndex (0) : endIndex (len - 1)]
        class_name = line[line.find(' ') + 1:]
        my_class = Class(class_name)
        print('Found class: {0}'.format(class_name))
        continue
    
    # Search for Identifier
    if 'ExposeToLua' in line:
        found_identifier = True
        continue


header.close()

# Do work
print(my_class)
my_class.generate_header()
my_class.generate_source()