REM Build script for Croak Engine

REM Get access to VS environment
CALL "C:\Program Files (x86)\Microsoft Visual Studio\2019\Enterprise\VC\Auxiliary\Build\vcvars32.bat"

REM Build the solution with a target platform and configuration
msbuild "..\..\Croak.sln" /p:Platform="x86" /p:Configuration="Release" /m

REM Make sure everything went well
if %ERRORLEVEL% NEQ 0 (
    ECHO Build Failure!
    EXIT /B 1
)

REM TODO: Unit Tests Go Here

REM TODO: Package build