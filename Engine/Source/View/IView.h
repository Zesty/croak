#pragma once
/** \file IView.h
 * Base interface for all views
 * A view is meant to be an actor that can query the state of the world and make decisions
 */
// Created by Billy Graban

#include <Logic/Actor/Actor.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class ApplicationLayer;

/** \class IView
 * @brief Base interface for all views
 * A view is meant to be an actor that can query the state of the world and make decisions
 */
class IView
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	IView();

	/** Default Destructor */
	virtual ~IView();

	/** Pure virtual function that initializes the view using data from the Application Layer
	 * @param pApp The Application Layer
	 */
	virtual bool Init(ApplicationLayer* pApp) = 0;

	/** Pure virtual function that allows the actor to update it's input using whatever mechanism desired */
	virtual void UpdateInput() = 0;

	/** Used to draw the scene for any human views that want it */
	virtual void ViewScene() = 0;
	
	/** Sets the actor that the view is associated with
	 * @param pActor the actor to associate
	 */
	void SetControlledActor(std::shared_ptr<Actor> pActor);

	/** Breaks the connection between the actor and view */
	void DetachActor();

protected:
	// --------------------------------------------------------------------- //
	// Protected Member Variables
	// --------------------------------------------------------------------- //
	std::shared_ptr<Actor> m_pActor;	/**< The actor (if any) that is associated with this view */

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}

/** \namespace Lua Contains all global C functions that connect the Croak Engine to Lua */
namespace Lua
{
	/** Lua binding for IView::SetControlledActor()
	 * @param L The lua state
	 * @return The number of arguments pushed onto the lua stack
	 * @see crk::IView::SetControlledActor()
	 */
	int View_SetControlledActor(lua_State* L);
}