#include "IView.h"

#include <Lua/lua.hpp>

#include <Logic/Actor/Actor.h>
#include <Logic/IGameLayer.h>

using crk::IView;

IView::IView()
{
	
}

IView::~IView()
{
	
}
void IView::SetControlledActor(std::shared_ptr<crk::Actor> pActor)
{
	pActor->SetOwningView(this);
	m_pActor = pActor;
}

void IView::DetachActor()
{
	m_pActor = nullptr;
}

int Lua::View_SetControlledActor(lua_State* L)
{
	IView* pView = static_cast<IView*>(lua_touserdata(L, -2));
	crk::Actor* pActor = static_cast<crk::Actor*>(lua_touserdata(L, -1));

	lua_getglobal(L, "GameLayer");
	crk::IGameLayer* pGame = static_cast<crk::IGameLayer*>(lua_touserdata(L, -1));

	lua_pop(L, 3);

	pView->SetControlledActor(pGame->GetActorById(pActor->GetId()));
	return 0;
}