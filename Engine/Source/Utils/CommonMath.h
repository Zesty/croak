#pragma once
/** \file CommonMath.h
 * Contains common math structures 
 */
// Created by Billy Graban

#include "Typedefs.h"

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \struct FRect
 * @brief Rectangle (x, y, w, h) stored in floats 
 */
struct FRect
{
	f32 x;			/**< Left-most position of the rectangle */
	f32 y;			/**< Top-most position of the rectangle */
	f32 width;		/**< Width of rectangle */
	f32 height;		/**< Height of rectangle */

	/** Default constructor */
	FRect();

	/** Constructor
	 * @param x Left-most position of the rectangle
	 * @param y Top-most position of the rectangle
	 * @param width Width of rectangle
	 * @param height Height of rectangle
	 */
	FRect(f32 x, f32 y, f32 width, f32 height);
};

/** \struct IRect
 * @brief Rectangle (x, y, w, h) stored in integers 
 */
struct IRect
{
	i32 x;			/**< Left-most position of the rectangle */
	i32 y;			/**< Top-most position of the rectangle */
	i32 width;		/**< Width of rectangle */
	i32 height;		/**< Height of rectangle */

	/** Default constructor */
	IRect();

	/** Constructor
	 * @param x Left-most position of the rectangle
	 * @param y Top-most position of the rectangle
	 * @param width Width of rectangle
	 * @param height Height of rectangle
	 */
	IRect(i32 x, i32 y, i32 width, i32 height);
};

/** /struct FColor
 * @brief Stores red, green, blue, and alpha channels (values from 0 to 1) 
 */
struct FColor
{
	f32 r;		/**< Red channel */
	f32 g;		/**< Green channel */
	f32 b;		/**< Blue channel */
	f32 a;		/**< Alpha channel */

	/** Default constructor
	 * Uninitialized colors will be magenta
	 */
	FColor();

	/** Constructor
	 * @param r Red channel (values from 0 to 1)
	 * @param g Green channel (values from 0 to 1)
	 * @param b Blue channel (values from 0 to 1)
	 * @param a Alpha channel (values from 0 to 1)
	 */
	FColor(f32 r, f32 g, f32 b, f32 a);

	/** Interpolates between two colors 
	 * The interpolation is done a per-channel basis
	 * @param a Start color
	 * @param b End color
	 * @param t Value from 0 to 1, with 0 representing a result of color 'a', and 1 representing a result of color 'b'
	 */
	static FColor Lerp(const FColor& a, const FColor& b, f32 t);
};

/** \struct FVec2
 * @brief Stores a point with floats 
 */
struct FVec2
{
	f32 x;										/**< The x position */
	f32 y;										/**< The y position */

	/** Default constructor
	 * Values are set to 0
	 */
	FVec2();

	/** Constructor
	 * @param x The x position
	 * @param y The y position
	 */
	FVec2(f32 x, f32 y);

	FVec2 operator+(const FVec2& other);		/**< Adds the components of the vectors together */
	FVec2& operator+=(const FVec2& other);		/**< Adds the components of 'other' to the calling object */
	FVec2 operator*(float scalar);				/**< Multiply all components by a scalar value */
};

/** \struct IVec2
 * @brief Stores a point with integers
 */
struct IVec2
{
	i32 x;			/**< The x position */
	i32 y;			/**< The y position */

	/** Default constructor
	 * Values are set to 0
	 */
	IVec2();

	/** Constructor
	 * @param x The x position
	 * @param y The y position
	 */
	IVec2(i32 x, i32 y);
};

/** Linearly interpolate between two floats
 * @param a The starting value
 * @param b The ending value
 * @param t The ratio between a and b.  This value should be between 1 and 0, with 0 resulting in a value of the float 'a'
 * and 1 resulting in a value of the float 'b'
 */
f32 Lerp(f32 a, f32 b, f32 t);
}