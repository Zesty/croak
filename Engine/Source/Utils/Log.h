#pragma once
/** \file Log.h
 * Logs colored messages to a console, and write messages to a log file.  Used for debugging real time
 * issues
 */
// Created by Billy Graban
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <chrono>

#include <Application/OS/IOpSys.h>
#include <Lua/lua.hpp>


// Don't log in release mode
#ifdef _ENGINE_SHIPPING
	#define LOG(...) ]
	#define LOG_SIMPLE(...)
#else
	/** \def LOG(category, format, ...) Adds file and line number to Log::Print automatically 
	 * @param category The category to use. @see crk::Log::Category
	 * @param format The sprintf formatted string to use when building the log
	 * @param ... The arguments to be supplied to sprintf
	 */
	#define LOG(category, format, ...) \
		crk::Log::Get()->Print(crk::Log::Category::category, __LINE__, __FILE__, format, __VA_ARGS__);

	 /** \def LOG_SIMPLE(format, ...) Calls PrintSimple from the log singleton
	  * @param format The sprintf formatted string to use when building the log
	  * @param ... The arguments to be supplied to sprintf
	  */
	#define LOG_SIMPLE(format, ...) \
		crk::Log::Get()->PrintSimple(format, __VA_ARGS__);
#endif



//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class Log
 * @brief Logs colored messages to a console, and write messages to a log file
 * Used for debugging real time issues
 */
class Log
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //

	/** \enum Category Types of log categories */
	enum class Category
	{
		Info,		/**< Simple information */
		Boot,		/**< Results of operations during booting */
		Resources,	/**< Loading or releasing resources */
		Todo,		/**< Reminder to modify code later (NOTE: Only is logged once) */
		Warning,	/**< Be careful, something might be wrong */
		Error,		/**< Something has failed */
		Critical,	/**< The engine cannot recover from a failure */
		Lua,		/**< Logs related to lua */
	};

	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //

	/** Return the singleton logging object
	 * @return Pointer to the singleton
	 */
	static Log* Get();

	/** Print data of a specific category to both the console and the output file.
	 * This will also color the output of the console based on the category
	 *
	 * @param category Which category to use, @see crk::Log::Category
	 * @param line The line (supplied by a macro) of where the log was called
	 * @param file The file (supplied by a maceo) of which file called the log
	 * @param format The sprintf format to use when printing the log
	 * @param ... Extra arguments needed by sprintf
	 */
	void Print(Category category, int line, const char* file, const char* format, ...);

	/** Prints data to the console and output file
	 * Only used at the very beginning before the OS has been initialized
	 *
	 * @param format The sprintf format to use when printing the log
	 * @param ... Extra arguments needed by sprintf
	 */
	void PrintSimple(const char* format, ...);

	/** Initialize the logger and open the target output file
	 * @param pOpSys Operating System interface to use
	 * @param outputFile Where the log will be written to on the hard disk
	 */
	bool Init(IOpSys* pOpSys, const char* outputFile);

	/** Release all resources used by the logger.  Must be called manually */
	void Cleanup();

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	bool m_initialized;					/**< True if initialized successfully */
	std::ofstream m_outFile;			/**< The file stream to write out to */
	std::stringstream m_stringStream;	/**< Used to build the string before writing it */
	IOpSys* m_pOpSys;					/**< Used to change colors of the console depending on category */
	char m_messageBuffer[512];			/**< Reusable buffer the full log message */

	// Member strings used often during logging
	std::string m_filename;			/**< Full path of the file that requested the log action */
	std::string m_shortFileName;	/**< Truncated file name */
	std::string m_lineId;			/**< Concatenation of m_shortFileName and line.  Allows Todo logs to only be displayed once */

	/** Any log with the Todo category is only logged once */
	std::unordered_set<std::string> m_todoSet;

	/** Map of category names for easy printing */
	static std::unordered_map<Log::Category, std::string> s_categoryNames;

	/** Map of category/color associations */
	static std::unordered_map<Log::Category, IOpSys::TextColor> s_categoryColorMap;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	Log();

	/** Default Destructor */
	~Log();

	// TODO: Disable other singleton functions


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}

/** \namespace Lua Contains all global C functions that connect the Croak Engine to Lua */
namespace Lua
{
	/** Lua binding for Log::Print()
	 * @param L The lua state
	 * @return The number of arguments pushed onto the lua stack
	 * @see crk::Log::Print()
	 */
	int Log_Log(lua_State* L);  // 'Glue function'
}