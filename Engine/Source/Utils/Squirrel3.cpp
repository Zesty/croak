#include "Squirrel3.h"

using crk::Squirrel3;

Squirrel3::Squirrel3(u32 seed)
	: m_seed(seed)
	, m_position(0)
{
}

Squirrel3::~Squirrel3()
{
}

void Squirrel3::ResetSeed(u32 seed, i32 position)
{
	m_seed = seed;
	m_position = position;
}

u32 Squirrel3::Get1DNoiseUint(i32 positionX, u32 seed)
{
	const u32 BIT_NOISE1 = 0x68E31DA4;
	const u32 BIT_NOISE2 = 0xB5297A4D;
	const u32 BIT_NOISE3 = 0x1B56C4E9;

	u32 mangledBits = (u32)positionX;
	mangledBits *= BIT_NOISE1;
	mangledBits += seed;
	mangledBits ^= (mangledBits >> 8);
	mangledBits *= BIT_NOISE2;
	mangledBits ^= (mangledBits << 8);
	mangledBits *= BIT_NOISE3;
	mangledBits ^= (mangledBits >> 8);
	return mangledBits;
}

u32 Squirrel3::RollRandomUint32()
{
	return Get1DNoiseUint(m_position++, m_seed);
}

u16 Squirrel3::RollRandomUint16()
{
	return (u16)Get1DNoiseUint(m_position++, m_seed);
}

u8 Squirrel3::RollRandomByte()
{
	return (u8)Get1DNoiseUint(m_position++, m_seed);
}

u32 Squirrel3::RollRandomLessThan(u32 maxValueNotInclusive)
{
	return Get1DNoiseUint(m_position++, m_seed) % maxValueNotInclusive;
}

i32 Squirrel3::RollRandomIntInRange(i32 minValueInclusive, i32 maxValueInclusive)
{
	u32 result = Get1DNoiseUint(m_position++, m_seed);
	result %= maxValueInclusive - minValueInclusive + 1;
	return (i32)result + minValueInclusive;
}

float Squirrel3::RollRandomFloatZeroToOne()
{
	return (float)Get1DNoiseUint(m_position++, m_seed) / (float)UINT32_MAX;
}

float Squirrel3::RollRandomFloatInRange(float minValueInclusive, float maxValueInclusive)
{
	float result = RollRandomFloatZeroToOne();
	result = result * (maxValueInclusive - minValueInclusive);
	result += minValueInclusive;
	return result;
}

float Squirrel3::RollRandomFloatNegativeOneToOne()
{
	return RollRandomFloatInRange(-1.0f, 1.0f);
}

bool Squirrel3::RollRandomChance(float probabilityOfReturningTrue)
{
	float result = RollRandomFloatZeroToOne();
	return (result <= probabilityOfReturningTrue);
}

crk::FVec2 Squirrel3::RollRandomDirection2D()
{
	FVec2 result;
	result.x = RollRandomFloatNegativeOneToOne();
	result.y = RollRandomFloatNegativeOneToOne();
	return result;
}
