#include "XmlLoaderUtil.h"

#include <string>

using crk::XmlLoaderUtil;

crk::Squirrel3* XmlLoaderUtil::s_FactoryRng = nullptr;

f32 crk::XmlLoaderUtil::ReadFloatValue(tinyxml2::XMLElement* pData, const char* name)
{
	using namespace tinyxml2;
	XMLElement* pElement = pData->FirstChildElement(name);
	XMLElement* pRandom = pElement->FirstChildElement("Random");

	if (!pRandom)
	{
		const char* text = pElement->GetText();
		return std::stof(text);
	}

	f32 min = pRandom->FloatAttribute("min");
	f32 max = pRandom->FloatAttribute("max");
	f32 negativeChance = pRandom->FloatAttribute("negativeChance");

	f32 result = s_FactoryRng->RollRandomFloatInRange(min, max);
	result *= s_FactoryRng->RollRandomChance(negativeChance) ? -1 : 1;

	return result;
}
