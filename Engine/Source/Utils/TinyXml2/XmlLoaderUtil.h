#pragma once
/** \file XmlLoaderUtil.h
 * Minor helper functions for parsing XML elements with randomized values 
 */
// Created by Billy Graban

#include "tinyxml2.h"

#include <Utils/Typedefs.h>
#include <Utils/Squirrel3.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class XmlLoaderUtil
 * @brief Minor helper functions for parsing XML elements with randomized values 
 */
class XmlLoaderUtil
{
public:
	/** The RNG used by the ActorFactory */
	static Squirrel3* s_FactoryRng;

	/** Return the value inside the element or randomize the value based on attributes
	 * @param pData The element to read from
	 * @param name The name of the element to search for
	 * @return The value of the float (either randomized or not, depending on the XML structure
	 */
	static f32 ReadFloatValue(tinyxml2::XMLElement* pData, const char* name);
};
}