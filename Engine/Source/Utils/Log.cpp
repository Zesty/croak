#include "Log.h"

#include <iostream>
#include <ctime>
#include <iomanip>
#include <stdarg.h>

using crk::Log;

std::unordered_map<Log::Category, std::string> Log::s_categoryNames =
{
	{ Log::Category::Info, "Info" },
	{ Log::Category::Boot, "Boot" },
	{ Log::Category::Todo, "Todo" },
	{ Log::Category::Resources, "Resources" },
	{ Log::Category::Warning, "Warning" },
	{ Log::Category::Error, "Error" },
	{ Log::Category::Lua, "Lua" },
	{ Log::Category::Critical, "Critical" },
};

std::unordered_map<Log::Category, crk::IOpSys::TextColor> Log::s_categoryColorMap =
{
	{ Log::Category::Info, crk::IOpSys::TextColor::White },
	{ Log::Category::Boot, crk::IOpSys::TextColor::Blue },
	{ Log::Category::Todo, crk::IOpSys::TextColor::Green },
	{ Log::Category::Resources, crk::IOpSys::TextColor::Purple},
	{ Log::Category::Warning, crk::IOpSys::TextColor::Yellow },
	{ Log::Category::Error, crk::IOpSys::TextColor::Red },
	{ Log::Category::Critical, crk::IOpSys::TextColor::Critical },
	{ Log::Category::Lua, crk::IOpSys::TextColor::Cyan },
};

Log* crk::Log::Get()
{
	static Log s_instance;
	return &s_instance;
}

void Log::Print(Log::Category category, int line, const char* file, const char* format, ...)
{
	// Use varargs to format string
	va_list args;
	va_start(args, format);

	vsprintf_s(m_messageBuffer, format, args);

	va_end(args);

	if (!m_initialized)
	{
		PrintSimple(format, args);
		return;
	}

	m_stringStream.str("");

	m_filename = file;
	m_shortFileName = m_filename.substr(m_filename.rfind('\\') + 1);
	m_lineId = m_shortFileName + ":" + std::to_string(line);
	
	bool shouldOutput = true;
	if (category == Log::Category::Todo)
	{
		auto setItr = m_todoSet.find(m_lineId);
		if (setItr != m_todoSet.end())
		{
			shouldOutput = false;
		}
		else
		{
			m_todoSet.emplace(m_lineId);
		}
	}

	if (!shouldOutput)
		return;

	// [Timestamp][Category] (file.h:32) - Message
	m_pOpSys->SetTextColor(s_categoryColorMap[category]);
	m_stringStream <<
		"[" << m_pOpSys->GetFormattedTimestamp()
		<< "][" << s_categoryNames[category] << "] (" 
		<< m_shortFileName.c_str() << ":" << line << ") - " 
		<< m_messageBuffer << std::endl;

	m_outFile << m_stringStream.str();
	std::cout << m_stringStream.str();
}

void crk::Log::PrintSimple(const char* format, ...)
{
	// Use varargs to format string
	va_list args;
	va_start(args, format);

	vsprintf_s(m_messageBuffer, format, args);

	va_end(args);

	std::cout << m_messageBuffer << std::endl;
}

bool crk::Log::Init(IOpSys* pOpSys, const char* outputFile)
{
	m_pOpSys = pOpSys;
	m_outFile.open(outputFile, 
		std::ios_base::out | std::ios_base::trunc);

	if (!m_outFile.is_open())
	{
		// ??
		return false;
	}

	m_initialized = true;
	return true;
}

void crk::Log::Cleanup()
{
	if (m_initialized)
	{
		m_pOpSys->RestoreDefaultTextColor();
		m_outFile.close();
	}
}

Log::Log()
	: m_messageBuffer()
	, m_pOpSys(nullptr)
	, m_initialized(false)
{
	
}

Log::~Log()
{
}

int Lua::Log_Log(lua_State* L)
{
	// Get the message from the stack
	// In lua, stack can be accessed backwards by using negative numbers:
	//		[-1] top of the stack
	const char* message = lua_tostring(L, -1);
	
	// Grab debug info from the stack (where this was called from)
	lua_Debug ar;
	lua_getstack(L, 1, &ar);
	lua_getinfo(L, "Slnt", &ar);

	// Shorten file name for easy reading
	static std::string file;
	file = ar.short_src;
	file = file.substr(file.rfind('/') + 1);

	// Log it!
	Log::Get()->Print(crk::Log::Category::Lua, ar.currentline,
		file.c_str(), message);

	return 0;
}
