#include "CommonMath.h"

using crk::FRect;

FRect::FRect()
	: x(0)
	, y(0)
	, width(0)
	, height(0)
{
}

crk::FRect::FRect(f32 x, f32 y, f32 width, f32 height)
	: x(x)
	, y(y)
	, width(width)
	, height(height)
{
}

crk::FColor::FColor()
	: r(255)
	, g(0)
	, b(255)
	, a(255)
{
}

crk::FColor::FColor(f32 r, f32 g, f32 b, f32 a)
	: r(r)
	, g(g)
	, b(b)
	, a(a)
{
}

crk::FColor crk::FColor::Lerp(const FColor& a, const FColor& b, f32 t)
{

	return FColor();
}

crk::FVec2::FVec2()
	: x(0)
	, y(0)
{
}

crk::FVec2::FVec2(f32 x, f32 y)
	: x(x)
	, y(y)
{
}

crk::FVec2 crk::FVec2::operator+(const FVec2& other)
{
	FVec2 result;
	result.x = this->x + other.x;
	result.y = this->y + other.y;
	return result;
}

crk::FVec2& crk::FVec2::operator+=(const FVec2& other)
{
	*this = *this + other;
	return *this;
}

crk::FVec2 crk::FVec2::operator*(float scalar)
{
	FVec2 result;
	result.x = this->x * scalar;
	result.y = this->y * scalar;
	return result;
}

crk::IVec2::IVec2()
	: x(0)
	, y(0)
{
}

crk::IVec2::IVec2(i32 x, i32 y)
	: x(x)
	, y(y)
{
}

f32 crk::Lerp(f32 a, f32 b, f32 t)
{
	return (a * (1.0f - t)) + (t * b);
}

crk::IRect::IRect()
	: x(0)
	, y(0)
	, width(0)
	, height(0)
{
}

crk::IRect::IRect(i32 x, i32 y, i32 width, i32 height)
	: x(x)
	, y(y)
	, width(width)
	, height(height)
{
}
