#pragma once
/** \file IGameLayer.h
 * Core interface for the logic of the game.  Must be inherited from in the Game project 
 */
// Created by Billy Graban

#include <vector>
#include <memory>

#include <View/IView.h>
#include <Logic/Actor/ActorFactory.h>
#include <Logic/Process/ProcessManager.h>
#include <Utils/Squirrel3.h>
#include <Logic/Event/EventDispatcher.h>
#include <Logic/Physics/Box2DPhysics.h>
#include <Logic/Scripting/LuaManager.h>

struct lua_State;

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class ApplicationLayer;
	class IContactListener;
	class TextComponent;

/** \class IGameLayer
 * @brief Core interface for the logic of the game.  Must be inherited from in the Game project 
 */
class IGameLayer
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	IGameLayer();

	/** Default Destructor */
	virtual ~IGameLayer();

	/** Get the name of the game (used for the window title) */
	virtual const char* GetGameName() const = 0;

	/** Initialize all modules
	 * @param pApp The application layer
	 * @return True if successful
	 */
	virtual bool Init(crk::ApplicationLayer* pApp);

	/** Adds a view to game's list of views
	 * @param pView The view to add
	 */
	virtual void AddView(std::unique_ptr<IView> pView);

	/** Creates a new view based on the string that is passed.  This will be improved and converted
	 * to a enum class.  Currently there are only two valid view types: 'Player' and 'Enemy'
	 * @param type The type of view to create
	 * @return The newly created view (nullptr on failure)
	 */
	virtual crk::IView* CreateView(const std::string& type) = 0;

	/** Step all modules forward by the a small amount of time
	 * @param deltaSeconds The amount of time (in seconds) since the last frame
	 */
	virtual void Update(float deltaSeconds);

	/** Registers all lambdas for creating components with the actor factory
	 * @param pApp The application layer
	 */
	virtual void RegisterComponentCreators(crk::ApplicationLayer* pApp);

	/** Registers all lambdas for creating processes with the actor factory.
	 * This can be compressed to a templated function now the engine is more decoupled
	 * @param pApp The application layer
	 */
	virtual void RegisterProcessCreators(ApplicationLayer* pApp);

	/** Acquires a resource based on the filepath and attempts to spawn the actor
	 * @param filepath The path to the actor resource
	 * @return Shared pointer to the new actor (if any)
	 */
	virtual std::shared_ptr<Actor> SpawnActor(const char* filepath);

	/** Attempts to spawn the actor
	 * @param pResource Resource containing raw XML data for the actor
	 * @return Shared pointer to the new actor (if any)
	 */
	virtual std::shared_ptr<crk::Actor> SpawnActor(IResource* pResource);

	/** Queue actor to be destroyed at the end of this frame 
	 * @param actorId The Id of the actor to be destroyed
	 */
	void DestroyActor(Id actorId);

	/** Deallocate all memory and clean up all library allocations */
	virtual void Cleanup();

	/** Builds a CollisionPair to be used later in the core loop
	 * I Really want to make these private and use friend functions to hide this functionality from
	 * everybody but the contact listener
	 * @param pActorA The first actor involved in the collision
	 * @param pActorB The second actor involved in the collision
	 * @param isOverlap True if the actors are overlapping
	 * @param isBeginning True if the collision just started
	 */
	void NotifyContact(Actor* pActorA, Actor* pActorB, bool isOverlap, bool isBeginning);

	/** Registers all global variables and global functions with lua */
	void RegisterWithLua();

protected:
	// --------------------------------------------------------------------- //
	// Protected Member Variables
	// --------------------------------------------------------------------- //
	/** Lookup table for actors, keyed by their unique Id */
	std::unordered_map<Id, std::shared_ptr<Actor>> m_actors;

	/** Collection of actors to add to the lookup table at the beginning of the next frame */
	std::vector<std::shared_ptr<Actor>> m_actorsToAdd;

	ActorFactory m_actorFactory;			/**< Creates actors and their components/processes */
	ProcessManager m_processManager;		/**< Manages processes of the game layer */
	Squirrel3 m_logicRng;					/**< Random number generator used for logical rolls */
	EventDispatcher* m_pEventDispatcher;	/**< Queues and dispatches events */
	LuaManager m_luaManager;				/**< Manages lua state and global variables */
	ApplicationLayer* m_pApp;				/**< The application layer that built this game layer */
	
	TextComponent* m_pFpsUiText;			/**< Displays the FPS */

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	std::vector<std::unique_ptr<IView>> m_pViews;	/**< Collection of all active views in the game */
	std::vector<Id> m_actorsToDestroy;				/**< Actors waiting to be destroyed at end of frame */
	std::vector<Actor*> m_actorsOnScreen;			/**< All actors on screen to be rendered */

	// These actors just touched this frame
	/** \struct CollisionPair */
	/** Contains collision data for contacts this frame */
	struct CollisionPair
	{
		Actor* m_pActorA;		/**< First actor in contact event */
		Actor* m_pActorB;		/**< Second actor in contact event */
		bool m_isOverlap;		/**< True if they are overlapping */
		bool m_isBegin;			/**< True if the contact is beginning */

		/** Constructor 
		 * @param pActorA The first actor involved in the collision
		 * @param pActorB The second actor involved in the collision
		 * @param isOverlap True if the actors are overlapping
		 * @param isBeginning True if the collision just started
		 */
		CollisionPair(Actor* pActorA, Actor* pActorB, bool isOverlap, bool isBegin);
	};

	/** Collection of collisions that happened this frame */
	std::vector<CollisionPair> m_recentCollisions;

	IPhysicsSimulation* m_pPhysics;		/**< Core physics simulation object */
	Squirrel3 m_coreRng;				/**< The RNG that seeds all other RNGs.  This is the core seed */
	size_t m_destroyActorListenerId;	/**< Used to remove event listener before cleanup */
	IVec2 m_screenDimensions;			/**< Dimension (in pixels) of the screen.  This could be removed I think */

	// Frame counter
	f32 m_frameTimer;					/**< Seconds elapsed */
	u32 m_frameCounter;					/**< Number of frames counted */
	u32 m_fps;							/**< Frames per second */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //

	/** Loop through recent collisions and execute relevant callbacks stored in the PhysicsComponent */
	void RespondToCollisions();

public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	/** Add a process to the process manager
	 * @param pProcess The process to add
	 */
	void AddProcess(std::shared_ptr<IProcess> pProcess) { m_processManager.AttachProcess(pProcess); }

	/** Tell the physics simulation which contact listener to use when handling contacts
	 * @param pListener The concrete listener to use
	 */
	void SetContactListener(IContactListener* pListener) { m_pPhysics->SetContactListener(pListener); }

	void SetDrawFPS(bool drawFps);							/**< Tell the PlayerView to draw the FPS or not */

	std::unordered_map<Id, std::shared_ptr<Actor>> &GetActors() { return m_actors; }	/**< All actors being updated this frame */
	std::vector <Actor*>& GetActorsOnScreen() { return m_actorsOnScreen; }				/**< Actors on the screen to be rendered */
	crk::IPhysicsSimulation* GetPhysicsSim() { return m_pPhysics; }						/**< Get the physics simulation */

	LuaManager* GetLuaManager() { return &m_luaManager; }	/**< Get the lua manager */
	Squirrel3& GetLogicRng() { return m_logicRng; }			/**< Get the Logic RNG */
	u32 GetFPS() { return m_fps; }							/**< Get the FPS count */
	crk::Actor* GetRawActorById(Id id);						/**< Get raw actor pointer by id (used with Lua) */
	crk::Actor* GetRawActorByName(std::string name);		/**< Get raw actor pointer by name (used with Lua) */
	std::shared_ptr<Actor> GetActorById(Id id);				/**< Get shared actor pointer by id (used Engine-side) */

};
}

/** \namespace Lua Contains all global C functions that connect the Croak Engine to Lua */
namespace Lua
{
	/** Lua binding for IGameLayer::GetRawActoryById()
	 * @param L The lua state
	 * @return The number of arguments pushed onto the lua stack
	 * @see crk::IGameLayer::GetRawActoryById()
	 */
	int GameLayer_GetActorById(lua_State* L);

	/** Lua binding for IGameLayer::GetRawActoryByName()
	 * @param L The lua state
	 * @return The number of arguments pushed onto the lua stack
	 * @see crk::IGameLayer::GetRawActoryByName()
	 */
	int GameLayer_GetActorByName(lua_State* L);

	/** Lua binding for IGameLayer::SpawnActor()
	 * @param L The lua state
	 * @return The number of arguments pushed onto the lua stack
	 * @see crk::IGameLayer::SpawnActor()
	 */
	int GameLayer_SpawnActor(lua_State* L);

	/** Lua binding for IGameLayer::CreateView()
	 * @param L The lua state
	 * @return The number of arguments pushed onto the lua stack
	 * @see crk::IGameLayer::CreateView()
	 */
	int GameLayer_CreateView(lua_State* L);
}