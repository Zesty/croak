#pragma once
/** \file IResource.h
 * Base interface for all resources used by the engine 
 */
// Created by Billy Graban

#include <vector>
#include <string>
#include <cstddef>

#include <Utils/Typedefs.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	/** Alias for the resource type id */
	using ResourceTypeId = u32;

/** \class IResource
 * @brief Base interface for all resources used by the engine 
 */
class IResource
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor 
	 * @param filepath Path to the resource
	 * @param data Raw vector of bytes representing the data within the file
	 */
	IResource(std::string filepath, std::vector<std::byte> data);

	/** Default Destructor */
	virtual ~IResource();


private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	std::vector<std::byte> m_data;	/**< Raw data in byte form */
	std::string m_filepath;			/**< Path to the resource */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	const std::vector<std::byte>& GetData() const { return m_data; }	/**< Get raw bytes */
	const std::string& GetName() const { return m_filepath; }			/**< Get filepath */

};
}