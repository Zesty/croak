#include "IResource.h"

using crk::IResource;

crk::IResource::IResource(std::string filepath, std::vector<std::byte> data)
	: m_filepath(filepath)
	, m_data(data)
{
}

IResource::~IResource()
{
	
}