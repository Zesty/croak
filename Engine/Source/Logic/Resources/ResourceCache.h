#pragma once
/** \file ResourceCache.h
 * Central repository for all resources
 * These resources are currently never unloaded until the game is shut down
 * This is a singleton
 */
// Created by Billy Graban

#include <unordered_map>
#include <string>
#include <memory>

#include "IResource.h"

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class IGraphics;
	class IAudio;
	class IFontLoader;
	class IFont;

/** \class ResourceCache
 * @brief Central repository for all resources
 * These resources are currently never unloaded until the game is shut down
 * This is a singleton
 */
class ResourceCache
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //

	/** Initialize the resource cache
	 * @param pGraphics Graphics object for loading images
	 * @param pAudio Audio object for loading sounds/music
	 * @param pFontLoader Loads fonts and builds font atlasses
	 * @return True if initialization is successful
	 */
	bool Init(IGraphics* pGraphics, IAudio* pAudio, IFontLoader* pFontLoader);

	/** Deallocate all resources */
	void Cleanup();

	/** Singleton getter */
	static ResourceCache* Get();

	/** Uses the lookup table to find the resource.  If not found, it will be loaded from
	 * the disk.
	 * @param filepath The location of the resource.  This will be sanitized
	 * @return Shared pointer to the base resource
	 */
	std::shared_ptr<IResource> AcquireResource(const char* filepath);

	/** Uses the lookup table to find a font of the desired location and size.  If not found
	 * a new font will be created
	 * @param filepath Path to the font file
	 * @param pointSize Vertical size of the resulting font in pixels
	 * @return Shared pointer to the new font object 
	 */
	std::shared_ptr<IFont> AcquireFont(const char* filepath, int pointSize);

	/** Calls AcquireResource and casts the result to the desired type
	 * @see crk::ResourceCache::AcquireResource
	 * @param filepath Path to the resource
	 * @return Shared pointer to the resource
	 */
	template <class ResourceType>
	std::shared_ptr<ResourceType> AcquireResource(const char* filepath);

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** Lookup table for all loaded resources (keyed by sanitized filepath) */
	std::unordered_map<std::string, std::shared_ptr<IResource>> m_resourceMap;

	IGraphics* m_pGraphics;			/**< Loads images */
	IAudio* m_pAudio;				/**< Loads audio files */
	IFontLoader* m_pFontLoader;		/**< Loads fonts and builds font atlasses */

	// HACK?  I don't want to expose a fontSize variable to AcquireResource, so I'm going to
	// do some bookkeeping under the covers here
	bool m_acquiringFont;	/**< True if the user just tried to Acquire a font */
	int m_fontSize;			/**< The size of the font that the user wished to acquire */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //
	/** Hidden Default Constructor */
	ResourceCache();

	/** Hidden Default Destructor */
	~ResourceCache();

public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};

template <class ResourceType>
std::shared_ptr<ResourceType> ResourceCache::AcquireResource(const char* filepath)
{
	auto pResource = AcquireResource(filepath);
	
	return std::static_pointer_cast<ResourceType>(pResource);
}

}