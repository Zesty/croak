#include "ResourceCache.h"

#include <cctype>  // std::tolower
#include <algorithm>
#include <fstream>

#include <Utils/Log.h>

// Interfaces for loading
#include <Application/Graphics/IGraphics.h>
#include <Application/Graphics/Textures/ITexture.h>
#include <Application/Graphics/Fonts/IFontLoader.h>
#include <Application/Graphics/Fonts/IFont.h>
#include <Application/Audio/IAudio.h>
#include <Application/Audio/Sound/ISound.h>
#include <Application/Audio/Music/IMusic.h>

using crk::ResourceCache;

ResourceCache::ResourceCache()
	: m_pGraphics(nullptr)
	, m_pAudio(nullptr)
	, m_pFontLoader(nullptr)
	, m_acquiringFont(false)
	, m_fontSize(0)
{
}

ResourceCache::~ResourceCache()
{
	
}

bool crk::ResourceCache::Init(IGraphics* pGraphics, IAudio* pAudio, IFontLoader* pFontLoader)
{
	m_pGraphics = pGraphics;
	m_pAudio = pAudio;
	m_pFontLoader = pFontLoader;

	return true;
}

void crk::ResourceCache::Cleanup()
{
	m_resourceMap.clear();
}

std::shared_ptr<crk::IResource> crk::ResourceCache::AcquireResource(const char* filepath)
{
	// C:/Hello/World/Stuff.txt
	// C:/Hello\\World\\Stuff.txt
	// C:/HELLO\\World\\stuff.txt
	// C:/HEllO/World\\sTUff.txt

	// Sanitizing the filepath so developers don't load the same memory into different buckets
	std::string path = filepath;
	std::transform(path.begin(), path.end(), path.begin(), std::tolower);
	std::replace(path.begin(), path.end(), '\\', '/');

	// Because of the added complexity of storing fonts of different sizes, we're going to need to build a
	//	new string to search the resource map for
	std::string pathWithSize = path;
	if (m_acquiringFont)
	{
		// In order to separate different font resources by point size, append the font size before the extension
		pathWithSize.insert(pathWithSize.find_last_of('.'), std::to_string(m_fontSize));
	}

	auto resItr = m_resourceMap.find(pathWithSize);
	if (resItr == m_resourceMap.end())
	{
		// Load the resource
		// Find the size of the file
		std::ifstream inFile(path.c_str(), std::ios_base::binary | std::ios_base::in);

		if (inFile.fail())
		{
			LOG(Error, "Could not load resource: %s", filepath);
			return nullptr;
		}

		inFile.seekg(0, std::ios::end);
		std::streamoff size = inFile.tellg();
		inFile.seekg(0, std::ios::beg);

		// Read entire contents into a vector
		std::vector<std::byte> data(static_cast<size_t>(size));
		inFile.read(reinterpret_cast<char*>(data.data()), size);
		inFile.close();

		std::string extension = path.substr(path.find_last_of('.') + 1);

		// Make the base resource
		auto pResource = std::make_shared<IResource>(path, data);

		// Depending on extension, call different loading functions
		if (extension == "xml")
		{
			LOG(Todo, "Create XmlResource instead of relying on just IResource?");
			resItr = m_resourceMap.emplace(path, pResource).first;
		}
		else if (extension == "png")
		{
			auto pTexture = m_pGraphics->LoadTexture(pResource.get());
			resItr = m_resourceMap.emplace(path, pTexture).first;
		}
		else if (extension == "wav")
		{
			auto pSound = m_pAudio->LoadSound(pResource.get());
			resItr = m_resourceMap.emplace(path, pSound).first;
		}
		else if (extension == "mp3" || extension == "ogg")
		{
			auto pMusic = m_pAudio->LoadMusic(pResource.get());
			resItr = m_resourceMap.emplace(path, pMusic).first;
		}
		else if (extension == "ttf")
		{
			if (!m_acquiringFont)
			{
				LOG(Error, "To acquire a font you should use AcquireFont");
				return nullptr;
			}

			auto pFont = m_pFontLoader->Load(pResource.get(), m_fontSize, m_pGraphics);
			resItr = m_resourceMap.emplace(pathWithSize, pFont).first;

			// Turn this off once we're done with the font
			m_acquiringFont = false;
		}
		else
		{
			// Unknown extension
			LOG(Error, "Unhandled extension: %s.  Resource cache failed to load: %s", extension.c_str(), path.c_str());
			inFile.close();
			return nullptr;
		}
	}

	return resItr->second;
}

std::shared_ptr<crk::IFont> crk::ResourceCache::AcquireFont(const char* filepath, int pointSize)
{
	m_fontSize = pointSize;
	m_acquiringFont = true;

	return AcquireResource<IFont>(filepath);
}

ResourceCache* crk::ResourceCache::Get()
{
	static ResourceCache sInstance;
	return &sInstance;
}
