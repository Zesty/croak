#pragma once
/** \file LuaManager.h
 * Contains the lua_State and makes interacting with it much cleaner 
 */
// Created by Billy Graban

#include <vector>
#include <memory>

#include <Lua/lua.hpp>

#include <Utils/Typedefs.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class IEvent;

/** \class LuaManager
 * @brief Contains the lua_State and makes interacting with it much cleaner 
 */
class LuaManager
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	LuaManager();

	/** Default Destructor */
	~LuaManager();

	/** Create the lua_State and load core libraries
	 * @return True if successful
	 */
	bool Init();

	/** Call Update() function exposed to lua
	 * @param deltaSeconds The amount of time (in seconds) that have elapsed since the last frame
	 */
	void Update(f32 deltaSeconds);

	/** Deallocate any memory that was used */
	void Cleanup();

	/** Call the function that has been pushed onto the stack
	 * @param argumentCount The number of arguments in the function
	 * @param returnCount The number of values returned from the lua function
	 */
	void CallFunction(int argumentCount = 0, int returnCount = 0) const;

	/** Push a global onto the stack
	 * @param name The name of the global to push
	 */
	void GetGlobal(const char* name);

	/** Create a new global variable and assign it a raw pointer
	 * @param rawPointer Memory address of the object to add to lua's global table
	 * @param name The key associated with this data
	 */
	void SetGlobalUserData(void* rawPointer, const char* name);

	/** Bind a C function to a name in lua's global table
	 * @param name The name to associate with the C function
	 * @param function Classic C-Style function pointer with signature <int(lua_State*)>
	 */
	void RegisterFunction(const char* name, lua_CFunction function);

	/** Sends a single event to the HandleEvents function in lua
	 * @param pEvent The event to push onto the stack
	 */
	void SendEvent(IEvent* pEvent);

	/** Sends all events in a single table to the HandleEvents function in lua
	 * @param pEvents All events that have not been processed by lua this frame
	 */
	void SendEvents(std::vector<std::unique_ptr<IEvent>>& pEvents);

	/** Push a float value onto the stack */
	void Push(f32 value);
private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	lua_State* m_pState;		/**< The lua state object */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	lua_State* GetState() const { return m_pState; }	/**< Get the lua state object */

};
}