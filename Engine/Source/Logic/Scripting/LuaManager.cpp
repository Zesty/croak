#include "LuaManager.h"

#include <Utils/Log.h>
#include <Logic/Event/IEvent.h>

using crk::LuaManager;

LuaManager::LuaManager()
	: m_pState(nullptr)
{

}

LuaManager::~LuaManager()
{

}

bool crk::LuaManager::Init()
{
	m_pState = luaL_newstate();
	if (!m_pState)
	{
		// TODO: Log failure
		return false;
	}

	luaL_openlibs(m_pState);

	return true;
}

void crk::LuaManager::Update(f32 deltaSeconds)
{
	GetGlobal("Update");
	Push(deltaSeconds);
	CallFunction(1);
}

void crk::LuaManager::Cleanup()
{
	lua_close(m_pState);
}

void LuaManager::GetGlobal(const char* name)
{
	lua_getglobal(m_pState, name);
}

void crk::LuaManager::SetGlobalUserData(void* rawPointer, const char* name)
{
	lua_pushlightuserdata(m_pState, rawPointer);
	lua_setglobal(m_pState, name);
}

void crk::LuaManager::RegisterFunction(const char* name, lua_CFunction function)
{
	lua_register(m_pState, name, function);
}

void crk::LuaManager::SendEvent(IEvent* pEvent)
{
	lua_getglobal(m_pState, "HandleEvents");

	// events = {}
	//	events[1] = { name:'EnemyDeathEvent', enemy:0x0123'4567}

	// Create Events table
	lua_newtable(m_pState);

	// Create event
	if (pEvent->PushLuaTable(m_pState))
	{
		// Store the event in the table as a single array value
		lua_seti(m_pState, -2, 1);
	}


	CallFunction(1, 0);
}

void crk::LuaManager::SendEvents(std::vector<std::unique_ptr<IEvent>>& pEvents)
{
	lua_getglobal(m_pState, "HandleEvents");

	// Create event table
	lua_newtable(m_pState);

	int count = 1;
	for (size_t i = 0; i < pEvents.size(); ++i)
	{
		if (pEvents[i]->PushLuaTable(m_pState))
		{
			lua_seti(m_pState, -2, count++);
		}
	}

	if (count > 1)
		CallFunction(1, 0);
	else
		lua_pop(m_pState, 2);
}

void crk::LuaManager::Push(f32 value)
{
	lua_pushnumber(m_pState, value);
}

void crk::LuaManager::CallFunction(int argumentCount, int returnCount) const
{
	// Calling a function on the stack:
	if (lua_pcall(m_pState, argumentCount, returnCount, 0) != LUA_OK)
	{
		LOG(Error, "Lua Error: %s", lua_tostring(m_pState, -1));
		return;
	}
}
