#include "IGameLayer.h"

#include <cassert>

#include <Utils/Log.h>

#include <View/IView.h>

#include <Logic/Resources/ResourceCache.h>

// For component creators
#include <Logic/Components/TransformComponent.h>
#include <Logic/Components/Render/TextComponent.h>
#include <Logic/Components/Render/RenderComponent.h>
#include <Logic/Components/Physics/Box2DPhysicsComponent.h>

// For processes
#include <Logic/Process/Move/MoveProcess.h>
#include <Logic/Process/Move/RotateProcess.h>
#include <Logic/Process/Timers/DelayProcess.h>
#include <Logic/Process/Render/AnimationProcess.h>
#include <Logic/Process/Render/SpriteFadeProcess.h>

// Events
#include <Logic/Event/Events/DestroyActorEvent.h>

#include <Application/ApplicationLayer.h>
#include <Logic/Physics/IPhysicsSimulation.h>
#include <Utils/TinyXml2/XmlLoaderUtil.h>
#include <Logic/Rendering/IRenderable.h>

using crk::IGameLayer;

IGameLayer::IGameLayer()
	: m_pEventDispatcher(nullptr)
	, m_pApp(nullptr)
	, m_pPhysics(nullptr)
	, m_destroyActorListenerId(0)
	, m_frameTimer(0.f)
	, m_frameCounter(0)
	, m_fps(0)
	, m_pFpsUiText(nullptr)
{
	
}

IGameLayer::~IGameLayer()
{
}

bool crk::IGameLayer::Init(ApplicationLayer* pApp)
{
	m_pEventDispatcher = EventDispatcher::Get();
	m_pEventDispatcher->SetLuaManager(&m_luaManager);
	m_pPhysics = pApp->GetPhysics();

	//LOG(Todo, "Create PlaySoundEvent to remove m_pApp from IGameLayer");
	//m_pApp = pApp;
	m_screenDimensions = pApp->GetScreenDimensions();

	// Use time to seed the core RNG.  All other RNGs will be seeded from this one
	m_coreRng = Squirrel3((u32)time(nullptr));
	m_logicRng = Squirrel3(m_coreRng.RollRandomUint32());

	LOG(Todo, "Move View::Inits to a more appropriate place (after they've had their actor attached)");
	for (auto& pView : m_pViews)
	{
		if (!pView->Init(pApp))
		{
			LOG(Error, "Failed to initialize a View");
			return false;
		}
	}

	// Set up event listeners
	m_destroyActorListenerId = 
		m_pEventDispatcher->AddEventListener(DestroyActorEvent::kEventId, [this](IEvent* pEvent)
		{
			assert(pEvent->GetEventId() == DestroyActorEvent::kEventId);
			auto pDestroyEvent = static_cast<DestroyActorEvent*>(pEvent);
			m_actorsToDestroy.push_back(pDestroyEvent->GetActorId());
		});

	// Factory needs game layer to add processes associated with actors on load
	m_actorFactory.SetGameLayer(this);

	// Actor factory contains it's own RNG
	m_actorFactory.SeedRng(m_coreRng.RollRandomUint32());
	XmlLoaderUtil::s_FactoryRng = m_actorFactory.GetRng();

	// Register functions used to create components and processes
	RegisterComponentCreators(pApp);
	RegisterProcessCreators(pApp);

	auto pFps = SpawnActor("Assets/Actors/UI/FPS.xml");
	m_pFpsUiText = pFps->GetComponent<crk::TextComponent>();
	m_pFpsUiText->UpdateText("FPS: 0");

	// Initialize scripting 
	if (!m_luaManager.Init())
	{
		LOG(Error, "Failed to initialize LuaManager");
		return false;
	}

	pApp->GetKeyboard()->RegisterWithLua(&m_luaManager);
	RegisterWithLua();

	return true;
}

void crk::IGameLayer::AddView(std::unique_ptr<IView> pView)
{
	m_pViews.emplace_back(std::move(pView));
}

void crk::IGameLayer::Update(float deltaSeconds)
{
	m_frameTimer += deltaSeconds;
	++m_frameCounter;
	if (m_frameTimer >= 1.0f)
	{
		m_frameTimer -= 1.0f;
		m_fps = m_frameCounter;
		m_pFpsUiText->UpdateText("FPS: " + std::to_string(m_frameCounter));
		m_frameCounter = 0;
	}

	// Only add actors at the start of the frame
	for (auto& pActor : m_actorsToAdd)
	{
		m_actors.emplace(pActor->GetId(), pActor);
	}
	m_actorsToAdd.clear();

	// Update the input for each view available
	for (auto& pView : m_pViews)
	{
		pView->UpdateInput();
	}

	m_luaManager.Update(deltaSeconds);

	// Update physics before actors so the dynamic components update can place the transform correctly before drawing
	m_pPhysics->Update(deltaSeconds);

	m_processManager.UpdateProcesses(deltaSeconds);

	LOG(Todo, "Extract AABB collision from IGameLayer");
	LOG(Todo, "Use insertion sort for building actorsOnScreen");

	// Update actor components
	for (auto& pActor : m_actors)
	{
		pActor.second->Update(deltaSeconds);

		auto pRenderComponent = pActor.second->GetComponent<RenderComponent>();

		if (pRenderComponent)
		{
			FRect drawRect = pRenderComponent->GetRenderable()->GetDrawRect();

			if (drawRect.x < m_screenDimensions.x &&
				drawRect.x + drawRect.width > 0 &&
				drawRect.y < m_screenDimensions.y &&
				drawRect.y + drawRect.height > 0)
			{
				m_actorsOnScreen.push_back(pActor.second.get());
			}
		}
	}

	// Sorting our actors here.  This makes the most sense as we only
	// care about sorting actors that will actually be drawn to the screen
	std::sort(m_actorsOnScreen.begin(), m_actorsOnScreen.end(),
		[](Actor* a, Actor* b)
		{ 
			auto pRenderComponentA = a->GetComponent<RenderComponent>();
			auto pRenderComponentB = b->GetComponent<RenderComponent>();

			return pRenderComponentA->GetLayer() < pRenderComponentB->GetLayer(); 
		});

	// Respond to collisions from physics system
	RespondToCollisions();

	// NOTE: I have moved this from above UpdateProcesses to below our collision response 
	// so objects that need to be destroyed are done for immediately
	m_pEventDispatcher->ProcessEvents();

	// HACK: Testing tiled map
	//m_actorsOnScreen.insert(m_actorsOnScreen.begin(), GetRawActorByName("Map"));

	// Draw the scene for players
	for (auto& pView : m_pViews)
	{
		pView->ViewScene();
	}

	// Clear the actors on screen
	m_actorsOnScreen.clear();

	// Destroy actors at end of frame
	LOG(Todo, "Find a way to remove views without O(n) cost");
	LOG(Todo, "Remove duct-tape from ActorDestroy loop.  It prevents the game from crashing when trying to delete an object that somehow no longer exists.  INVESTIGATE");
	for (auto id : m_actorsToDestroy)
	{
		auto pActorItr = m_actors.find(id);

		// INVESTIGATE: I don't think this check should be necessary
		if (pActorItr == m_actors.end())
		{
			LOG(Warning, "Bad News!  IGameLayer is trying to delete an actor that doesn't exist");
			continue;
		}

		auto pOwningView = pActorItr->second->GetOwningView();

		if (pOwningView)
		{
			pOwningView->DetachActor();

			for (auto viewItr = m_pViews.begin(); viewItr != m_pViews.end(); ++viewItr)
			{
				if (viewItr->get() == pOwningView)
				{
					m_pViews.erase(viewItr);
					break;
				}
			}
		}

		m_processManager.AbortProcessesOnActor(id);
		m_actors.erase(id);
	}
	m_actorsToDestroy.clear();

	// NOTE: Destroying bodies in Box2D will generate an end overlap event.  We want to clear the vector again
	// so there aren't any collisions with dead actors inside
	m_recentCollisions.clear();
}

void crk::IGameLayer::RegisterComponentCreators(crk::ApplicationLayer* pApp)
{
	// Transform
	m_actorFactory.RegisterComponentCreator(crk::IComponent::HashName(TransformComponent::GetName()),
		[](crk::Actor* pOwner) { return std::make_unique<crk::TransformComponent>(pOwner); });

	// PhysicsBody
	m_actorFactory.RegisterComponentCreator(IComponent::HashName(Box2DPhysicsComponent::GetName()),
		[this](crk::Actor* pOwner)
		{
			std::unique_ptr<Box2DPhysicsComponent> pBody = std::make_unique<Box2DPhysicsComponent>(pOwner, m_pPhysics);
			return pBody;
		});

	// TextComponent
	m_actorFactory.RegisterComponentCreator(IComponent::HashName(TextComponent::GetName()),
		[](crk::Actor* pOwner)
		{
			std::unique_ptr<TextComponent> pText = std::make_unique<TextComponent>(pOwner);
			return pText;
		});

	// RenderComponent
	m_actorFactory.RegisterComponentCreator(IComponent::HashName(RenderComponent::GetName()),
		[](crk::Actor* pOwner) { return std::make_unique<RenderComponent>(pOwner); });
}

void crk::IGameLayer::RegisterProcessCreators(ApplicationLayer* pApp)
{
	// Simple default processes
	m_actorFactory.RegisterProcessCreator(DelayProcess::GetName(),
		[](Actor* pOwner) { return std::make_shared<DelayProcess>(pOwner); });
	m_actorFactory.RegisterProcessCreator(SpriteFadeProcess::GetName(),
		[](Actor* pOwner) { return std::make_shared<SpriteFadeProcess>(pOwner); });
	m_actorFactory.RegisterProcessCreator(MoveProcess::GetName(),
		[](Actor* pOwner) { return std::make_shared<MoveProcess>(pOwner); });
	m_actorFactory.RegisterProcessCreator(RotateProcess::GetName(),
		[](Actor* pOwner) { return std::make_shared <RotateProcess>(pOwner); });
	m_actorFactory.RegisterProcessCreator(AnimationProcess::GetName(),
		[](Actor* pOwner) { return std::make_shared<AnimationProcess>(pOwner); });
}

std::shared_ptr<crk::Actor> crk::IGameLayer::SpawnActor(const char* filepath)
{
	auto pResource = ResourceCache::Get()->AcquireResource(filepath);
	return SpawnActor(pResource.get());
}

std::shared_ptr<crk::Actor> crk::IGameLayer::SpawnActor(IResource* pResource)
{
	std::shared_ptr<crk::Actor> pActor = m_actorFactory.CreateActor(pResource);

	// Don't add actors until the next frame
	if (pActor)
	{
		m_actorsToAdd.push_back(pActor);
	}

	return pActor;
}

void crk::IGameLayer::DestroyActor(Id actorId)
{
	m_actorsToDestroy.push_back(actorId);
}

void crk::IGameLayer::Cleanup()
{
	// Stop listening for DestroyActorEvent
	m_pEventDispatcher->RemoveEventListener(DestroyActorEvent::kEventId, m_destroyActorListenerId);

	// Clean up Lua here?
	m_luaManager.Cleanup();

	// Clean up views before actors
	for (auto& pView : m_pViews)
	{
		pView->DetachActor();
		pView.reset();
	}

	// Manually clear actors
	for (auto& pActor : m_actors)
	{
		pActor.second.reset();
	}
	m_actors.clear();

	// I'll need to call this manually in order to make sure GameLayer functions can still be called?
	m_processManager.AbortAllProcesses();

	// Clear all recent collisions generated from removal of actors from the Physics Simulation
	m_recentCollisions.clear();

}

void crk::IGameLayer::NotifyContact(Actor* pActorA, Actor* pActorB, bool isOverlap, bool isBeginning)
{
	CollisionPair pair{ pActorA, pActorB, isOverlap, isBeginning };
	m_recentCollisions.emplace_back(pair);
}

void crk::IGameLayer::RegisterWithLua()
{
	m_luaManager.SetGlobalUserData(this, "GameLayer");
	m_luaManager.SetGlobalUserData(&m_actorFactory, "ActorFactory");

	m_luaManager.RegisterFunction("log", Lua::Log_Log);

	// IGameLayer
	m_luaManager.RegisterFunction("getActorById", Lua::GameLayer_GetActorById);
	m_luaManager.RegisterFunction("getActorByName", Lua::GameLayer_GetActorByName);
	m_luaManager.RegisterFunction("spawnActor", Lua::GameLayer_SpawnActor);
	m_luaManager.RegisterFunction("createView", Lua::GameLayer_CreateView);

	// IView
	m_luaManager.RegisterFunction("setControlledActor", Lua::View_SetControlledActor);

	// Actor
	m_luaManager.RegisterFunction("getComponent", Lua::Actor_GetComponent);
	m_luaManager.RegisterFunction("getName", Lua::Actor_GetName);

	// Actor Factory
	m_luaManager.RegisterFunction("randomFloatRange", Lua::ActorFactory_RandomFloatRange);

	// Transform
	m_luaManager.RegisterFunction("move", Lua::TransformComponent_Move);
	m_luaManager.RegisterFunction("getPosition", Lua::TransformComponent_GetPosition);

	// IPhysicsComponent
	m_luaManager.RegisterFunction("movePhysicsBody", Lua::PhysicsComponent_ApplyLinearImpulseToCenter);
	m_luaManager.RegisterFunction("setLinearVelocity", Lua::PhysicsComponent_SetLinearVelocity);
	m_luaManager.RegisterFunction("setBodyPosition", Lua::PhysicsComponent_SetPosition);
}

void crk::IGameLayer::RespondToCollisions()
{
	if (m_recentCollisions.size() > 0)
	{
		for (auto& pair : m_recentCollisions)
		{
			if (pair.m_isOverlap)
			{
				if (pair.m_isBegin)
				{
					pair.m_pActorA->GetComponent<Box2DPhysicsComponent>()->HandleBeginOverlap(pair.m_pActorB);
					pair.m_pActorB->GetComponent<Box2DPhysicsComponent>()->HandleBeginOverlap(pair.m_pActorA);
				}
				else
				{
					pair.m_pActorA->GetComponent<Box2DPhysicsComponent>()->HandleEndOverlap(pair.m_pActorB);
					pair.m_pActorB->GetComponent<Box2DPhysicsComponent>()->HandleEndOverlap(pair.m_pActorA);
				}
			}
			else
			{
				if (pair.m_isBegin)
				{
					pair.m_pActorA->GetComponent<Box2DPhysicsComponent>()->HandleBeginCollision(pair.m_pActorB);
					pair.m_pActorB->GetComponent<Box2DPhysicsComponent>()->HandleBeginCollision(pair.m_pActorA);
				}
				else
				{
					pair.m_pActorA->GetComponent<Box2DPhysicsComponent>()->HandleEndCollision(pair.m_pActorB);
					pair.m_pActorB->GetComponent<Box2DPhysicsComponent>()->HandleEndCollision(pair.m_pActorA);
				}
			}
		}

		m_recentCollisions.clear();
	}
}

void crk::IGameLayer::SetDrawFPS(bool drawFps)
{
	m_pFpsUiText->GetOwner()->SetActive(drawFps);
}

crk::Actor* crk::IGameLayer::GetRawActorById(Id id)
{
	auto mapItr = m_actors.find(id);
	if (mapItr == m_actors.end())
	{
		// Iterate through the actors waiting to be added to the world as well
		for (auto& actor : m_actorsToAdd)
		{
			if (actor->GetId() == id)
			{
				return actor.get();
			}
		}

		LOG(Warning, "Actor id: '%d' doesn't exist", id);
		return nullptr;
	}

	return mapItr->second.get();
}

crk::Actor* crk::IGameLayer::GetRawActorByName(std::string name)
{
	LOG(Todo, "GetRawActorByName is extremely slow, as it has to iterate over all actors in the world");
	for (auto& [key, value] : m_actors)
	{
		if (value->GetName() == name)
		{
			return value.get();
		}
	}

	// Iterate through the actors waiting to be added to the world as well
	for (auto& actor : m_actorsToAdd)
	{
		if (actor->GetName() == name)
		{
			return actor.get();
		}
	}

	return nullptr;
}

std::shared_ptr<crk::Actor> crk::IGameLayer::GetActorById(Id id)
{
	auto mapItr = m_actors.find(id);
	if (mapItr == m_actors.end())
	{
		for (auto& pActor : m_actorsToAdd)
		{
			if (pActor->GetId() == id)
			{
				return pActor;
			}
		}

		LOG(Warning, "Could not find actor with id: %d", id);
		return nullptr;
	}

	return mapItr->second;
}

crk::IGameLayer::CollisionPair::CollisionPair(Actor* pActorA, Actor* pActorB, bool isOverlap, bool isBegin)
	: m_pActorA(pActorA)
	, m_pActorB(pActorB)
	, m_isOverlap(isOverlap)
	, m_isBegin(isBegin)
{
}

// actor = GetActorById(1)
int Lua::GameLayer_GetActorById(lua_State* L)
{
	// Assume that:
	//	- there is only one argument
	//	- the argument is an integer (unsigned)
	Id id = static_cast<Id>(lua_tointeger(L, -1));							// + 1

	// How do we call a member function?
	lua_getglobal(L, "GameLayer");											// + 2
	IGameLayer* pGame = static_cast<IGameLayer*>(lua_touserdata(L, -1));	// + 2

	// Clean up stack
	lua_pop(L, 2);															// + 0

	// Call our C++ function
	crk::Actor* pActor = pGame->GetRawActorById(id);

	// In order to return a value, we need to push it onto the stack
	lua_pushlightuserdata(L, pActor);

	// There is one return value
	return 1;
}

int Lua::GameLayer_GetActorByName(lua_State* L)
{
	const char* name = lua_tostring(L, -1);

	lua_getglobal(L, "GameLayer");
	IGameLayer* pGame = static_cast<IGameLayer*>(lua_touserdata(L, -1));

	// Clean up stack
	lua_pop(L, 2);

	auto pActor = pGame->GetRawActorByName(name);
	lua_pushlightuserdata(L, pActor);

	return 1;
}

int Lua::GameLayer_SpawnActor(lua_State* L)
{
	const char* filepath = lua_tostring(L, -1);
	lua_getglobal(L, "GameLayer");
	IGameLayer* pGame = static_cast<IGameLayer*>(lua_touserdata(L, -1));

	lua_pop(L, 2);

	auto pActor = pGame->SpawnActor(filepath);
	lua_pushlightuserdata(L, pActor.get());

	return 1;
}

int Lua::GameLayer_CreateView(lua_State* L)
{
	std::string viewType = lua_tostring(L, -1);
	lua_getglobal(L, "GameLayer");
	IGameLayer* pGame = static_cast<IGameLayer*>(lua_touserdata(L, -1));

	lua_pop(L, 2);

	auto pView = pGame->CreateView(viewType);
	lua_pushlightuserdata(L, pView);

	return 1;
}
