#pragma once
/** \file PlaySoundEvent.h
 * Event for playing a sound 
 */
// Created by Billy Graban

#include "..\IEvent.h"
#include <Logic/Resources/IResource.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class ISound;

/** \class PlaySoundEvent
 * @brief Event for playing a sound 
 */
class PlaySoundEvent
	: public IEvent
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //
	// {A960D9A2-0B70-4B8E-9652-AECF978E7167}
	/** Event Id */
	static const EventId kEventId = 0xA960D9A2;

	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor 
	 * @param pSound The sound resource to play
	 */
	PlaySoundEvent(ISound* pSound);

	/** Default Destructor */
	~PlaySoundEvent();

	/** Get the Event Id associated with the event */
	EventId GetEventId() const override { return kEventId; }

	/** Get the name of the event (for logging purposes)
	 * @return "PlaySoundEvent"
	 */
	const char* GetName() const override { return "PlaySoundEvent"; }


private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	/** Sound resource to play */
	ISound* m_pSound;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	ISound* GetResource() { return m_pSound; }		/**< Get the sound resource to play */

};
}