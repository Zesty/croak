#pragma once
/** \file DestroyActorEvent.h
 * Event for destroying an actor 
 */
// Created by Billy Graban

#include "..\IEvent.h"

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class DestroyActorEvent
 * @brief Event for destroying an actor 
 */
class DestroyActorEvent
	: public IEvent
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //
	// {0B4BDB80-2AED-4E26-AD70-765D3A983468}
	/** Event Id */
	const static EventId kEventId = 0x0B4BDB80;

	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor 
	 * @param actorId The Id of the actor to be destroyed
	 */
	DestroyActorEvent(Id actorId);

	/** Default Destructor */
	~DestroyActorEvent();

	/** Get the Event Id associated with the event */
	EventId GetEventId() const override { return kEventId; }

	/** Get the name of the event (for logging purposes)
	 * @return "DestroyActorEvent"
	 */
	const char* GetName() const override { return "DestroyActorEvent"; }
	
	/** Push event data as a table onto the lua stack 
	 * [id] -> m_actorId
	 * [name] -> GetName()
	 */
	virtual bool PushLuaTable(lua_State* L) const override;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** Id of the actor to be destroyed */
	Id m_actorId;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	Id GetActorId() const { return m_actorId; }		/**< Get the id of the actor to be destroyed */

};
}