#include "DestroyActorEvent.h"

#include <Lua/lua.hpp>

using crk::DestroyActorEvent;

DestroyActorEvent::DestroyActorEvent(Id actorId)
	: m_actorId(actorId)
{
}

DestroyActorEvent::~DestroyActorEvent()
{
	
}

bool crk::DestroyActorEvent::PushLuaTable(lua_State* L) const
{
	lua_newtable(L);

	lua_pushnumber(L, m_actorId);
	lua_setfield(L, -2, "id");

	lua_pushstring(L, GetName());
	lua_setfield(L, -2, "name");

	return true;
}
