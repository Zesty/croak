#pragma once
/** \file CreateActorEvent.h
 * Event used to create an actor 
 */
// Created by Billy Graban

#include "..\IEvent.h"

#include <string>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class CreateActorEvent
 * @brief Event used to create an actor 
 */
class CreateActorEvent
	: public IEvent
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //
	// {D283A178-AD47-46CE-8388-5CED37A42253}
	/** Event Id */
	const static EventId kEventId = 0xD283A178;


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor 
	 * @param xmlFilepath Filepath to the xml that contains the actor definition
	 * @param isEnemy True if we are spawning an enemy (TODO: Not all actors are player/enemy)
	 */
	CreateActorEvent(const char* xmlFilepath, bool isEnemy);

	/** Default Destructor */
	~CreateActorEvent();

	/** Get the Event Id associated with the event */
	EventId GetEventId() const override { return kEventId; }

	/** Get the name of the event (for logging purposes)
	 * @return "CreateActor"
	 */
	const char* GetName() const override { return "CreateActor"; }

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	bool m_isEnemy;					/**< Is the new actor an enemy? */
	std::string m_xmlFilepath;		/**< Filepath to actor XML */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	const char* GetXmlFilepath() { return m_xmlFilepath.c_str(); }	/**< Get XML filepath */
	bool IsEnemy() { return m_isEnemy; }							/**< Get if the actor to spawn is an enemy */

};
}