#include "CreateActorEvent.h"

using crk::CreateActorEvent;

CreateActorEvent::CreateActorEvent(const char* xmlFilepath, bool isEnemy)
	: m_xmlFilepath(xmlFilepath)
	, m_isEnemy(isEnemy)
{
	
}

CreateActorEvent::~CreateActorEvent()
{
	
}