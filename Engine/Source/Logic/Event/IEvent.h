#pragma once
/** \file IEvent.h
 * Base Event Interface 
 */
// Created by Billy Graban

#include <Utils/Typedefs.h>

struct lua_State;

//! \namespace crk Contains all Croak Engine code
namespace crk
{

/** \class IEvent
 * @brief Base Event Interface 
 */
class IEvent
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //
	/** Alias for Event Ids*/
	using EventId = Id;

	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	IEvent();

	/** Default Destructor */
	virtual ~IEvent();

	/** Get the Event Id associated with the event */
	virtual EventId GetEventId() const = 0;

	/** Get the name of the event (for logging purposes) */
	virtual const char* GetName() const = 0;

	/** Push event data as a table onto the lua stack */
	virtual bool PushLuaTable(lua_State* L) const { return false; }


private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}