#include "EventDispatcher.h"

#include <assert.h>

#include <Utils/Log.h>
#include <Logic/Scripting/LuaManager.h>

using crk::EventDispatcher;

EventDispatcher::EventDispatcher()
	: m_pLuaManager(nullptr)
{
	
}

EventDispatcher::~EventDispatcher()
{
	
}

EventDispatcher* crk::EventDispatcher::Get()
{
	static EventDispatcher s_instance;
	return &s_instance;
}

size_t crk::EventDispatcher::AddEventListener(EventId id, std::function<void(IEvent*)> listener)
{
	// Grab the array of listeners associated with this event
	auto& listeners = m_eventListeners[id];
	
	// Loop through the vector looking for an open slot
	for (size_t index = 0; index < listeners.size(); ++index)
	{
		if (listeners[index] == nullptr)
		{
			listeners[index] = listener;
			return index;
		}
	}

	// No open slots
	listeners.emplace_back(listener);
	return listeners.size() - 1;
}

void crk::EventDispatcher::RemoveEventListener(EventId id, size_t index)
{
	auto& listeners = m_eventListeners[id];
	if (index < listeners.size())
	{
		listeners[index] = nullptr;
	}
	else
	{
		LOG(Error, "Invalid index received when attempting to remove event listener from dispatcher");
	}
}

void crk::EventDispatcher::QueueEvent(std::unique_ptr<IEvent> pEvent)
{
	m_queue.push_back(std::move(pEvent));
}

void crk::EventDispatcher::TriggerEvent(std::unique_ptr<IEvent> pEvent)
{
	auto& listeners = m_eventListeners[pEvent->GetEventId()];
	for (auto& listener : listeners)
	{
		if (listener != nullptr)
		{
			listener(pEvent.get());
		}
	}

	assert(m_pLuaManager);
	m_pLuaManager->SendEvent(pEvent.get());
}

void crk::EventDispatcher::ProcessEvents()
{

	// Move our queue over to a local variable.  This means that any events added throughout the course of this loop
	// will be handled next frame
	auto processingQueue = std::move(m_queue);
	for (auto& pEvent : processingQueue)
	{
		auto& listeners = m_eventListeners[pEvent->GetEventId()];
		for (auto& listener : listeners)
		{
			if (listener != nullptr)
			{
				listener(pEvent.get());
			}
		}
	}

	assert(m_pLuaManager);
	m_pLuaManager->SendEvents(processingQueue);
}
