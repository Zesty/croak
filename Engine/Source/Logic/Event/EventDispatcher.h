#pragma once
/** \file EventDispatcher.h
 * Register and dispatch events to listeners 
 */
// Created by Billy Graban

#include <unordered_map>
#include <vector>
#include <functional>
#include <memory>

#include <Logic/Event/IEvent.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class LuaManager;

/** \class EventDispatcher
 * @brief Register and dispatch events to listeners 
 */
class EventDispatcher
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //
	/** Alias for EventId */
	using EventId = IEvent::EventId;

	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //

	/** Singleton getter */
	static EventDispatcher* Get();

	/** Registers a std::function to be executed whenever a specific event is triggered
	 * @param id The EventId to listen for
	 * @param listener The function to execute once the event has been triggered
	 * @return The index of the listener when added to the list (can be used for removing listeners later)
	 */
	size_t AddEventListener(EventId id, std::function<void(IEvent*)> listener);

	/** Removes a listener from the event list
	 * @param id The EventId to remove the listener from
	 * @param index The index of the listener to remove
	 */
	void RemoveEventListener(EventId id, size_t index);

	/** Set an event to be triggered the next time the EventDispatcher processes events
	 * @param pEvent The event to trigger
	 */
	void QueueEvent(std::unique_ptr<IEvent> pEvent);

	/** Immediately trigger and event
	 * Warning!  Use this sparingly, as it is easy to get stuck in a feedback loop
	 * @param pEvent The event to trigger immediately
	 */
	void TriggerEvent(std::unique_ptr<IEvent> pEvent);

	/** Process all events that were queued last frame */
	void ProcessEvents();


private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** Each key in this map is associated with a vector of callbacks.  When an event is triggered this list
	 * will be looped through and every callback will be fired */
	std::unordered_map<EventId, std::vector<std::function<void(IEvent*)>>> m_eventListeners;

	/** Queue of events to handle on next ProcessEvents */
	std::vector<std::unique_ptr<IEvent>> m_queue;

	/** Used to push event data onto the stack so Lua can respond to events if desired */
	LuaManager* m_pLuaManager;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //

	/** Hidden default constructor */
	EventDispatcher();

	/** Hidden default destructor */
	~EventDispatcher();

public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	void SetLuaManager(LuaManager* pLuaManager) { m_pLuaManager = pLuaManager; }	/**< Set the Lua manager */


};
}