#pragma once
/** \file TiledMap.h
 * Renderable the contains the tile data from a Tiled map 
 */
// Created by Billy Graban

#include ".\IRenderable.h"

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class TiledMap
 * @brief Renderable the contains the tile data from a Tiled map 
 */
class TiledMap
	: public IRenderable
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor
	 * @param pOwner The actor that owns this renderable
	 */
	TiledMap(Actor* pOwner);

	/** Default Destructor */
	~TiledMap();

	/** Initialize component using an XML Element
	 * @param pData The data to use when initializing
	 * @return True if initialized successfully
	 */
	virtual bool Init(tinyxml2::XMLElement* pData) override;

	/** Get references to other components relevant to this one
	 * Logs error on failure
	 * @return True if successful
	 */
	virtual bool PostInit() override;

	/** Draw the component to the screen
	 * @param pGraphics The graphics object used for rendering
	 * @return True if rendering successful
	 */
	virtual bool Render(IGraphics* pGraphics) override;

	/** Get the rectangle (in pixel space) that the image will take on the screen
	 * Used for culling
	 * @return The rectangle (in pixel space) */
	virtual FRect GetDrawRect() override;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	struct LayerData;

	/** \struct MapData */
	/** Stores all data within the Tiled map */
	struct MapData
	{
		int m_width;		/**< Width of the map (in tiles) */
		int m_height;		/**< Height of the map (in tiles) */
		int m_tileWidth;	/**< Width of each tile (in pixels) */
		int m_tileHeight;	/**< Height of each tile (in pixels) */

		/** All layers (in render order) */
		std::vector<LayerData> m_layers;
	};

	/** \struct TilesetData */
	/** Contains data representing a single tileset */
	struct TilesetData
	{
		int m_firstGid;			/**< Id of the first tile in the set */
		std::string m_source;	/**< Path to the image used when rendering the tileset */

		int m_tileWidth;		/**< Width of each tile (in pixels) */
		int m_tileHeight;		/**< Height of each tile (in pixels) */
		int m_tileCount;		/**< Number of tiles total */
		int m_columns;			/**< Number of columns (width in tiles) */

		int m_imageWidth;		/**< Width of the image (in pixels) */
		int m_imageHeight;		/**< Height of the image (in pixels) */
	};


	/** \struct TileData */
	/** Stores id an position */
	struct TileData
	{
		int m_x;	/**< X position (in tiles) */
		int m_y;	/**< Y position (in tiles) */
		int m_id;	/**< Id of sub-image to draw */
	};

	/** \struct LayerData */
	/** All relevant data for each layer in a map */
	struct LayerData
	{
		int m_id;		/**< Id of the layer (no purpose I can find) */
		int m_width;	/**< Width of the layer (in tiles) */
		int m_height;	/**< Height of the layer (in tiles) */

		/** Collection of non-empty tiles within the layer */
		std::vector<TileData> m_tiles;
	};

	/** Map data to draw */
	MapData m_mapData;

	/** Tileset data to use
	 * This might need to be updated in the future when a map uses more than one tileset
	 */
	TilesetData m_tilesetData;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //
	/** Helper function for loading and parsing the map file
	 * @param source Path to the XML file
	 * @result True if load is successful
	 */
	bool LoadMap(const char* source);

public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}