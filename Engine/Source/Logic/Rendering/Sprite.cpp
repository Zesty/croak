#include "Sprite.h"

#include <Application/Graphics/IGraphics.h>
#include <Logic/Components/TransformComponent.h>
#include <Logic/Actor/Actor.h>
#include <Logic/Resources/ResourceCache.h>
#include <Utils/Log.h>

using crk::Sprite;

Sprite::Sprite(crk::Actor* pOwner)
	: IRenderable(pOwner)
	, m_useSourceRect(false)
{
	
}

Sprite::~Sprite()
{
	
}

bool crk::Sprite::Init(tinyxml2::XMLElement* pData)
{
	using namespace tinyxml2;
	const char* sSrc = pData->Attribute("src");

	if (sSrc)
	{
		m_pTexture = ResourceCache::Get()->AcquireResource<ITexture>(sSrc);
	}

	XMLElement* pTint = pData->FirstChildElement("Tint");

	// Set default tint (white)
	m_tint = FColor{ 1, 1, 1, 1 };

	if (pTint)
	{
		m_tint.r = pTint->FloatAttribute("r");
		m_tint.g = pTint->FloatAttribute("g");
		m_tint.b = pTint->FloatAttribute("b");
		m_tint.a = pTint->FloatAttribute("a");
	}

	m_originalColor = m_tint;

	return true;
}

bool crk::Sprite::PostInit()
{
	m_pTransform = m_pOwner->GetComponent<TransformComponent>();

	if (!m_pTransform)
	{
		LOG(Error, "Sprite Component requires Actor to have a Transform Component as well");
		return false;
	}

	return true;
}

bool crk::Sprite::Render(IGraphics* pGraphics)
{
	if (!m_pTexture)
	{
		LOG(Error, "Sprite Component has no texture to render");
		return false;
	}

	pGraphics->SetTextureTint(m_pTexture.get(), m_tint);
	pGraphics->SetTextureAlpha(m_pTexture.get(), m_tint.a);

	// Convert to screen space
	// Transform position is the center of the object
	f32 angle = m_pTransform->GetAngle();
	if (m_useSourceRect)
	{
		LOG(Warning, "Do I really need dimInPixels?");
		FVec2 pos = m_pTransform->GetPosition() * kPixelsPerMeter;
		FVec2 dim = FVec2(static_cast<f32>(m_dimInPixels.x), static_cast<f32>(m_dimInPixels.y));
		pGraphics->DrawTexture(m_pTexture.get(), m_sourceRect, FRect{ pos.x - (dim.x / 2), pos.y - (dim.y / 2), dim.x, dim.y });
	}
	else
	{
		pGraphics->DrawTexture(m_pTexture.get(), GetDrawRect(), angle);
	}

	return true;
}

crk::FRect crk::Sprite::GetDrawRect()
{
	FVec2 pos = m_pTransform->GetPosition() * kPixelsPerMeter;
	FVec2 dim = GetDimensions();

	FRect drawRect = FRect{ pos.x - (dim.x / 2), pos.y - (dim.y / 2), dim.x, dim.y };
	return drawRect;
}

crk::FRect crk::Sprite::GetSourceRect()
{
	FVec2 dim = GetDimensions();

	LOG(Todo, "No need to recalc source rect every frame.  Low hanging fruit");
	FRect sourceRect = FRect{ m_sourceRect.x / kPixelsPerMeter, m_sourceRect.y / kPixelsPerMeter, m_sourceRect.width / kPixelsPerMeter, m_sourceRect.height / kPixelsPerMeter };
	return sourceRect;
}

void crk::Sprite::SetTint(const FColor& tint)
{
	m_tint.r = tint.r;
	m_tint.g = tint.g;
	m_tint.b = tint.b;
}

crk::FVec2 crk::Sprite::GetDimensions()
{
	FVec2 dimensions;
	FVec2 scale = m_pTransform->GetScale();

	dimensions.x = m_pTexture->GetWidth() * scale.x;
	dimensions.y = m_pTexture->GetHeight() * scale.y;
	return dimensions;
}