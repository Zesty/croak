#include "IRenderable.h"

#include <Logic/Actor/Actor.h>

using crk::IRenderable;

IRenderable::IRenderable(crk::Actor* pOwner)
	: m_pOwner(pOwner)
	, m_pTransform(nullptr)
{
	
}

IRenderable::~IRenderable()
{
	
}