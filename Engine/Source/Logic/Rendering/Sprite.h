#pragma once
/** \file Sprite.h
 * A single image that can be rendered to the screen
 * This can be the whole image or a portion of the source
 */
// Created by Billy Graban

#include ".\IRenderable.h"

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class Sprite
 * @brief A single image that can be rendered to the screen
 * This can be the whole image or a portion of the source
 */
class Sprite
	: public IRenderable
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor
	 * @param pOwner The actor that owns this renderable
	 */
	Sprite(Actor* pOwner);

	/** Default Destructor */
	~Sprite();

	/** Initialize component using an XML Element
	 * @param pData The data to use when initializing
	 * @return True if initialized successfully
	 */
	virtual bool Init(tinyxml2::XMLElement* pData) override;

	/** Get references to other components relevant to this one
	 * Logs error on failure
	 * @return True if successful
	 */
	virtual bool PostInit() override;

	/** Draw the component to the screen
	 * @param pGraphics The graphics object used for rendering
	 * @return True if rendering successful
	 */
	virtual bool Render(IGraphics* pGraphics) override;

	/** Get the rectangle (in pixel space) that the image will take on the screen
	 * Used for culling
	 * @return The rectangle (in pixel space) */
	virtual FRect GetDrawRect() override;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	FColor m_tint;				/**< Tint of the Sprite */
	FColor m_originalColor;		/**< Starting color of the sprite (used for restoring) */

	// Dimensions in pixels (Used for sprite components that only draw a section of a sprite)
	IVec2 m_dimInPixels;		/**< Dimensions in pixels */

	FRect m_sourceRect;			/**< The portion of the texture to draw (in pixel space) */
	bool m_useSourceRect;		/**< True if the Sprite is only drawing a portion of the source */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	void SetTexture(std::shared_ptr<ITexture> pTexture) { m_pTexture = pTexture; }	/**< Set the texture to use when rendering */
	void SetAlpha(f32 alpha) { m_tint.a = alpha; }									/**< Set the transparency of the Sprite */
	void SetTint(const FColor& tint);												/**< Set the tint of the Sprite */
	void SetDimInPixels(IVec2 dim) { m_dimInPixels = dim; }							/**< Return the dimension of the Sprite to draw in pixels (maybe unused) */
	void SetImageSourceRect(FRect rect) { m_sourceRect = rect; }					/**< Set the source rect of the sub-image */
	void UseImageSourceRect() { m_useSourceRect = true; }							/**< Notify class to use source rect when drawing */

	ITexture* GetTexture() const { return m_pTexture.get(); }						/**< Get the texture */
	FColor GetTint() const { return m_tint; }										/**< Get the current tint */
	FColor GetOriginalColor() const { return m_originalColor; }						/**< Gets the original coloring */
	FVec2 GetDimensions();															/**< Gets scaled dimensions of sprite (in world units) */
	FRect GetSourceRect();															/**< Gets the source rect that defines the sub-image to draw */
};
}