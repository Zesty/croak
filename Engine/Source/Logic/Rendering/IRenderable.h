#pragma once
/** \file IRenderable.h
 * Core interface for all objects that can be rendered to screen 
 */
// Created by Billy Graban

#include <memory>

#include <Utils/TinyXml2/tinyxml2.h>
#include <Utils/Typedefs.h>
#include <Utils/CommonMath.h>
#include <Application/Graphics/Textures/ITexture.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class Actor;
	class IGraphics;
	class TransformComponent;

/** \class IRenderable
 * @brief Core interface for all objects that can be rendered to screen 
 */
class IRenderable
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor 
	 * @param pOwner The actor that owns this renderable 
	 */
	IRenderable(Actor* pOwner);

	/** Default Destructor */
	virtual ~IRenderable();

	/** Initialize component using an XML Element
	 * @param pData The data to use when initializing
	 * @return True if initialized successfully
	 */
	virtual bool Init(tinyxml2::XMLElement* pData) = 0;

	/** Get references to other components relevant to this one
	 * Logs error on failure
	 * @return True if successful
	 */
	virtual bool PostInit() = 0;

	/** Draw the component to the screen
	 * @param pGraphics The graphics object used for rendering
	 * @return True if rendering successful
	 */
	virtual bool Render(IGraphics* pGraphics) = 0;

	/** Get the rectangle (in pixel space) that the image will take on the screen
	 * Used for culling
	 * @return The rectangle (in pixel space) */
	virtual FRect GetDrawRect() = 0;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

protected:
	Actor* m_pOwner;						/**< The actor that owns this renderable */
	TransformComponent* m_pTransform;		/**< The transform of the actor that owns this renderable */
	std::shared_ptr<ITexture> m_pTexture;	/**< Texture to render to screen */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	std::shared_ptr<ITexture> GetTexture() { return m_pTexture; }		/**< Get the texture that will be rendered */
};
}