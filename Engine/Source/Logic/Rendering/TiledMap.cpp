#include "TiledMap.h"

#include <Utils/Log.h>
#include <Logic/Resources/ResourceCache.h>
#include <Application/Graphics/IGraphics.h>
#include <Logic/Actor/Actor.h>
#include <Logic/Components/TransformComponent.h>

using crk::TiledMap;

TiledMap::TiledMap(crk::Actor* pOwner)
	: IRenderable(pOwner)
{
	
}

TiledMap::~TiledMap()
{
	
}

bool crk::TiledMap::Init(tinyxml2::XMLElement* pData)
{
	const char* sSource = pData->Attribute("source");
	if (!sSource)
	{
		LOG(Error, "TiledMapRenderer Component must have a source attribute");
		return false;
	}

	if (!LoadMap(sSource))
	{
		LOG(Error, "Failed to load map");
		return false;
	}

	return true;
}

bool crk::TiledMap::PostInit()
{
	m_pTransform = m_pOwner->GetComponent<TransformComponent>();
	if (!m_pTransform)
	{
		LOG(Error, "TiledMapRenderer must be attached to an Actor with a TransformComponent");
		return false;
	}

	return true;
}

bool crk::TiledMap::Render(IGraphics* pGraphics)
{
	FRect srcRect;
	FRect destRect;
	f32 tileSize = 32;

	// Tile sizes never change
	destRect.width = tileSize;
	destRect.height = tileSize;
	srcRect.width = tileSize;
	srcRect.height = tileSize;

	FVec2 pos = m_pTransform->GetPosition();
	FVec2 pixelPos = pos * kPixelsPerMeter;

	// Get the raw texture outside of the hot loop
	ITexture* pRawTexture = m_pTexture.get();

	//i32 drawCount = 0;

	// TODO: Find the location of the camera relative to the map.  Use the dimensions of the world and a quick-access
	// data structure to simply loop through the tiles that are on screen
	for (auto& layer : m_mapData.m_layers)
	{
		for (auto& tile : layer.m_tiles)
		{
			// Calculate the destination rect
			destRect.x = tile.m_x * tileSize + pixelPos.x;
			destRect.y = tile.m_y * tileSize + pixelPos.y;

			// Early out
			if (destRect.x < -tileSize || destRect.x > kWindowWidth || destRect.y < -tileSize || destRect.y > kWindowHeight)
				continue;

			// Calculate source rect
			srcRect.x = (tile.m_id % m_tilesetData.m_columns) * tileSize;
			srcRect.y = (tile.m_id / m_tilesetData.m_columns) * tileSize;

			pGraphics->DrawTexture(pRawTexture, srcRect, destRect);
			//++drawCount;
		}
	}

	//LOG(Info, "%d tiles drawn this frame", drawCount);

	return true;
}

crk::FRect crk::TiledMap::GetDrawRect()
{
	FVec2 pixelPos = m_pTransform->GetPosition() * kPixelsPerMeter;
	return FRect(pixelPos.x, pixelPos.y,
		static_cast<f32>(m_mapData.m_width * m_mapData.m_tileWidth),
		static_cast<f32>(m_mapData.m_height * m_mapData.m_tileHeight));
}

bool crk::TiledMap::LoadMap(const char* source)
{
	using namespace tinyxml2;

	XMLDocument mapDocument;
	XMLError error = mapDocument.LoadFile(source);
	if (error != XML_SUCCESS)
	{
		LOG(Error, "Failed to load file (%s): %s", source, mapDocument.ErrorStr());
		return false;
	}

	XMLElement* pMapRoot = mapDocument.RootElement();

	m_mapData.m_width = pMapRoot->IntAttribute("width");
	m_mapData.m_height = pMapRoot->IntAttribute("height");
	m_mapData.m_tileWidth = pMapRoot->IntAttribute("tilewidth");
	m_mapData.m_tileHeight = pMapRoot->IntAttribute("tileheight");

	// Can contain N tilesets, we *should* loop through siblings
	XMLElement* pTileset = pMapRoot->FirstChildElement("tileset");

	m_tilesetData.m_firstGid = pTileset->IntAttribute("firstgid");
	std::string tilesetXmlPath = pTileset->Attribute("source");

	// Open our second file!
	std::string mapFile = source;
	std::string pathToTilesetXml = mapFile.substr(0, mapFile.rfind('/') + 1);
	pathToTilesetXml += tilesetXmlPath;

	XMLDocument tilesetDocument;
	error = tilesetDocument.LoadFile(pathToTilesetXml.c_str());
	if (error != XML_SUCCESS)
	{
		LOG(Error, "Failed to load file (%s): %s", pathToTilesetXml.c_str(), tilesetDocument.ErrorStr());
		return false;
	}

	XMLElement* pTilesetRoot = tilesetDocument.RootElement();
	XMLElement* pTilesetImage = pTilesetRoot->FirstChildElement("image");

	m_tilesetData.m_columns = pTilesetRoot->IntAttribute("columns");
	m_tilesetData.m_tileWidth = pTilesetRoot->IntAttribute("tilewidth");
	m_tilesetData.m_tileHeight = pTilesetRoot->IntAttribute("tileheight");
	m_tilesetData.m_tileCount = pTilesetRoot->IntAttribute("tilecount");

	m_tilesetData.m_source = pTilesetImage->Attribute("source");
	m_tilesetData.m_imageWidth = pTilesetImage->IntAttribute("width");
	m_tilesetData.m_imageHeight = pTilesetImage->IntAttribute("height");

	// Read in the layer
	// NOTE: a map can have many layers!  This should be looped
	LayerData layerData;
	for (XMLElement* pLayer = pMapRoot->FirstChildElement("layer"); pLayer != nullptr;
		pLayer = pLayer->NextSiblingElement("layer"))
	{
		layerData.m_id = pLayer->IntAttribute("id");
		layerData.m_width = pLayer->IntAttribute("width");
		layerData.m_height = pLayer->IntAttribute("height");

		// TODO: Promote?
		int mapSize = layerData.m_width * layerData.m_height;

		// No need to reserve, as many tiles may be empty.  Eat the CPU cost of reallocation for
		// memory savings
		//m_layerData.m_tiles.reserve(mapSize);

		XMLElement* pData = pLayer->FirstChildElement("data");
		std::string dataString = pData->GetText();

		std::stringstream sStream;
		int readCount = 0;
		sStream << dataString;

		// Grid locations used to store a tile's location.  This assumes that the tilemaps are going to sparse
		int mapX = 0;
		int mapY = 0;
		while (readCount < mapSize)
		{
			TileData tile;
			sStream >> tile.m_id;

			// Ignore comma/newline
			sStream.ignore();
			++readCount;

			// Store the coordinates
			tile.m_x = mapX;
			tile.m_y = mapY;

			// Update tile position
			++mapX;
			if (mapX >= layerData.m_width)
			{
				mapX = 0;
				++mapY;
			}

			// Id of 0 means no data.  I don't really like doing all of this work before throwing the tile out
			// but it must be done
			if (tile.m_id == 0)
				continue;

			// Tile was valid
			--tile.m_id;
			layerData.m_tiles.push_back(tile);
		}

		// Store this layer in array of layers in the map data
		// Move the layer to the map and clear out the previous tiles
		m_mapData.m_layers.emplace_back(std::move(layerData));
		layerData.m_tiles.clear();
	}

	// Load the image
	std::string pathToTilesetImage =
		pathToTilesetXml.substr(0, pathToTilesetXml.rfind('/') + 1);
	pathToTilesetImage += m_tilesetData.m_source;

	LOG(Todo, "Deal with tilesets that contain multiple tilesheets");
	m_pTexture = ResourceCache::Get()->AcquireResource<ITexture>(pathToTilesetImage.c_str());

	return true;
}
