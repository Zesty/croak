#pragma once
/** \file Box2DPhysics.h
 * Object that contains the Box2D Library object 
 */
// Created by Billy Graban

#include ".\IPhysicsSimulation.h"

#include <vector>
#include <unordered_map>

#include <Box2D/Box2D.h>

#include <Logic/Physics/IContactListener.h>
#include <Utils/CommonMath.h>

// TODO: kPixelsPerMeter
// TODO: Accumulator (Fixed Time Step)
// MAYBE: https://www.iforce2d.net/b2dtut/

/** How often the physics simulation should step (in seconds) */
constexpr f32 kPhysicsStep = 1.f / 60.f;

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class PhysicsDebugDraw
 * @brief Draw primitive shapes to display physics volumes and their states 
 */
class PhysicsDebugDraw : public b2Draw
{
private:
	/** Graphics object used to render primitives */
	IGraphics* m_pGraphics;

	/** Reusable array of points for drawing circles */
	std::vector<FVec2> m_points;

public:
	// Inherited via b2Draw

	/** Draws the outline of a polygon
	 * @param vertices Array of vertices (in world space)
	 * @param vertexCount The number of vertices in the array
	 * @param color Color to draw the outline
	 */
	virtual void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color) override;

	/** Draws a filled polygon
	 * @param vertices Array of vertices (in world space)
	 * @param vertexCount The number of vertices in the array
	 * @param color Color to fill the polygon
	 */
	virtual void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color) override;

	/** Draws the outline of a circle
	 * @param center The center of the circle (in world space)
	 * @param radius The radius of the circle (in units)
	 * @param color Color to draw the outline
	 */
	virtual void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color) override;

	/** Draws a filled circle
	 * @param center The center of the circle (in world space)
	 * @param radius The radius of the circle (in units)
	 * @param axis I have no idea what this does
	 * @param color Color to draw the outline
	 */
	virtual void DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color) override;

	/** Draw a line segment
	 * @param p1 Beginning of the line (in world space)
	 * @param p2 End of the line (in world space)
	 * @param color Color to draw the line
	 */
	virtual void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color) override;

	/** Draws a representation of a transform
	 * Not implemented yet
	 * @param xf The transform data*/
	virtual void DrawTransform(const b2Transform& xf) override;

	/** Draw a single point
	 * Not implemented yet
	 * @param p Position of the point (in world space)
	 * @param size The size of the point (in pixels?)
	 * @param color Color to draw the point
	 */
	virtual void DrawPoint(const b2Vec2& p, float32 size, const b2Color& color) override;

	void SetGraphics(IGraphics* pGraphics) { m_pGraphics = pGraphics; }		/**< Set the render object */
	IGraphics* GetGraphics() const { return m_pGraphics; }					/**< Get the render object */
};


/** \class Box2DPhysics
 * @brief Object that contains the Box2D Library object 
 */
class Box2DPhysics
	: public IPhysicsSimulation
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //

	/** Number of iterations the simulation takes to hone in on the resulting velocity */
	static const i32 kVelocityIterations = 8;
	/** Number of iterations the simulation takes to hone in on the resulting position */
	static const i32 kPositionIterations = 3;

	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor 
	 * @param gravity The starting gravity of the world
	 */
	Box2DPhysics(FVec2 gravity);

	/** Default Destructor */
	~Box2DPhysics();

	// Inherited via IPhysicsSimulation

	/** Initialize the physics simulation
	 * @param pGraphics Renderer for physics debug draw
	 * @return True if successful
	 */
	virtual bool Init(IGraphics* pGraphics) override;

	/** Update the simulation
	 * @param deltaSeconds The amount of time that has elapsed (in seconds) since the last frame
	 */
	virtual void Update(f32 deltaSeconds) override;

	/** Draws debug information for visual debugging */
	virtual void DrawDebug() override;

	/** Set which contact listener the physics simulation will use
	 * @param pListener Defines the callbacks when objects contact
	 */
	virtual void SetContactListener(IContactListener* pListener) override { m_world.SetContactListener(pListener); }

	/** Returns the flag associated with a category name */
	u16 GetCategoryFlag(const std::string& category);

	static b2Vec2 ConvertFVec(FVec2 vec);			/**< Helper function converting FVec2 to b2Vec2 */
	static FColor b2ColorToFColor(b2Color color);	/**< Helper function converting b2Color to FColor */
	static FVec2 b2Vec2ToFVec(b2Vec2 vec);			/**< Helper function converting b2Vec2 to FVec2  */
private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** Owned instance of the physics world */
	b2World m_world;

	/** Object responsible for drawing visual debugging information */
	PhysicsDebugDraw m_debugDraw;

	/** Stores the amount of time that has passed (used to decide when to step the world) */
	f32 m_accumulator;

	/** Lookup table of flag Ids associated with their string names */
	std::unordered_map<std::string, u16> m_categoryFlags;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	b2World* GetWorld() { return &m_world; }		/**< Get the Box2D world */



};



}