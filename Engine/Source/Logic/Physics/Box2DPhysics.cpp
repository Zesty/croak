#include "Box2DPhysics.h"

#include <Utils/Log.h>
#include <Application/Graphics/IGraphics.h>

using crk::Box2DPhysics;

Box2DPhysics::Box2DPhysics(crk::FVec2 gravity)
	: m_world(ConvertFVec(gravity))
	, m_accumulator(0.f)
{
	
}

Box2DPhysics::~Box2DPhysics()
{
}

bool crk::Box2DPhysics::Init(IGraphics* pGraphics)
{
	if (!pGraphics)
	{
		LOG(Error, "Failed to initialize Box2DPhysics.  No IGraphics object found");
		return false;
	}

	m_debugDraw.SetGraphics(pGraphics);
	m_world.SetDebugDraw(&m_debugDraw);
	m_debugDraw.SetFlags(b2Draw::e_shapeBit);
	return true;
}

void crk::Box2DPhysics::Update(f32 deltaSeconds)
{
	m_accumulator += deltaSeconds;

	if (m_accumulator >= kPhysicsStep)
	{
		//LOG(Info, "Physics Step");
		m_accumulator -= kPhysicsStep;
		m_world.Step(kPhysicsStep, kVelocityIterations, kPositionIterations);
	}
}

void crk::Box2DPhysics::DrawDebug()
{
	m_world.DrawDebugData();
}

u16 crk::Box2DPhysics::GetCategoryFlag(const std::string& category)
{
	size_t numCategories = m_categoryFlags.size();
	if (numCategories >= 16)
	{
		LOG(Critical, "Too many Box2DPhysics categories");
		return 0;
	}

	auto mapItr = m_categoryFlags.find(category);
	if (mapItr == m_categoryFlags.end())
	{
		mapItr = m_categoryFlags.emplace(category, (1 << numCategories)).first;
		LOG(Info, "Added new physics category: %s", category.c_str());
	}

	return mapItr->second;
}

b2Vec2 crk::Box2DPhysics::ConvertFVec(crk::FVec2 vec)
{
	return b2Vec2(vec.x, vec.y);
}

crk::FColor crk::Box2DPhysics::b2ColorToFColor(b2Color color)
{
	FColor result{ color.r, color.g, color.b, color.a };
	return result;
}

crk::FVec2 crk::Box2DPhysics::b2Vec2ToFVec(b2Vec2 vec)
{
	return FVec2{ vec.x, vec.y };
}

void crk::PhysicsDebugDraw::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{
	LOG(Warning, "PhysicsDebugDraw::DrawPolygon not implemented");
}

void crk::PhysicsDebugDraw::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{
	if (vertexCount == 4)
	{
		// This assumes that 0 is the min corner and 2 is the max corner
		FRect rect;
		rect.x = vertices[0].x;
		rect.y = vertices[0].y;
		rect.width = (vertices[2].x - vertices[0].x);
		rect.height = (vertices[2].y - vertices[0].y);

		FColor c = Box2DPhysics::b2ColorToFColor(color);
		c.a = 0.5f;
		m_pGraphics->FillRect(rect, c);
		return;
	}

	m_points.clear();
	for (int i = 0; i < vertexCount; ++i)
	{
		m_points.push_back(Box2DPhysics::b2Vec2ToFVec(vertices[i]));
	}

	m_points.push_back(m_points[0]);
	FColor c = Box2DPhysics::b2ColorToFColor(color);
	c.a = 0.5f;

	m_pGraphics->DrawPolygon(m_points, c);
}

void crk::PhysicsDebugDraw::DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color)
{
}

void crk::PhysicsDebugDraw::DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color)
{
	const int segments = 16;
	FColor c = Box2DPhysics::b2ColorToFColor(color);
	c.a = 0.5f;
	m_pGraphics->DrawCircle(Box2DPhysics::b2Vec2ToFVec(center), radius, segments, c);
}

void crk::PhysicsDebugDraw::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color)
{
}

void crk::PhysicsDebugDraw::DrawTransform(const b2Transform& xf)
{
}

void crk::PhysicsDebugDraw::DrawPoint(const b2Vec2& p, float32 size, const b2Color& color)
{
}
