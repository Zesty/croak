#pragma once
/** \file IContactListener.h
 * Base Object for contact listeners 
 */
// Created by Billy Graban

#include <Box2D/Box2D.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class IContactListener
 * @brief Base Object for contact listeners 
 */
class IContactListener
	: public b2ContactListener
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	IContactListener();

	/** Default Destructor */
	~IContactListener();

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}