#pragma once
/** \file IPhysicsSimulation.h
 * Base Interface for a physics simulation object 
 */
// Created by Billy Graban

#include <memory>

#include <Utils/Typedefs.h>
#include <Utils/CommonMath.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class IGraphics;
	class IContactListener;

/** \class IPhysicsSimulation
 * @brief Base Interface for a physics simulation object 
 */
class IPhysicsSimulation
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	IPhysicsSimulation();

	/** Default Destructor */
	virtual ~IPhysicsSimulation();

	/** Initialize the physics simulation 
	 * @param pGraphics Renderer for physics debug draw
	 * @return True if successful
	 */
	virtual bool Init(IGraphics* pGraphics) = 0;

	/** Update the simulation
	 * @param deltaSeconds The amount of time that has elapsed (in seconds) since the last frame
	 */
	virtual void Update(f32 deltaSeconds) = 0;

	/** Draws debug information for visual debugging */
	virtual void DrawDebug() = 0;

	/** Set which contact listener the physics simulation will use
	 * @param pListener Defines the callbacks when objects contact
	 */
	virtual void SetContactListener(IContactListener* pListener) { }

	/** Create the concrete physics system to be used
	 * @return Unique pointer to the physics system
	 */
	static std::unique_ptr<IPhysicsSimulation> Create(FVec2 gravity);


private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}