#include "IPhysicsSimulation.h"

#include <Logic/Physics/Box2DPhysics.h>

using crk::IPhysicsSimulation;

IPhysicsSimulation::IPhysicsSimulation()
{
	
}

IPhysicsSimulation::~IPhysicsSimulation()
{
	
}

std::unique_ptr<IPhysicsSimulation> crk::IPhysicsSimulation::Create(crk::FVec2 gravity)
{
	return std::make_unique<Box2DPhysics>(gravity);
}
