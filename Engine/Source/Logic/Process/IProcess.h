#pragma once
/** \file IProcess.h
 * Base class for all process objects.  A process is a function that takes more than one frame to complete 
 */
// Created by Billy Graban

#include <functional>
#include <memory>

#include <Logic/Actor/Actor.h>
#include <Utils/TinyXml2/tinyxml2.h>
#include <Utils/Typedefs.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class IProcess
 * @brief Base class for all process objects.  A process is a function that takes more than one frame to complete 
 */
class IProcess
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //

	/** \enum State Possible states of a process */
	enum class State
	{
		kUninitialized,		/**< Process has not been set up yet */
		kRunning,			/**< Process is actively running */
		kPaused,			/**< Process has been paused */
		kSucceeded,			/**< Process executed successfully */
		kFailed,			/**< Process was not successful */
		kAborted			/**< Process was forcibly stopped before completing */
	};


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor 
	 * @param pOwner The actor that owns this process
	 */
	IProcess(Actor* pOwner = nullptr);

	/** Default Destructor */
	virtual ~IProcess();

	/** Initializes the process
	 * Responsible for linking to necessary components and logging failures
	 * @return True if successful
	 */
	virtual bool Init() { return true; }

	/** Updates the process by a chunk of time
	 * @param deltaSeconds The amount of time (in seconds) that have elapsed since the last frame
	 */
	virtual void Update(f32 deltaSeconds) = 0;

	/** Loads process data from an XML element
	 * @param pData The root element of the process data
	 * @return True if successful
	 */
	virtual bool LoadXML(tinyxml2::XMLElement* pData) = 0;

	/** Calls the Abort callback (if any) */
	void OnAbort();

	/** Calls the Succeed callback (if any) */
	void OnSucceed();

	/** Calls the Fail callback (if any) */
	void OnFail();

	/** Remove the child process from this process
	 * @return Shared pointer to the child process
	 */
	std::shared_ptr<IProcess> RemoveChild();

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	State m_state;								/**< Current state of the process */
	std::function<void()> m_succeedCallback;	/**< Callback to execute when process succeeds */
	std::function<void()> m_abortCallback;		/**< Callback to execute when process is aborted */
	std::function<void()> m_failCallback;		/**< Callback to execute when process fails */
	std::shared_ptr<IProcess> m_pChild;			/**< Child process to add to the manager on successful execution */

protected:
	// --------------------------------------------------------------------- //
	// Protected Member Variables
	// --------------------------------------------------------------------- //
	Actor* m_pOwner;		/**< Owner of the process (if any) */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	void Succeed()	{ m_state = State::kSucceeded; }	/**< Sets the state of the process to Suceeded */
	void Fail()		{ m_state = State::kFailed; }		/**< Sets the state of the process to Failed */
	void Abort()	{ m_state = State::kAborted; }		/**< Sets the state of the process to Aborted */
	void Pause()	{ m_state = State::kPaused; }		/**< Sets the state of the process to Paused */
	void Resume()	{ m_state = State::kRunning; }		/**< Sets the state of the process to Running */

	State GetState() const	{ return m_state; }			/**< Get the state of the process */
	bool IsAlive() const	{ return (m_state == State::kRunning || m_state == State::kPaused); }	/**< Is the process alive? */
	bool IsDead() const		{ return (m_state == State::kFailed || m_state == State::kAborted || m_state == State::kSucceeded); }	/**< Is the process dead? */
	Actor* GetOwner() const { return m_pOwner; }		/**< Get the owner of the process */

	void SetOnSucceed(std::function<void()> callback) { m_succeedCallback = callback; }		/**< Set the callback for Succeed */
	void SetOnFail(std::function<void()> callback) { m_failCallback = callback; }			/**< Set the callback for Failed */
	void SetOnAbort(std::function<void()> callback) { m_abortCallback = callback; }			/**< Set the callback for Aborted */

	/** Attach a child that will be added to the manager once this process has executed successfully
	 * @param pProcess The process that will be added to the manager once this process is successful
	 */
	void AttachChild(std::shared_ptr<IProcess> pProcess) { m_pChild = pProcess; }
};
}