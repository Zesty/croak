#include "ProcessManager.h"

#include <Utils/Log.h>

using crk::ProcessManager;

ProcessManager::ProcessManager()
{
	
}

ProcessManager::~ProcessManager()
{
	AbortAllProcesses();
}

void crk::ProcessManager::UpdateProcesses(float deltaSeconds)
{
	size_t index = 0;
	while (index != m_pProcesses.size())
	{
		// TODO: If this ever slows down due to vector::erase, use Pop'n'Swap instead of erase!
		// This will destroy the order of processes but that really shouldn't matter, right?
		auto pProcess = m_pProcesses[index];
		IProcess::State state = pProcess->GetState();

		if (state == IProcess::State::kRunning)
		{
			pProcess->Update(deltaSeconds);
		}
		else if (state == IProcess::State::kUninitialized)
		{
			if (pProcess->Init())
			{
				pProcess->Resume();
			}
			else
			{
				// Remove the process!
				m_pProcesses.erase(m_pProcesses.cbegin() + index);  // TODO: Research if cbegin matters at all!
				continue;
			}
		}
		else if (pProcess->IsDead())
		{
			if (state == IProcess::State::kSucceeded)
			{
				pProcess->OnSucceed();
				auto pChild = pProcess->RemoveChild();
				if (pChild)
				{
					AttachProcess(pChild);
				}
			}
			else if (state == IProcess::State::kAborted)
			{
				pProcess->OnAbort();
			}
			else if (state == IProcess::State::kFailed)
			{
				pProcess->OnFail();
			}

			m_pProcesses.erase(m_pProcesses.cbegin() + index);
			continue;
		}

		// Only increment the index if a process was not removed from the vector
		++index;
	}
}

void crk::ProcessManager::AttachProcess(std::shared_ptr<IProcess> pProcess)
{
	// push_back(Copy)
	// emplace_back(Attempts to construct in-place)
	m_pProcesses.emplace_back(pProcess);
}

void crk::ProcessManager::AbortProcessesOnActor(Id actorId)
{
	size_t index = 0;
	while (index < m_pProcesses.size())
	{
		Actor* pOwner = m_pProcesses[index]->GetOwner();
		if (pOwner && pOwner->GetId() == actorId)
		{
			m_pProcesses.erase(m_pProcesses.cbegin() + index);
			continue;
		}

		++index;
	}
}

void crk::ProcessManager::AbortAllProcesses()
{
	for (auto& pProcess : m_pProcesses)
	{
		if (pProcess->IsAlive())
		{
			pProcess->Abort();
			pProcess->OnAbort();
		}

		pProcess.reset();
	}
	m_pProcesses.clear();
}
