#include "IProcess.h"


using crk::IProcess;

IProcess::IProcess(crk::Actor* pOwner)
	: m_state(State::kUninitialized)
	, m_pOwner(pOwner)
{
	
}

IProcess::~IProcess()
{
	
}

void crk::IProcess::OnAbort()
{
	if (m_abortCallback)
	{
		m_abortCallback();
	}
}

void crk::IProcess::OnSucceed()
{
	if (m_succeedCallback)
	{
		m_succeedCallback();
	}
}

void crk::IProcess::OnFail()
{
	if (m_failCallback)
	{
		m_failCallback();
	}
}

std::shared_ptr<IProcess> crk::IProcess::RemoveChild()
{
	auto pChild = m_pChild;
	m_pChild = nullptr;
	return pChild;
}
