#pragma once
/** \file SpriteFadeProcess.h
 * Fades a sprite's alpha over time 
 */
// Created by Billy Graban

#include "..\IProcess.h"

#include <Utils/Typedefs.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class Sprite;

/** \class SpriteFadeProcess
 * @brief Fades a sprite's alpha over time
 */
class SpriteFadeProcess
	: public IProcess
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Constructor 
	 * @param pOwner The actor that owns this process
	 * @param startAlpha The starting transparency of the sprite
	 * @param endAlpha The target transparency of the sprite
	 * @param duration How long (in seconds) it takes the process to fade the sprite
	 */
	SpriteFadeProcess(Actor* pOwner, f32 startAlpha, f32 endAlpha, f32 duration);

	/** Constructor
	 * @param pOwner The actor that owns this process
	 */
	SpriteFadeProcess(Actor* pOwner);

	/** Default Destructor */
	~SpriteFadeProcess();

	/** Updates the transparency of the target Sprite
	 * @param deltaSeconds The amount of time (in seconds) that have elapsed since the last frame
	 */
	void Update(f32 deltaSeconds) override;

	/** Initializes the process
	 * Responsible for linking to necessary components and logging failures
	 * @return True if successful
	 */
	bool Init() override;

	/** Gets the name of the process (for logging purposes)
	 * @return "SpriteFadeProcess"
	 */
	static const char* GetName() { return "SpriteFadeProcess"; }

	/** Loads process data from an XML element
	 * @param pData The root element of the process data
	 * @return True if successful
	 */
	bool LoadXML(tinyxml2::XMLElement* pData) override;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	f32 m_startAlpha;			/**< The transparency of the Sprite at the start of the process */
	f32 m_endAlpha;				/**< The transparency of the Sprite at the end of the process */
	f32 m_duration;				/**< How long (in seconds) it takes for the process to execute */
	f32 m_timer;				/**< Measures elapsed time */
	Sprite* m_pSprite;			/**< The Sprite to modify */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}