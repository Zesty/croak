#pragma once
/** \file ScaleProcess.h
 * Scales a transform over time 
 */
// Created by Billy Graban

#include "..\IProcess.h"
#include <Utils/CommonMath.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class TransformComponent;

/** \class ScaleProcess
 * @brief Scales a transform over time 
 */
class ScaleProcess
	: public IProcess
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Constructor
	 * @param pOwner The actor that owns this process
	 * @param start The starting scale of the sprite
	 * @param end The target scale of the sprite
	 * @param duration How long (in seconds) it takes the process to scale the sprite
	 */
	ScaleProcess(Actor* pOwner, FVec2 start, FVec2 end, f32 duration);

	/** Default Constructor */
	ScaleProcess(Actor* pOwner);

	/** Default Destructor */
	~ScaleProcess();

	/** Updates the scale of the transfrorm
	 * @param deltaSeconds The amount of time (in seconds) that have elapsed since the last frame
	 */
	void Update(f32 deltaSeconds) override;
	
	/** Initializes the process
	 * Responsible for linking to necessary components and logging failures
	 * @return True if successful
	 */
	bool Init() override;

	/** Gets the name of the process (for logging purposes)
	 * @return "ScaleProcess"
	 */
	static const char* GetName() { return "ScaleProcess"; }

	/** Loads process data from an XML element
	 * @param pData The root element of the process data
	 * @return True if successful
	 */
	bool LoadXML(tinyxml2::XMLElement* pData) override;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	TransformComponent* m_pTransform;		/**< The transform to scale */
	FVec2 m_start;							/**< Starting scale of the transform */
	FVec2 m_end;							/**< Ending scale of the transform */
	f32 m_duration;							/**< How long (in seconds) it takes for the process to execute */
	f32 m_timer;							/**< Measures elapsed time internally */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}