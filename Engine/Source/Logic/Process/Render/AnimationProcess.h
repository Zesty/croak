#pragma once
/** \file AnimationProcess.h
 * Animates a Sprite component by changing textures
 * Currently only supports whole-texture frames, meaning that source rects are not yet supported
 */
// Created by Billy Graban

#include <vector>
#include <string>

#include "../IProcess.h"
#include <Utils/Typedefs.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class Sprite;
	class ITexture;
	class IResource;

/** \class AnimationProcess
 * @brief Animates a Sprite component by changing textures
 */
class AnimationProcess
	: public IProcess
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor 
	 * @param pOwner The actor that owns this process
	 * @param pAnimationResource The resource containing the XML data to use when initializing
	 * the animation
	 */
	AnimationProcess(Actor* pOwner, IResource* pAnimationResource);

	/** Constructor
	 * @param pOwner The actor that owns this process
	 */
	AnimationProcess(Actor* pOwner);

	/** Default Destructor */
	~AnimationProcess();

	// Inherited via IProcess

	/** Updates the timer and changes frames of the animation
	 * @param deltaSeconds The amount of time (in seconds) that have elapsed since the last frame
	 */
	void Update(float deltaSeconds) override;

	/** Gets the name of the process (for logging purposes)
	 * @return "AnimationProcess"
	 */
	static const char* GetName() { return "AnimationProcess"; }

	/** Initializes the process
	 * Responsible for linking to necessary components and logging failures
	 * @return True if successful
	 */
	bool Init() override;

	/** Loads process data from an XML element
	 * @param pData The root element of the process data
	 * @return True if successful
	 */
	bool LoadXML(tinyxml2::XMLElement* pData) override;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** \struct Frame A single frame of animation */
	struct Frame
	{
		std::shared_ptr<ITexture> m_pTexture;	/**< Texture for the frame */
		f32 m_duration;							/**< How long (in seconds) the frame will last for */


		/** Default constructor */
		Frame();
	};

	/** \struct Animation A collection of frames and other animation-related data */
	struct Animation
	{
		std::vector<Frame> m_frames;	/**< Collection of frames */
		bool m_isLooping;				/**< True if the animation should loop */
		
		/** Default constructor */
		Animation();
	};

	Sprite* m_pSprite;									/**< The target sprite to change the image of */
	Animation m_animation;								/**< Animation to use for the process */
	std::shared_ptr<IResource> m_pAnimationResource;	/**< Resource containing XML data of the animation and its frames */

	f32 m_curFrameTimer;								/**< Measures time spent in the current frame */
	i8 m_curFrameIndex;									/**< The current frame index */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //

	/** Helper function for loading animation data from m_pAnimationResource 
	 * @return True on successful load
	 */
	bool LoadAnimation();

public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
};
}