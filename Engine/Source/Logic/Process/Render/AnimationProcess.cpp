#include "AnimationProcess.h"

#include <Utils/Log.h>
#include <Utils/TinyXml2/tinyxml2.h>

#include <Logic/Resources/IResource.h>
#include <Logic/Resources/ResourceCache.h>

#include <Logic/Components/Render/RenderComponent.h>
#include <Logic/Rendering/Sprite.h>

using crk::AnimationProcess;

AnimationProcess::AnimationProcess(crk::Actor* pOwner, crk::IResource* pAnimationResource)
	: IProcess(pOwner)
	, m_pAnimationResource(pAnimationResource)
	, m_curFrameIndex(0)
	, m_curFrameTimer(0.0f)
	, m_pSprite(nullptr)
{
	
}

AnimationProcess::AnimationProcess(crk::Actor* pOwner)
	: IProcess(pOwner)
		, m_pAnimationResource(nullptr)
		, m_curFrameIndex(0)
		, m_curFrameTimer(0.0f)
		, m_pSprite(nullptr)
{
}

AnimationProcess::~AnimationProcess()
{
	
}

void crk::AnimationProcess::Update(float deltaSeconds)
{
	m_curFrameTimer -= deltaSeconds;

	if (m_curFrameTimer <= 0)
	{
		++m_curFrameIndex;

		// Reset if looping
		if ((size_t)m_curFrameIndex >= m_animation.m_frames.size())
		{
			if (m_animation.m_isLooping)
			{
				m_curFrameIndex = 0;
			}
			else
			{
				// Done with process if animation is complete
				Succeed();
				return;
			}
		}

		m_curFrameTimer += m_animation.m_frames[m_curFrameIndex].m_duration;

		// Update the sprite
		m_pSprite->SetTexture(m_animation.m_frames[m_curFrameIndex].m_pTexture);
	}
}

bool crk::AnimationProcess::Init()
{
	m_pSprite = m_pOwner->GetComponent<RenderComponent>()->GetRenderable<Sprite>();

	if (!m_pSprite)
	{
		LOG(Error, "Actor with AnimationProcess must also have a SpriteComponent");
		return false;
	}

	if (!LoadAnimation())
	{
		LOG(Error, "Failed to load animation file: %s", m_pAnimationResource->GetName().c_str());
		return false;
	}

	return true;
}

bool crk::AnimationProcess::LoadXML(tinyxml2::XMLElement* pData)
{
	const char* pSrc = pData->Attribute("src");
	if (!pSrc)
	{
		LOG(Error, "AnimationProcess must have a 'src' attribute");
		return false;
	}

	m_pAnimationResource = ResourceCache::Get()->AcquireResource(pSrc);

	if (!m_pAnimationResource)
	{
		LOG(Error, "Failed to acquire animation resource: %s", pSrc);
		return false;
	}

	return true;
}

bool crk::AnimationProcess::LoadAnimation()
{
	using namespace tinyxml2;

	XMLDocument doc;
	XMLError error = doc.Parse(reinterpret_cast<const char*>(
		m_pAnimationResource->GetData().data()), m_pAnimationResource->GetData().size());

	if (error != XML_SUCCESS)
	{
		LOG(Error, "Failed to parse file %s -- %s", m_pAnimationResource->GetName().c_str(), doc.ErrorStr());
		return false;
	}

	// Just for funsies
	using namespace std::string_literals;
	XMLElement* pRoot = doc.RootElement();
	if (pRoot->Name() != "Animation"s)
	{
		LOG(Error, "Animation file must have root element named 'Animation'");
		return false;
	}

	// Animation attributes
	m_animation.m_isLooping = pRoot->BoolAttribute("isLooping");
	f32 defaultDuration = pRoot->FloatAttribute("defaultDuration", 0.0f);

	// Frames
	for (XMLElement* pFrame = pRoot->FirstChildElement("Frame"); pFrame != nullptr;
		pFrame = pFrame->NextSiblingElement("Frame"))
	{
		Frame frame;
		frame.m_duration = pFrame->FloatAttribute("duration", defaultDuration);

		std::string source = pFrame->Attribute("src");
		frame.m_pTexture = ResourceCache::Get()->AcquireResource<ITexture>(source.c_str());

		m_animation.m_frames.emplace_back(frame);
	}

	m_pSprite->SetTexture(m_animation.m_frames[0].m_pTexture);
	m_curFrameTimer = m_animation.m_frames[0].m_duration;
	m_curFrameIndex = 0;

	return true;
}

crk::AnimationProcess::Frame::Frame()
	: m_duration(0.0f)
	, m_pTexture(nullptr)
{
}

crk::AnimationProcess::Animation::Animation()
	: m_frames()
	, m_isLooping(false)
{
}
