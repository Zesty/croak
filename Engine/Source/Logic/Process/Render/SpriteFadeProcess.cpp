#include "SpriteFadeProcess.h"

#include <Utils/Log.h>
#include <Logic/Components/Render/RenderComponent.h>
#include <Logic/Rendering/Sprite.h>

using crk::SpriteFadeProcess;

SpriteFadeProcess::SpriteFadeProcess(crk::Actor* pOwner, f32 startAlpha, f32 endAlpha, f32 duration)
	: IProcess(pOwner)
	, m_startAlpha(startAlpha)
	, m_endAlpha(endAlpha)
	, m_duration(duration)
	, m_pSprite(nullptr)
	, m_timer(0.0f)
{
	
}
SpriteFadeProcess::SpriteFadeProcess(crk::Actor* pOwner)
	: IProcess(pOwner)
	, m_startAlpha(0.0f)
	, m_endAlpha(0.0f)
	, m_duration(0.0f)
	, m_pSprite(nullptr)
	, m_timer(0.0f) 
{

}
SpriteFadeProcess::~SpriteFadeProcess()
{
	
}

void crk::SpriteFadeProcess::Update(f32 deltaSeconds)
{
	m_timer += deltaSeconds;

	if (m_timer < m_duration)
	{
		f32 ratio = m_timer / m_duration;
		f32 alpha = Lerp(m_startAlpha, m_endAlpha, ratio);

		m_pSprite->SetAlpha(alpha);
	}
	else
	{
		m_pSprite->SetAlpha(m_endAlpha);
		Succeed();
	}
}

bool crk::SpriteFadeProcess::Init()
{
	m_pSprite = m_pOwner->GetComponent<RenderComponent>()->GetRenderable<Sprite>();

	if (!m_pSprite)
	{
		LOG(Error, "SpriteFadeProcess must be attached to an actor with a Sprite Renderable");
		return false;
	}

	return true;
}

bool crk::SpriteFadeProcess::LoadXML(tinyxml2::XMLElement* pData)
{
	LOG(Todo, "Allow SpriteFadeProcess to randomize values");
	m_startAlpha = pData->FloatAttribute("start");
	m_endAlpha = pData->FloatAttribute("end");
	m_duration = pData->FloatAttribute("duration");

	return true;
}
