#include "ScaleProcess.h"

#include <Utils/Log.h>
#include <Logic/Components/TransformComponent.h>

using crk::ScaleProcess;

crk::ScaleProcess::ScaleProcess(Actor* pOwner)
	: IProcess(pOwner)
	, m_start()
	, m_end()
	, m_duration(0.f)
	, m_timer(0.f)
	, m_pTransform(nullptr)
{
}

crk::ScaleProcess::ScaleProcess(Actor* pOwner, FVec2 start, FVec2 end, f32 duration)
	: IProcess(pOwner)
	, m_start(start)
	, m_end(end)
	, m_duration(duration)
	, m_timer(0.f)
	, m_pTransform(nullptr) 
{
}

ScaleProcess::~ScaleProcess()
{
	
}

void crk::ScaleProcess::Update(f32 deltaSeconds)
{
	m_timer += deltaSeconds;

	if (m_timer < m_duration)
	{
		FVec2 current;
		f32 ratio = m_timer / m_duration;
		current.x = Lerp(m_start.x, m_end.x, ratio);
		current.y = Lerp(m_start.y, m_end.y, ratio);

		m_pTransform->SetScale(current);
	}
	else
	{
		m_pTransform->SetScale(m_end);
		Succeed();
	}
}

bool crk::ScaleProcess::Init()
{
	m_pTransform = m_pOwner->GetComponent<TransformComponent>();
	if (!m_pTransform)
	{
		LOG(Error, "ScaleProcess must be attached to Actor that has a TransformComponent");
		return false;
	}

	return true;
}

bool crk::ScaleProcess::LoadXML(tinyxml2::XMLElement* pData)
{
	LOG(Error, "ScaleProcess::LoadXML - Not implemented yet");
	return false;
}
