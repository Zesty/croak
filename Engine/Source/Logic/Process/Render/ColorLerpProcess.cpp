#include "ColorLerpProcess.h"

#include <Utils/Log.h>
#include <Logic/Components/Render/RenderComponent.h>
#include <Logic/Rendering/Sprite.h>

using crk::ColorLerpProcess;

ColorLerpProcess::ColorLerpProcess(crk::Actor* pOwner)
	: IProcess(pOwner)
	, m_start()
	, m_end()
	, m_duration(0)
	, m_pSprite(nullptr)
	, m_timer(0.f)
{
	
}

crk::ColorLerpProcess::ColorLerpProcess(crk::Actor* pOwner, crk::FColor start, crk::FColor end, f32 duration)
	: IProcess(pOwner)
	, m_start(start)
	, m_end(end)
	, m_duration(duration)
	, m_pSprite(nullptr)
	, m_timer(0.f)
{
}

ColorLerpProcess::~ColorLerpProcess()
{
	
}

void crk::ColorLerpProcess::Update(f32 deltaSeconds)
{
	m_timer += deltaSeconds;

	if (m_timer < m_duration)
	{
		FColor current;
		f32 ratio = m_timer / m_duration;
		current.r = Lerp(m_start.r, m_end.r, ratio);
		current.g = Lerp(m_start.g, m_end.g, ratio);
		current.b = Lerp(m_start.b, m_end.b, ratio);

		m_pSprite->SetTint(current);
	}
	else
	{
		m_pSprite->SetTint(m_end);
		Succeed();
	}
}

bool crk::ColorLerpProcess::Init()
{
	m_pSprite = m_pOwner->GetComponent<RenderComponent>()->GetRenderable<Sprite>();
	if (!m_pSprite)
	{
		LOG(Error, "ColorLerpProcess must be attached to an Actor with a Sprite Renderable");
		return false;
	}

	return true;
}

bool crk::ColorLerpProcess::LoadXML(tinyxml2::XMLElement* pData)
{
	LOG(Error, "ColorLerpProcess::LoadXML - Not implemented yet");
	return false;
}
