#pragma once
/** \file ColorLerpProcess.h
 * Interpolates the tint of a Sprite over time 
 */
// Created by Billy Graban

#include "..\IProcess.h"

#include <Utils/CommonMath.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class Sprite;

/** \class ColorLerpProcess
 * @brief Interpolates the tint of a Sprite over time
 */
class ColorLerpProcess
	: public IProcess
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //

	/** Constructor
	 * @param pOwner The actor that owns this process
	 * @param start The starting color of the sprite
	 * @param end The target color of the sprite
	 * @param duration How long (in seconds) it takes the process to interpolate the sprite's tint
	 */
	ColorLerpProcess(Actor* pOwner, FColor start, FColor end, f32 duration);
	/** Default Constructor */
	ColorLerpProcess(Actor* pOwner);

	/** Default Destructor */
	~ColorLerpProcess();

	/** Updates the timer to interpolate the Sprite tint
	 * @param deltaSeconds The amount of time (in seconds) that have elapsed since the last frame
	 */
	void Update(f32 deltaSeconds) override;

	/** Initializes the process
	 * Responsible for linking to necessary components and logging failures
	 * @return True if successful
	 */
	bool Init() override;

	/** Gets the name of the process (for logging purposes)
	 * @return "ColorLerpProcess"
	 */
	static const char* GetName() { return "ColorLerpProcess"; }

	/** Loads process data from an XML element
	 * @param pData The root element of the process data
	 * @return True if successful
	 */
	bool LoadXML(tinyxml2::XMLElement* pData) override;
private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	Sprite* m_pSprite;		/**< The sprite whose tint will be modified */
	FColor m_start;			/**< The starting tint of the sprite */
	FColor m_end;			/**< The final tint of the sprite */
	f32 m_duration;			/**< The length of time (in seconds) that it takes the process to execute */
	f32 m_timer;			/**< Measures time elapsed internally */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}