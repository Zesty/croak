#include "DelayProcess.h"

#include <Utils/Log.h>

using crk::DelayProcess;

crk::DelayProcess::DelayProcess(crk::Actor* pOwner, f32 delay)
	: IProcess(pOwner)
	, m_delay(delay)
{
	LOG(Todo, "Update LoadXml/Init documentation with valid Xml attributes and child element data");
}

crk::DelayProcess::DelayProcess(Actor* pOwner)
	: IProcess(pOwner)
	, m_delay(0.0f)
{
}

DelayProcess::~DelayProcess()
{
	
}

void crk::DelayProcess::Update(f32 deltaSeconds)
{
	// TODO: When the user presses a key, wait 2 seconds and then play a sound effect
	// - Make sure that the process manager is updated correctly
	// - When the user presses a key create the new process and add it to the manager
	// - Set the OnSuccess callback to play a sound
	m_delay -= deltaSeconds;
	if (m_delay <= 0)
	{
		Succeed();
	}
}

bool crk::DelayProcess::LoadXML(tinyxml2::XMLElement* pData)
{
	using namespace tinyxml2;
	const XMLAttribute* pDelay = pData->FindAttribute("delay");
	if (pDelay)
	{
		m_delay = pDelay->FloatValue();
		return true;
	}

	XMLElement* pRandom = pData->FirstChildElement("Random");
	if (pRandom)
	{
		// TODO: Parse element and randomize the result!
	}

	LOG(Error, "DelayProcess must have a 'delay' Attribute");
	return false;
}
