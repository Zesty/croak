#pragma once
/** \file DelayProcess.h
 * Delays for a number of seconds 
 */
// Created by Billy Graban

#include "..\IProcess.h"

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class DelayProcess
 * @brief Delays for a number of seconds 
 */
class DelayProcess
	: public IProcess
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Constructor 
	 * @param pOwner The actor that owns the process
	 * @param delay The number of seconds to wait
	 */
	DelayProcess(Actor* pOwner, f32 delay);

	/** Default Constructor
	 * @param pOwner The actor that owns the process
	 */
	DelayProcess(Actor* pOwner);

	/** Default Destructor */
	~DelayProcess();

	/** Updates the process by a chunk of time
	 * @param deltaSeconds The amount of time (in seconds) that have elapsed since the last frame
	 */
	void Update(f32 deltaSeconds) override;

	/** Gets the name of the process (for logging purposes)
	 * @return "DelayProcess"
	 */
	static const char* GetName() { return "DelayProcess"; }

	/** Loads process data from an XML element
	 * @param pData The root element of the process data
	 * @return True if successful
	 */
	bool LoadXML(tinyxml2::XMLElement* pData) override;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** The amount of time to wait (in seconds) before successfully executing the process */
	f32 m_delay;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}