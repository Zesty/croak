#pragma once
/** \file ProcessManager.h
 * Manages lifetime of all processes and handles child management
 */
// Created by Billy Graban

#include <memory>
#include <vector>

#include "IProcess.h"

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class ProcessManager
 * @brief Manages lifetime of all processes and handles child management 
 */
class ProcessManager
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	ProcessManager();

	/** Default Destructor */
	~ProcessManager();

	/** Update all processes that are currently active
	 * @param deltaSeconds The amount of time (in seconds) that has elapsed since last frame
	 */
	void UpdateProcesses(float deltaSeconds);

	/** Add a new process to the manager
	 * @param pProcess The process to add
	 */
	void AttachProcess(std::shared_ptr<IProcess> pProcess);

	/** Walks through all active processes and aborts any process owned by an actor
	 * @param actorId The Id of the actor whose processes should be aborted
	 */
	void AbortProcessesOnActor(Id actorId);

	/** Abort every single process in the manager */
	void AbortAllProcesses();

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** Collection of processes */
	std::vector<std::shared_ptr<IProcess>> m_pProcesses;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}