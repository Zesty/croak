#include "MoveProcess.h"

#include <Logic/Components/TransformComponent.h>
#include <Logic/Components/Physics/Box2DPhysicsComponent.h>
#include <Utils/Log.h>
#include <Utils/TinyXml2/XmlLoaderUtil.h>

using crk::MoveProcess;

MoveProcess::MoveProcess(crk::Actor* pOwner, crk::FVec2 moveSpeed)
	: IProcess(pOwner)
	, m_moveSpeed(moveSpeed)
	, m_pTransform(nullptr)
	, m_pBody(nullptr)
	, m_targetBody(false)
{
	
}

MoveProcess::MoveProcess(crk::Actor* pOwner)
	: IProcess(pOwner)
	, m_moveSpeed()
	, m_pTransform(nullptr)
	, m_pBody(nullptr)
	, m_targetBody(false)
{

}

MoveProcess::~MoveProcess()
{
	
}

bool crk::MoveProcess::Init()
{
	if (m_targetBody)
	{
		m_pBody = m_pOwner->GetComponent<Box2DPhysicsComponent>();

		if (!m_pBody)
		{
			LOG(Error, "Move process is targeting a physics body, but has no Box2DPhysicsComponent");
			return false;
		}
	}
	else
	{
		m_pTransform = m_pOwner->GetComponent<TransformComponent>();

		if (!m_pTransform)
		{
			LOG(Error, "MoveProcess must be attached to Actor with a TransformComponent");
			return false;
		}
	}

	return true;
}

void crk::MoveProcess::Update(float deltaSeconds)
{
	if (m_targetBody)
	{
		m_pBody->SetLinearVelocity(m_moveSpeed);
	}
	else
	{
		m_pTransform->Move(m_moveSpeed * deltaSeconds);
	}
}

bool crk::MoveProcess::LoadXML(tinyxml2::XMLElement* pData)
{
	using namespace tinyxml2;

	// If the process targets the physics or the transform
	const char* sTarget = pData->Attribute("target");
	if (sTarget != nullptr)
	{
		m_targetBody = (sTarget == std::string("body"));
	}

	f32 x = XmlLoaderUtil::ReadFloatValue(pData, "X");
	f32 y = XmlLoaderUtil::ReadFloatValue(pData, "Y");

	m_moveSpeed = FVec2(x, y);
 	return true;
}
