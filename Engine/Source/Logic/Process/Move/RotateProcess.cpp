#include "RotateProcess.h"

#include <Utils/Log.h>
#include <Logic/Components/Physics/Box2DPhysicsComponent.h>
#include <Logic/Components/TransformComponent.h>
#include <Utils/TinyXml2/XmlLoaderUtil.h>

using crk::RotateProcess;

RotateProcess::RotateProcess(crk::Actor* pOwner, f32 rotateSpeed)
	: IProcess(pOwner)
	, m_rotateSpeed(rotateSpeed)
	, m_targetBody(false)
	, m_pBodyComponent(nullptr)
	, m_pTransform(nullptr)
{
	
}

RotateProcess::~RotateProcess()
{
	
}

bool crk::RotateProcess::Init()
{
	if (m_targetBody)
	{
		m_pBodyComponent = m_pOwner->GetComponent<Box2DPhysicsComponent>();

		if (!m_pBodyComponent)
		{
			LOG(Error, "Move process is targeting a physics body, but has no Box2DPhysicsComponent");
			return false;
		}
	}
	else
	{
		m_pTransform = m_pOwner->GetComponent<TransformComponent>();

		if (!m_pTransform)
		{
			LOG(Error, "MoveProcess must be attached to Actor with a TransformComponent");
			return false;
		}
	}

	return true;
}

void crk::RotateProcess::Update(f32 deltaSeconds)
{
	if (m_targetBody)
	{
		m_pBodyComponent->SetAngularVelocity(m_rotateSpeed);
	}
	else
	{
		m_pTransform->Rotate(m_rotateSpeed * deltaSeconds);
	}
}

bool crk::RotateProcess::LoadXML(tinyxml2::XMLElement* pData)
{
	const char* sTarget = pData->Attribute("target");
	if (sTarget)
		m_targetBody = sTarget == std::string("body");
	else
		m_targetBody = false;

	m_rotateSpeed = XmlLoaderUtil::ReadFloatValue(pData, "Speed");
	//m_rotateSpeed = pData->FloatAttribute("speed");

	return true;
}
