#pragma once
/** \file MoveProcess.h
 * Move an object in a direction over time
 * This can be used in two different ways:
 * - Move a transform in units per second
 * - Set linear velocity of a physics body
 */
// Created by Billy Graban

#include <Utils/CommonMath.h>
#include <Utils/Squirrel3.h>

#include "..\IProcess.h"

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class TransformComponent;
	class Box2DPhysicsComponent;

/** \class MoveProcess
 * @brief Move an object in a direction over time
 * This can be used in two different ways:
 * - Move a transform in units per second
 * - Set linear velocity of a physics body
 */
class MoveProcess
	: public IProcess
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor 
	 * @param pOwner The actor that owns this process
	 * @param moveSpeed The direction and magnitude of the movement
	 */
	MoveProcess(Actor* pOwner, FVec2 moveSpeed);

	/** Default Constructor
	 * @param pOwner The actor that owns this process
	 */
	MoveProcess(Actor* pOwner);

	/** Default Destructor */
	~MoveProcess();

	// Inherited via IProcess

	/** Initializes the process
	 * Responsible for linking to necessary components and logging failures
	 * @return True if successful
	 */
	bool Init() override;

	/** Update the target's position/velocity
	 * @param deltaSeconds The amount of time (in seconds) that have elapsed since the last frame
	 */
	void Update(float deltaSeconds) override;

	/** Gets the name of the process (for logging purposes)
	 * @return "DelayProcess"
	 */
	static const char* GetName() { return "MoveProcess"; }

	/** Loads process data from an XML element
	 * @param pData The root element of the process data
	 * @return True if successful
	 */
	bool LoadXML(tinyxml2::XMLElement* pData) override;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	TransformComponent* m_pTransform;	/**< The transform to move (if any) */
	Box2DPhysicsComponent* m_pBody;		/**< The physics body to move (if any) */
	FVec2 m_moveSpeed;					/**< The speed at which to move */
	bool m_targetBody;					/**< True if the process will target a physics body */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
};
}