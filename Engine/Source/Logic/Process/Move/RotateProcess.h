#pragma once
/** \file RotateProcess.h
 * Rotate an object over time
 * This can be used in two different ways:
 * - Rotate a transform in degrees per second
 * - Apply angular velocity to a physics body
 */
// Created by Billy Graban

#include "..\IProcess.h"

#include <Utils/Typedefs.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class Box2DPhysicsComponent;
	class TransformComponent;

/** \class RotateProcess
 * @brief Rotate an object over time
 * This can be used in two different ways:
 * - Rotate a transform in degrees per second
 * - Apply angular velocity to a physics body
 */
class RotateProcess
	: public IProcess
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor 
	 * @param pOwner The actor that owns this process
	 * @param rotateSpeed The rotational speed [(in degrees per second) or (angular velocity)] of the transform
	 */
	RotateProcess(Actor* pOwner, f32 rotateSpeed = 0.f);

	/** Default Destructor */
	~RotateProcess();

	/** Initializes the process
	 * Responsible for linking to necessary components and logging failures
	 * @return True if successful
	 */
	bool Init() override;

	/** Rotate the target by the number of elapsed seconds
	 * @param deltaSeconds The amount of time (in seconds) that have elapsed since the last frame
	 */
	void Update(f32 deltaSeconds) override;

	/** Loads process data from an XML element
	 * @param pData The root element of the process data
	 * @return True if successful
	 */
	bool LoadXML(tinyxml2::XMLElement* pData) override;

	/** Gets the name of the process (for logging purposes)
	 * @return "RotateProcess"
	 */
	static const char* GetName() { return "RotateProcess"; }

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	f32 m_rotateSpeed;							/**< The speed to rotate the target */
	bool m_targetBody;							/**< True if rotating a physics body */
	Box2DPhysicsComponent* m_pBodyComponent;	/**< The physics body to rotate (if any) */
	TransformComponent* m_pTransform;			/**< The transform to rotate (if any) */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	void SetRotateSpeed(f32 speed) { m_rotateSpeed = speed; }	/**< Set the rotate speed */

};
}