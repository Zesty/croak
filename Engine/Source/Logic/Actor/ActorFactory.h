#pragma once
/** \file ActorFactory.h
 * Loads actors, components, and processes from XML files and creates actors
 */
// Created by Billy Graban
#include <functional>
#include <memory>

#include <Logic/Components/IComponent.h>
#include <Logic/Process/IProcess.h>
#include <Logic/Actor/Actor.h>
#include <Utils/Squirrel3.h>

struct lua_State;

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class IResource;

/** \class ActorFactory
 * @brief Loads actors, components, and processes from XML files and creates actors
 */
class ActorFactory
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //

	/** Alias for functions that create components */
	using CreateComponentFunction = std::function<std::unique_ptr<IComponent>(Actor*)>;

	/** Alias for functions that create processes*/
	using CreateProcessFunction = std::function<std::shared_ptr<IProcess>(Actor*)>;

	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	ActorFactory();

	/** Default Destructor */
	~ActorFactory();

	/** Create an actor from the raw data of a resource
	 * @param pActorResource The resource to use when building the actor
	 * @return Shared pointer to the new actor, if successful
	 */
	std::shared_ptr<Actor> CreateActor(IResource* pActorResource);

	/** Add a component creation function to the lookup table 
	 * @param id The component Id receiving from Hashing the component name
	 * @param pFunction A std::function that takes in an Actor* and returns a unique pointer to the new component
	 */
	void RegisterComponentCreator(Id id, CreateComponentFunction pFunction);

	/** Add a process creation function to the lookup table
	 * @param processName The name of the process to create
	 * @param pFunction A std::function that takes in an Actor* and returns a shared pointer to the new process
	 */
	void RegisterProcessCreator(std::string processName, CreateProcessFunction pFunction);


private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** The id that the next actor created will have.  Increments after each actor creation */
	Id m_nextActorId;

	/** Lookup table for O(1) searching of component creation functions */
	std::unordered_map<Id, CreateComponentFunction> m_componentCreatorMap;

	// OPTIMIZATION: This could be done with hashed Ids if necessary
	/** Lookup table for O(1) searching of process creation functions */
	std::unordered_map<std::string, CreateProcessFunction> m_processCreatorMap;

	/** Given new processes when they are created */
	IGameLayer* m_pGameLayer;

	// This is meant to be used by only component Init functions
	/** Random number generator specifically used for spawning actors */
	static Squirrel3 m_factoryRng;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //

	/** Reads component data from XML and creates it
	 * @param pData XML Element containing component data
	 * @param pOwner The actor that will own this component
	 * @return Unique pointer to the newly created component
	 */
	std::unique_ptr<IComponent> CreateComponent(tinyxml2::XMLElement* pData, Actor* pOwner);

	/** Reads process data from XML and creates it
	 * @param pData XML Element containing process data
	 * @param pOwner The actor that will own this process
	 * @return Unique pointer to the newly created process
	 */
	std::shared_ptr<IProcess> CreateProcess(tinyxml2::XMLElement* pData, Actor* pOwner);

	/** Helper function for parsing specific strings in XML process data
	 * - DestroyActor - Will assign the Actor::Destroy function for this callback
	 * @param name The name of the identifier in the XML Element
	 * @param pOwner The owner of the process callback
	 */
	std::function<void()> GetCommonCallback(std::string name, Actor* pOwner);

public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	void SetGameLayer(IGameLayer* pGameLayer) { m_pGameLayer = pGameLayer; }		/**< Get the game layer */
	void SeedRng(u32 seed) { m_factoryRng.ResetSeed(seed); }						/**< Seed the RNG */

	Squirrel3* GetRng() { return &m_factoryRng; }									/**< Get the RNG */

	/** Used by TinyXml2Extension for process initialization */
	static float FloatRange(float min, float max);

	/** Used by TinyXml2Extension for process initialization */
	static float FloatRange(float max);
};
}

/** \namespace Lua Contains all global C functions that connect the Croak Engine to Lua */
namespace Lua
{
	/** Lua binding for ActorFactory::FloatRange(float, float)
	 * @param L The lua state
	 * @return The number of arguments pushed onto the lua stack 
	 * @see crk::ActorFactory::FloatRange()
	 */
	int ActorFactory_RandomFloatRange(lua_State* L);
}