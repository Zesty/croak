#pragma once
/** \file Actor.h
 * An actor is an object in the game world.  Contains components. 
 */
// Created by Billy Graban

#include <memory>
#include <unordered_map>
#include <string>

#include <Logic/Components/IComponent.h>
#include <Utils/Typedefs.h>
#include <Utils/TinyXml2/tinyxml2.h>

struct lua_State;

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class IGameLayer;
	class IView;

/*! \class Actor 
 * @brief An actor is an object in the game world.  Contains components. 
 */
class Actor
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	Actor(Id id);

	/** Default Destructor */
	~Actor();

	/** Initializes the actor from an XML element
	 * Element must be named \<Actor\>
	 * @param pData The XML element to parse
	 * @return True if initialized successfully
	 */
	bool Init(tinyxml2::XMLElement* pData);

	/** Executes PostInit all components that have been added to the actor
	 * @return True if all components PostInit successfully
	 */
	bool PostInit();

	/** Send event that will destroy the actor next frame */
	void Destroy();

	/** Update all components
	 * @param deltaSeconds The amount of time (in seconds) that have elapsed since the last frame
	 */
	void Update(f32 deltaSeconds);

	/** Render all components
	 * This is a strong candidate for refactoring, as only one type of component can ever be rendered
	 * @param pGraphics The graphics object to use when rendering any component attached to the actor
	 */
	void Render(IGraphics* pGraphics);

	/** Adds a new component to the actor
	 * pComponent The component to add (using std::move)
	 */
	void AddComponent(std::unique_ptr<IComponent> pComponent);

	/** Get access to a component
	 * @param id The id of the component (hashed name)
	 * @return Base component pointer, if found
	 */
	IComponent* GetComponent(Id id);

	/** Templatized version that uses dynamic_cast */
	template <class Type>
	Type* GetComponent();

	Id GetId() { return m_id; }		/**< Returns the id of the actor (unique) */

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** Unique id of the actor */
	Id m_id;

	/** Lookup table of components by component Id */
	std::unordered_map<Id, std::unique_ptr<IComponent>> m_pComponents;

	/** Name of the actor (used for logging) */
	std::string m_name;

	/** The view that owns the actor (if any) */
	IView* m_pOwningView;

	/** True if the actor should be updated/rendered */
	bool m_isActive;

	/** Special tag associated with this actor
	 * FUTURE: Actors should have a vector of tags
	 */
	std::string m_tag;
	
	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	const std::string& GetName() const { return m_name; }			/**< Get the name of the actor */
	const std::string& GetTag() const { return m_tag; }				/**< Get the tag of the actor */
	IView* GetOwningView() { return m_pOwningView; }				/**< Get the owning view */

	void SetOwningView(IView* pView) { m_pOwningView = pView; }		/**< Set the owning view */
	void SetActive(bool isActive) { m_isActive = isActive; }		/**< Set if the actor is active */
};

template <class Type>
Type* Actor::GetComponent()
{
	Type* pResult = dynamic_cast<Type*>(GetComponent(IComponent::HashName(Type::GetName())));
	return pResult;
}
}

/** \namespace Lua Contains all global C functions that connect the Croak Engine to Lua */
namespace Lua
{
	/** Lua binding for Actor::GetComponent(Id)
	 * @param L The lua state
	 * @return The number of arguments pushed onto the lua stack 
	 * @see crk::Actor::GetComponent()
	 */
	int Actor_GetComponent(lua_State* L);

	/** Lua binding for Actor::GetName()
	 * @param L The lua state
	 * @return The number of arguments pushed onto the lua stack 
	 * @see crk::Actor::GetName()
	 */
	int Actor_GetName(lua_State* L);
}