#include "ActorFactory.h"

#include <Lua/lua.hpp>

#include <Utils/Log.h>
#include <Logic/IGameLayer.h>
#include <Logic/Resources/IResource.h>

using crk::ActorFactory;

crk::Squirrel3 ActorFactory::m_factoryRng;

ActorFactory::ActorFactory()
	: m_nextActorId(0)
	, m_pGameLayer(nullptr)
{
	
}

ActorFactory::~ActorFactory()
{
	
}

std::shared_ptr<crk::Actor> crk::ActorFactory::CreateActor(IResource* pActorResource)
{
	using namespace tinyxml2;

	std::shared_ptr<Actor> pActor(new Actor(m_nextActorId++));

	// Allow developers to create a simple raw actor
	if (pActorResource == nullptr)
	{
		return pActor;
	}

	// Init game-specific things here!
	XMLDocument doc;
	XMLError error = doc.Parse(reinterpret_cast<const char*>(pActorResource->GetData().data()), pActorResource->GetData().size());

	if (error != XML_SUCCESS)
	{
		LOG(Error, "Failed to load actor: %s -- %s", pActorResource->GetName().c_str(), doc.ErrorStr());
		return nullptr;
	}

	// Loaded the file!  Let's grab the node and pass it to an actor
	XMLElement* pRoot = doc.RootElement();

	if (!pActor->Init(pRoot))
	{
		LOG(Error, "Failed to initialize Actor from file: %s", pActorResource->GetName().c_str());
		return nullptr;
	}

	// Components
	XMLElement* pComponents = pRoot->FirstChildElement("Components");
	if (pComponents)
	{
		// Iterating through the children of the node and add a component for each element
		for (XMLElement* pElement = pComponents->FirstChildElement(); pElement != nullptr;
			pElement = pElement->NextSiblingElement())
		{
			pActor->AddComponent(CreateComponent(pElement, pActor.get()));
		}
	}
	else
	{
		LOG(Warning, "Actor from file %s has no components", pActorResource->GetName().c_str());
	}

	

	// Processes
	XMLElement* pProcesses = pRoot->FirstChildElement("Processes");
	if (pProcesses)
	{
		for (XMLElement* pElement = pProcesses->FirstChildElement(); pElement != nullptr;
			pElement = pElement->NextSiblingElement())
		{
			std::shared_ptr<IProcess> pProcess = CreateProcess(pElement, pActor.get());
			if (pProcess)
			{
				m_pGameLayer->AddProcess(pProcess);
			}
			else
			{
				LOG(Error, "Failed to create process: %s", pElement->Name());
				return nullptr;
			}
		}
	}

	// Post-Init components before reading and creating processes
	if (!pActor->PostInit())
	{
		LOG(Error, "Actor from file %s failed to PostInit properly", pActorResource->GetName().c_str());
		return nullptr;
	}

	return pActor;
}

void crk::ActorFactory::RegisterComponentCreator(Id id, CreateComponentFunction pFunction)
{
	m_componentCreatorMap[id] = pFunction;
}

void crk::ActorFactory::RegisterProcessCreator(std::string processName, CreateProcessFunction pFunction)
{
	m_processCreatorMap[processName] = pFunction;
}

std::unique_ptr<crk::IComponent> crk::ActorFactory::CreateComponent(tinyxml2::XMLElement* pData, Actor* pOwner)
{
	const char* name = pData->Name();
	Id componentId = IComponent::HashName(name);

	// Check if the Id is in the map
	auto createItr = m_componentCreatorMap.find(componentId);
	if (createItr == m_componentCreatorMap.end())
	{
		LOG(Error, "There is no component creation function associated with %s", name);
		return nullptr;
	}

	std::unique_ptr<IComponent> pComponent = createItr->second(pOwner);

	if (pComponent == nullptr || !pComponent->Init(pData))
	{
		LOG(Error, "Failed to initialize component: %s", name);
		return nullptr;
	}

	return pComponent;
}

std::shared_ptr<crk::IProcess> crk::ActorFactory::CreateProcess(tinyxml2::XMLElement* pData, Actor* pOwner)
{
	using namespace tinyxml2;

	const char* name = pData->Name();
	auto createItr = m_processCreatorMap.find(name);

	if (createItr == m_processCreatorMap.end())
	{
		LOG(Error, "There is no process creation function associated with %s", name);
		return nullptr;
	}

	std::shared_ptr<IProcess> pProcess = createItr->second(pOwner);

	if (!pProcess || pProcess->LoadXML(pData) == false || pProcess->Init() == false)
	{
		LOG(Error, "Failed to create process: %s", name);
		return nullptr;
	}

	// Check for child processes
	XMLElement* pChild = pData->FirstChildElement("Child");
	if (pChild)
	{
		// This will recurse until no child proc is found
		std::shared_ptr<IProcess> pChildProcess = CreateProcess(pChild->FirstChildElement(), pOwner);
		if (pChildProcess)
		{
			pProcess->AttachChild(pChildProcess);
		}
	}

	// Check for common callbacks
	XMLElement* pOnAction = pData->FirstChildElement("OnAbort");
	if (pOnAction)
	{
		pProcess->SetOnAbort(GetCommonCallback(pOnAction->GetText(), pOwner));
	}

	pOnAction = pData->FirstChildElement("OnSucceed");
	if (pOnAction)
	{
		pProcess->SetOnSucceed(GetCommonCallback(pOnAction->GetText(), pOwner));
	}

	pOnAction = pData->FirstChildElement("OnFail");
	if (pOnAction)
	{
		pProcess->SetOnFail(GetCommonCallback(pOnAction->GetText(), pOwner));
	}


	return pProcess;
}

std::function<void()> crk::ActorFactory::GetCommonCallback(std::string name, Actor* pOwner)
{
	if (name == "DestroyActor")
	{
		return [pOwner]() { pOwner->Destroy(); };
	}

	LOG(Error, "Unknown CommonCallback identifier parsed when creating a Process");
	return std::function<void()>();
}

float crk::ActorFactory::FloatRange(float min, float max)
{
	return m_factoryRng.RollRandomFloatInRange(min, max);
}

float crk::ActorFactory::FloatRange(float max)
{
	return m_factoryRng.RollRandomFloatInRange(0.f, max);
}

int Lua::ActorFactory_RandomFloatRange(lua_State* L)
{
	f32 min = static_cast<f32>(lua_tonumber(L, -2));
	f32 max = static_cast<f32>(lua_tonumber(L, -1));

	lua_pop(L, 2);

	f32 result = crk::ActorFactory::FloatRange(min, max);
	lua_pushnumber(L, result);

	return 1;
}
