#include "Actor.h"

#include <Utils/Log.h>

// REMOVE ME: Once Actor is decoupled from IGameLayer
#include <Logic/IGameLayer.h>

#include <Logic/Event/EventDispatcher.h>
#include <Logic/Event/Events/DestroyActorEvent.h>

// TODO: Find a better place for our Glue functions
#include <Logic/Components/TransformComponent.h>

using crk::Actor;

crk::Actor::Actor(Id id)
	: m_id(id)
	, m_pOwningView(nullptr)
	, m_isActive(true)
{
}

Actor::~Actor()
{
	m_pComponents.clear();
}

bool crk::Actor::Init(tinyxml2::XMLElement* pData)
{
	std::string elementName(pData->Name());
	if (elementName != "Actor")
	{
		LOG(Error, "Actor Xml root node must be named Actor.  Found: %s", elementName.c_str());
		return false;
	}

	const char* sName = pData->Attribute("name");
	if (sName == nullptr)
	{
		m_name = "Actor_" + std::to_string(m_id);
	}
	else
	{
		m_name = sName;
	}

	const char* sTag = pData->Attribute("tag");
	if (sTag)
	{
		m_tag = sTag;
	}

	return true;
}

bool crk::Actor::PostInit()
{
	for (auto& pComponent : m_pComponents)
	{
		if (!pComponent.second->PostInit())
		{
			// The PostInit function is responsible for displaying relevant errors
			return false;
		}
	}

	return true;
}

void crk::Actor::Destroy()
{
	EventDispatcher::Get()->TriggerEvent(std::make_unique<DestroyActorEvent>(m_id));
}

void crk::Actor::Update(f32 deltaSeconds)
{
	if (!m_isActive)
		return;

	for (auto& pComponent : m_pComponents)
	{
		pComponent.second->Update(deltaSeconds);
	}
}

void crk::Actor::Render(IGraphics* pGraphics)
{
	if (!m_isActive)
		return;

	for (auto& pComponent : m_pComponents)
	{
		pComponent.second->Render(pGraphics);
	}
}

void crk::Actor::AddComponent(std::unique_ptr<IComponent> pComponent)
{
	if (!pComponent)
	{
		LOG(Error, "Cannot add a null component to actor (ID: %d)", m_id);
		return;
	}

	m_pComponents[pComponent->GetId()] = std::move(pComponent);
}

crk::IComponent* crk::Actor::GetComponent(Id id)
{
	auto itr = m_pComponents.find(id);
	if (itr == m_pComponents.end())
	{
		//LOG(Warning, "%s doesn't have that component", m_name.c_str());
		return nullptr;
	}

	return itr->second.get();
}

int Lua::Actor_GetComponent(lua_State* L)
{
	// Two arguments:
	//	- Actor*
	//	- Component Name
	crk::Actor* pActor = static_cast<crk::Actor*>(lua_touserdata(L, -2));
	const char* name = lua_tostring(L, -1);

	// Clean up the stack as well
	lua_pop(L, 2);

	if (pActor)
	{
		crk::IComponent* pResult = pActor->GetComponent(crk::IComponent::HashName(name));
		lua_pushlightuserdata(L, pResult);
	}
	else
	{
		lua_pushnil(L);
	}

	return 1;
}

int Lua::Actor_GetName(lua_State* L)
{
	crk::Actor* pActor = static_cast<crk::Actor*>(lua_touserdata(L, -1)); 

	lua_pop(L, 1);

	lua_pushstring(L, pActor->GetName().c_str());

	return 1;
}
