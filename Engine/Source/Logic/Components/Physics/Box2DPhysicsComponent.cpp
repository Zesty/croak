#include "Box2DPhysicsComponent.h"

#include <Utils/Log.h>
#include <Logic/Physics/Box2DPhysics.h>
#include <Logic/Actor/Actor.h>
#include <Logic/Components/TransformComponent.h>

// New render hotness
#include <Logic/Components/Render/RenderComponent.h>
#include <Logic/Rendering/IRenderable.h>

// HACK: Getting kPixelsPerMeter
#include <Application/Graphics/IGraphics.h>

using crk::Box2DPhysicsComponent;

Box2DPhysicsComponent::Box2DPhysicsComponent(crk::Actor* pOwner, crk::IPhysicsSimulation* pPhysics)
	: IPhysicsComponent(pOwner, GetName())
{
	m_pPhysics = static_cast<Box2DPhysics*>(pPhysics);
	m_pWorld = m_pPhysics->GetWorld();
}

Box2DPhysicsComponent::~Box2DPhysicsComponent()
{
	if (m_pBody)
	{
		if (m_pFixture)
		{
			m_pBody->DestroyFixture(m_pFixture);
		}

		m_pWorld->DestroyBody(m_pBody);
	}
}

bool crk::Box2DPhysicsComponent::Init(tinyxml2::XMLElement* pData)
{
	if (!m_pWorld)
	{
		LOG(Error, "Cannot add Box2DPhysicsComponent to a null b2World");
		return false;
	}

	using namespace tinyxml2;

	// Read if body is a sensor (This feels like a HACK, as nullptr is being implicity cast to false)
	bool isSensor = pData->Attribute("isSensor");

	// Allow to rotate
	m_bodyDef.fixedRotation = pData->BoolAttribute("canRotate");
	
	// Read body type
	const char* sType = pData->Attribute("type");

	if (sType == nullptr)
	{
		LOG(Warning, "No body type found in PhysicsComponent.  Defaulting to static");
		sType = "static";
	}

	std::string type = sType;
	if (type == "static")
		m_bodyType = b2BodyType::b2_staticBody;
	else if (type == "dynamic")
		m_bodyType = b2BodyType::b2_dynamicBody;
	else if (type == "kinematic")
		m_bodyType = b2BodyType::b2_kinematicBody;
	else
	{
		LOG(Error, "Unknown body type for PhysicsComponent: %s", type.c_str());
		return false;
	}

	m_bodyDef.linearDamping = pData->FloatAttribute("linearDamping");

	// Read category
	const char* sCategory = pData->Attribute("category");
	if (sCategory != nullptr)
	{
		u16 categoryFlag = m_pPhysics->GetCategoryFlag(sCategory);
		m_fixtureDef.filter.categoryBits = categoryFlag;
	}

	// Read mask
	const char* sMask = pData->Attribute("mask");
	if (sMask != nullptr)
	{
		// Parse masks
		std::vector<std::string> maskedCategories;
		std::string masks = sMask;

		size_t pipeIndex;
		while (true)
		{
			pipeIndex = masks.find('|');

			if (pipeIndex == -1)
			{
				maskedCategories.push_back(masks);
				break;
			}
			std::string mask = masks.substr(0, pipeIndex);
			maskedCategories.push_back(mask);
			masks = masks.substr(pipeIndex + 1);
		}

		// Create mask
		u16 bitMask = 0;
		for (auto& mask : maskedCategories)
		{
			bitMask |= m_pPhysics->GetCategoryFlag(mask);
		}

		m_fixtureDef.filter.maskBits = bitMask;
	}

	// Set the body def
	m_bodyDef.type = m_bodyType;
	m_fixtureDef.isSensor = isSensor;
	m_bodyDef.gravityScale = 0.0f;
	
	// Parsing box collider
	XMLElement* pBox = pData->FirstChildElement("Box");
	if (pBox)
	{
		f32 width = pBox->FloatAttribute("width");
		f32 height = pBox->FloatAttribute("height");

		m_polyShape.SetAsBox(width / 2.0f, height / 2.0f);
		m_fixtureDef.shape = &m_polyShape;
		m_isCircle = false;

		return true;
	}

	// Parsing circle collider
	XMLElement* pCircle = pData->FirstChildElement("Circle");
	if (pCircle)
	{
		m_circleShape.m_radius = pCircle->FloatAttribute("radius");
		m_fixtureDef.shape = &m_circleShape;
		m_isCircle = true;

		return true;
	}

	// Parsing polygon collider
	XMLElement* pPolygon = pData->FirstChildElement("Polygon");
	if (pPolygon)
	{
		std::string pointData = pPolygon->Attribute("points");
		m_isCircle = false;

		std::vector<b2Vec2> points;
		while (true)
		{
			b2Vec2 pt;
			int readCount = sscanf_s(pointData.c_str(), "%f,%f", &pt.x, &pt.y);
			size_t spaceIndex = pointData.find(' ');
			if (readCount != 2)
				break;

			pointData = pointData.substr(static_cast<size_t>(spaceIndex) + 1);
			LOG(Info, "Found point point in polygon: %f, %f", pt.x, pt.y);

			LOG(Todo, "Add Xml attribute for whether the polygon points are in meters or pixels");
			pt.x /= kPixelsPerMeter;
			pt.y /= kPixelsPerMeter;

			points.push_back(pt);

			if (spaceIndex == -1)
				break;
		}

		m_polyShape.Set(points.data(), static_cast<int32>(points.size()));

		m_fixtureDef.shape = &m_polyShape;
		return true;
	}

	return false;
}

void crk::Box2DPhysicsComponent::Update(f32 deltaSeconds)
{
	// Only update dynamic bodies
	if (m_bodyType != b2BodyType::b2_dynamicBody)
		return;

	m_pTransform->SetPosition(Box2DPhysics::b2Vec2ToFVec(m_pBody->GetPosition()));
	m_pTransform->SetAngle(m_pBody->GetAngle());
}

bool crk::Box2DPhysicsComponent::PostInit()
{
	// Get the transform of the actor
	m_pTransform = m_pOwner->GetComponent<TransformComponent>();
	if (!m_pTransform)
	{
		LOG(Error, "Actor with BodyComponent must also have a Transform");
		return false;
	}

	// Edge case for polygons
	if (!m_isCircle && m_polyShape.m_count != 4)
	{
		// Currently this PostInit is not guaranteed to execute before the Sprite PostInit
		// This is a little extra work, but it allows the developer to organize the components in
		//	any order they wish in the Xml file
		auto pTexture = m_pOwner->GetComponent<RenderComponent>()->GetRenderable()->GetTexture();
		FVec2 scale = m_pTransform->GetScale();

		// When removing this, also remove the include above
		LOG(Todo, "Move kPixelsPerMeter out of IGraphics and into a better place");

		// Offset all verts by half-dimensions (scaled)
		// Xml polygons are currently stored in pixel values
		for (int i = 0; i < m_polyShape.m_count; ++i)
		{
			m_polyShape.m_vertices[i].x -= ((pTexture->GetWidth() * scale.x) / 2.0f) / kPixelsPerMeter;
			m_polyShape.m_vertices[i].y -= ((pTexture->GetHeight() * scale.y) / 2.0f) / kPixelsPerMeter;
		}
	}

	FVec2 pos = m_pTransform->GetPosition();
	m_bodyDef.position = b2Vec2(pos.x, pos.y);

	// Create Body/Fixture
	m_pBody = m_pWorld->CreateBody(&m_bodyDef);
	m_pFixture = m_pBody->CreateFixture(&m_fixtureDef);

	m_pBody->SetUserData(m_pOwner);

	return true;
}

void crk::Box2DPhysicsComponent::ApplyLinearImpulseToCenter(FVec2 impulse)
{
	m_pBody->ApplyLinearImpulseToCenter(Box2DPhysics::ConvertFVec(impulse), true);
}

void crk::Box2DPhysicsComponent::SetLinearVelocity(FVec2 velocity)
{
	m_pBody->SetLinearVelocity(Box2DPhysics::ConvertFVec(velocity));
}

void crk::Box2DPhysicsComponent::SetAngularVelocity(f32 angularVelocity)
{
	m_pBody->SetAngularVelocity(angularVelocity);
}

void crk::Box2DPhysicsComponent::SetPosition(FVec2 position)
{
	m_pBody->SetTransform(Box2DPhysics::ConvertFVec(position), m_pBody->GetAngle());
}
