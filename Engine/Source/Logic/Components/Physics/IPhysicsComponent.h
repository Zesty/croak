#pragma once
/** \file IPhysicsComponent.h
 * Base Component for a physics collision 
 */
// Created by Billy Graban

#include <functional>

#include "..\IComponent.h"

#include <Utils/CommonMath.h>

struct lua_State;

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class Actor;

/** \class IPhysicsComponent
 * @brief Base Component for a physics collision 
 */
class IPhysicsComponent
	: public IComponent
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor
	 * @param pOwner The actor that owns this component
	 * @param name The name of this component
	 */
	IPhysicsComponent(Actor* pOwner, const char* name);

	/** Default Destructor */
	~IPhysicsComponent();

	/** Applies linear impulse to the center of the body
	 * @param impulse The impulse (in MKS)
	 */
	virtual void ApplyLinearImpulseToCenter(FVec2 impulse) = 0;

	/** Sets the linear velocity of the body directly
	 * @param velocity New velocity of the body
	 */
	virtual void SetLinearVelocity(FVec2 velocity) = 0;

	/** Sets the angular velocity of the body directly
	 * @param angularVelocity New angular velocity of the body
	 */
	virtual void SetAngularVelocity(f32 angularVelocity) = 0;

	/** Sets the position of the body
	 * Warning!  This teleports the objects, sidestepping any physical contacts between point A and B.
	 * Consider doing the math and setting the velocity
	 * @param position The new position (in world units)
	 */
	virtual void SetPosition(FVec2 position) = 0;

	/** Executes begin overlap callback (if one has been set) */
	void HandleBeginOverlap(Actor* pActor);

	/** Executes end overlap callback (if one has been set) */
	void HandleEndOverlap(Actor* pActor);

	/** Executes begin collision callback (if one has been set) */
	void HandleBeginCollision(Actor* pActor);

	/** Executes end collision callback (if one has been set) */
	void HandleEndCollision(Actor* pActor);

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** Alias for callbacks */
	using CollisionHandler = std::function<void(Actor * pOther)>;

	CollisionHandler m_beginCollision;		/**< Callback for begin collision */
	CollisionHandler m_endCollision;		/**< Callback for end collision */
	CollisionHandler m_beginOverlap;		/**< Callback for begin overlap */
	CollisionHandler m_endOverlap;			/**< Callback for end overlap */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	void SetBeginCollision(CollisionHandler handler)	{ m_beginCollision = handler; }		/**< Set begin collision callback */
	void SetEndCollision(CollisionHandler handler)		{ m_endCollision = handler; }		/**< Set end collision callback */
	void SetBeginOverlap(CollisionHandler handler)		{ m_beginOverlap = handler; }		/**< Set begin overlap callback */
	void SetEndOverlap(CollisionHandler handler)		{ m_endOverlap = handler; }			/**< Set end overlap callback */

};
}

/** \namespace Lua Contains all global C functions that connect the Croak Engine to Lua */
namespace Lua
{
	/** Lua binding for IPhysicsComponent::ApplyLinearImpulseToCenter(FVec2)
	 * @param L The lua state
	 * @return The number of arguments pushed onto the lua stack 
	 * @see crk::IPhysicsComponent::ApplyLinearImpulseToCenter()
	 */
	int PhysicsComponent_ApplyLinearImpulseToCenter(lua_State* L);

	/** Lua binding for IPhysicsComponent::SetLinearVelocity(FVec2)
	 * @param L The lua state
	 * @return The number of arguments pushed onto the lua stack 
	 * @see crk::IPhysicsComponent::SetLinearVelocity()
	 */
	int PhysicsComponent_SetLinearVelocity(lua_State* L);

	/** Lua binding for IPhysicsComponent::SetPosition(FVec2)
	 * @param L The lua state
	 * @return The number of arguments pushed onto the lua stack 
	 * @see crk::IPhysicsComponent::SetPosition()
	 */
	int PhysicsComponent_SetPosition(lua_State* L);
}