#pragma once
/** \file Box2DPhysicsComponent.h
 * Physics component implemented with Box2D 
 */
// Created by Billy Graban

#include ".\IPhysicsComponent.h"

#include <Box2D/Box2D.h>
#include <Logic/Components/TransformComponent.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class Actor;
	class IPhysicsSimulation;
	class Box2DPhysics;

/** \class Box2DPhysicsComponent
 * @brief Physics component implemented with Box2D 
 */
class Box2DPhysicsComponent
	: public IPhysicsComponent
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor
	 * @param pOwner The actor that owns this component
	 * @param pPhysics Physics simulation object used by this component (assumed to be Box2D)
	 */
	Box2DPhysicsComponent(Actor* pOwner, IPhysicsSimulation* pPhysics);

	/** Default destructor */
	~Box2DPhysicsComponent();

	/** The name of this component (used for hashing)
	 * @return "PhysicsComponent"
	 */
	static const char* GetName() { return "PhysicsComponent"; }

	/** Initialize component using an XML Element
	 * @param pData The data to use when initializing
	 * @return True if initialized successfully
	 */
	bool Init(tinyxml2::XMLElement* pData) override;

	/** Update component data by delta time
	 * @param deltaSeconds The number of seconds that have elapsed since the last frame
	 */
	void Update(f32 deltaSeconds) override;

	/** Get references to other components relevant to this one
	 * Logs error on failure
	 * @return True if successful
	 */
	bool PostInit() override;

	/** Applies linear impulse to the center of the body
	 * @param impulse The impulse (in MKS)
	 */
	void ApplyLinearImpulseToCenter(FVec2 impulse) override;

	/** Sets the linear velocity of the body directly
	 * @param velocity New velocity of the body
	 */
	void SetLinearVelocity(FVec2 velocity) override;

	/** Sets the angular velocity of the body directly
	 * @param angularVelocity New angular velocity of the body
	 */
	void SetAngularVelocity(f32 angularVelocity) override;

	/** Sets the position of the body
	 * Warning!  This teleports the objects, sidestepping any physical contacts between point A and B.
	 * Consider doing the math and setting the velocity
	 * @param position The new position (in world units)
	 */
	virtual void SetPosition(FVec2 position) override;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	b2World* m_pWorld;					/**< The Box2D world */
	b2Body* m_pBody;					/**< The body of object */
	b2Fixture* m_pFixture;				/**< The fixture attached to the body (Future: use a vector for more complex bodies) */

	b2PolygonShape m_polyShape;			/**< Polygon shape for collisions */
	b2CircleShape m_circleShape;		/**< Circle shape for collisions */
	b2BodyDef m_bodyDef;				/**< Body definition for body creation */
	b2FixtureDef m_fixtureDef;			/**< Fixture definition for fixture creation */
	b2BodyType m_bodyType;				/**< Type of body (kinematic, dynamic, static) */

	TransformComponent* m_pTransform;	/**< Transform associated with owning actor */
	Box2DPhysics* m_pPhysics;			/**< Physics simulation this object lives in */
	bool m_isCircle;					/**< Used during parsing for edge case of circle collider */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}

