#include "IPhysicsComponent.h"

#include <Lua/lua.hpp>

#include <Utils/Log.h>

using crk::IPhysicsComponent;

IPhysicsComponent::IPhysicsComponent(crk::Actor* pOwner, const char* name)
	: IComponent(pOwner, name)
{
	
}

IPhysicsComponent::~IPhysicsComponent()
{
	
}

void crk::IPhysicsComponent::HandleBeginOverlap(Actor* pActor)
{
	if (m_beginOverlap)
	{
		m_beginOverlap(pActor);
	}
}

void crk::IPhysicsComponent::HandleEndOverlap(Actor* pActor)
{
	if (m_endOverlap)
	{
		m_endOverlap(pActor);
	}
}

void crk::IPhysicsComponent::HandleBeginCollision(Actor* pActor)
{
	if (m_beginCollision)
	{
		m_beginCollision(pActor);
	}
}

void crk::IPhysicsComponent::HandleEndCollision(Actor* pActor)
{
	if (m_endCollision)
	{
		m_endCollision(pActor);
	}
}

int Lua::PhysicsComponent_ApplyLinearImpulseToCenter(lua_State* L)
{
	// Testing broken pointers lua-side when component is found on init
	void* pRawPointer = lua_touserdata(L, -3);
	auto pComponent = static_cast<IPhysicsComponent*>(pRawPointer);
	
	f32 xForce = static_cast<f32>(lua_tonumber(L, -2));
	f32 yForce = static_cast<f32>(lua_tonumber(L, -1));

	lua_pop(L, 3);

	pComponent->ApplyLinearImpulseToCenter(crk::FVec2(xForce, yForce));

	return 0;
}

int Lua::PhysicsComponent_SetLinearVelocity(lua_State* L)
{
	auto pComponent = static_cast<IPhysicsComponent*>(lua_touserdata(L, -3));

	f32 xForce = static_cast<f32>(lua_tonumber(L, -2));
	f32 yForce = static_cast<f32>(lua_tonumber(L, -1));

	lua_pop(L, 3);

	if (pComponent)
		pComponent->SetLinearVelocity(crk::FVec2(xForce, yForce));
	else
		LOG(Error, "Lua Function: setLinearVelocity failed to cast PhysicsComponent");

	return 0;
}

int Lua::PhysicsComponent_SetPosition(lua_State* L)
{
	auto pComponent = static_cast<IPhysicsComponent*>(lua_touserdata(L, -3));

	f32 xForce = static_cast<f32>(lua_tonumber(L, -2));
	f32 yForce = static_cast<f32>(lua_tonumber(L, -1));

	lua_pop(L, 3);

	if (pComponent)
		pComponent->SetPosition(crk::FVec2(xForce, yForce));
	else
		LOG(Error, "Lua Function: setPosition failed to cast PhysicsComponent");

	return 0;
}
