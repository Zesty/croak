#include "TextComponent.h"

#include <Utils/Log.h>
#include <Logic/Actor/Actor.h>
#include <Logic/Resources/ResourceCache.h>

#include <Logic/Components/Render/RenderComponent.h>
#include <Logic/Rendering/Sprite.h>

using crk::TextComponent;

TextComponent::TextComponent(crk::Actor* pOwner)
	: IComponent(pOwner, GetName())
{
	
}

TextComponent::~TextComponent()
{
	
}

bool crk::TextComponent::Init(tinyxml2::XMLElement* pData)
{
	const char* sFontPath = pData->Attribute("font");
	if (!sFontPath)
	{
		LOG(Error, "TextComponent must have a font");
		return false;
	}

	int fontSize = pData->IntAttribute("size");
	if (fontSize == 0)
	{
		LOG(Error, "TextComponent must have a size (font size) attribute");
		return false;
	}

	m_pFont = ResourceCache::Get()->AcquireFont(sFontPath, fontSize);
	if (!m_pFont)
	{
		LOG(Error, "TextComponent failed to acquire font");
		return false;
	}

	return true;
}

bool crk::TextComponent::PostInit()
{
	m_pSprite = m_pOwner->GetComponent<RenderComponent>()->GetRenderable<Sprite>();
	if (!m_pSprite)
	{
		LOG(Error, "TextComponent must be attached to an actor with a Sprite Renderable");
		return false;
	}

	return true;
}

void crk::TextComponent::UpdateText(const std::string& text)
{
	m_pSprite->SetTexture(m_pFont->BuildTextureFromString(text));
}
