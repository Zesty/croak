#pragma once
/** \file RenderComponent.h
 * Base component for all renderables 
 */
// Created by Billy Graban

#include "..\IComponent.h"

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class IRenderable;
	class TransformComponent;

/** \class RenderComponent
 * @brief Base component for all renderables 
 */
class RenderComponent
	: public IComponent
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor
	 * @param pOwner The actor that owns this component
	 */
	RenderComponent(Actor* pOwner);

	/** Default Destructor */
	~RenderComponent();

	/** The name of this component (used for hashing)
	 * @return "RenderComponent"
	 */
	static const char* GetName() { return "RenderComponent"; }

	// Inherited via IComponent

	/** Initialize component using an XML Element
	 * @param pData The data to use when initializing
	 * @return True if initialized successfully
	 */
	virtual bool Init(tinyxml2::XMLElement* pData) override;

	/** Get references to other components relevant to this one
	 * Logs error on failure
	 * @return True if successful
	 */
	virtual bool PostInit() override;

	/** Draw the component to the screen
	 * @param pGraphics The graphics object used for rendering
	 * @return True if rendering successful
	 */
	virtual bool Render(IGraphics* pGraphics) override;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	IRenderable* m_pRenderable;			/**< Base renderable to draw */
	TransformComponent* m_pTransform;	/**< The transform of the owning actor */
	i32 m_layer;						/**< Determines the draw order (lower values are drawn first) */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	i32 GetLayer() { return m_layer; }									/**< Get the render layer */
	IRenderable* GetRenderable() const { return m_pRenderable; }		/**< Get the base renderable */

	/** Cast the base renderable to a specific concrete render type
	 * @return The dynamic_casted pointer, or nullptr on failure
	 */
	template <typename Type>
	Type* GetRenderable();

};

template<typename Type>
inline Type* RenderComponent::GetRenderable()
{
	return dynamic_cast<Type*>(m_pRenderable);
}

}