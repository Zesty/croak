#include "RenderComponent.h"

#include <Logic/Rendering/IRenderable.h>
#include <Logic/Components/TransformComponent.h>
#include <Utils/Log.h>

// Renderables
#include <Logic/Rendering/Sprite.h>
#include <Logic/Rendering/TiledMap.h>

using crk::RenderComponent;

RenderComponent::RenderComponent(crk::Actor* pOwner)
	: IComponent(pOwner, GetName())
	, m_pRenderable(nullptr)
{
	
}

RenderComponent::~RenderComponent()
{
	if (m_pRenderable)
	{
		delete m_pRenderable;
		m_pRenderable = nullptr;
	}
}

bool crk::RenderComponent::Init(tinyxml2::XMLElement* pData)
{
	const char* sType = pData->Attribute("type");
	if (!sType)
	{
		LOG(Error, "RenderComponent must have a type attribute");
		return false;
	}

	m_layer = pData->IntAttribute("layer");

	std::string type = sType;
	if (type == "Sprite")
	{
		m_pRenderable = new Sprite(GetOwner());
	}
	else if (type == "TiledMap")
	{
		m_pRenderable = new TiledMap(GetOwner());
	}
	else
	{
		LOG(Error, "RenderComponent failed to load unknown type: %s", sType);
		return false;
	}

	if (!m_pRenderable->Init(pData))
	{
		LOG(Error, "Failed to initialize renderable");
		return false;
	}

	return true;
}

bool crk::RenderComponent::PostInit()
{
	if (!m_pRenderable->PostInit())
	{
		LOG(Error, "Renderable failed to PostInit");
		return false;
	}
	return true;
}

bool crk::RenderComponent::Render(IGraphics* pGraphics)
{
	return m_pRenderable->Render(pGraphics);
}
