#pragma once
/** \file TextComponent.h
 * Draws text to sprites
 */
// Created by Billy Graban

#include "..\IComponent.h"

#include <memory>

#include <Application/Graphics/Fonts/IFont.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class Sprite;

/** \class TextComponent
 * @brief Draws text to sprites 
 */
class TextComponent
	: public IComponent
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor
	 * @param pOwner The actor that owns this component
	 */
	TextComponent(Actor* pOwner);

	/** Default Destructor */
	~TextComponent();

	/** The name of this component (used for hashing)
	 * @return "TextComponent"
	 */
	static const char* GetName() { return "TextComponent"; }

	/** Initialize component using an XML Element
	 * @param pData The data to use when initializing
	 * @return True if initialized successfully
	 */
	bool Init(tinyxml2::XMLElement* pData) override;

	/** Get references to other components relevant to this one
	 * Logs error on failure
	 * @return True if successful
	 */
	bool PostInit() override;

	/** Redraws the text into a new texture stored in the Sprite
	 * Note: This can be expensive, so use it only when necessary
	 * @param text New text to draw to the Sprite
	 */
	void UpdateText(const std::string& text);

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	std::shared_ptr<IFont> m_pFont;			/**< Font to use when rendering new text */
	Sprite* m_pSprite;						/**< Sprite to render the text to */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}