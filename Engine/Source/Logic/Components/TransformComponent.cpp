#include "TransformComponent.h"

#include <Utils/Log.h>

using crk::TransformComponent;

crk::TransformComponent::TransformComponent(Actor* pOwner)
	: IComponent(pOwner, GetName())
	, m_position(0.0f, 0.0f)
	, m_localOffset(0.f, 0.f)
	, m_scale(1.0f, 1.0f)
	, m_pParent(nullptr)
{
}

TransformComponent::~TransformComponent()
{
	
}

bool crk::TransformComponent::Init(tinyxml2::XMLElement* pData)
{
	using namespace tinyxml2;
	XMLElement* pPosition = pData->FirstChildElement("Position");
	if (pPosition)
	{
		m_localOffset.x = pPosition->FloatAttribute("x");
		m_localOffset.y = pPosition->FloatAttribute("y");
		m_isDirty = true;
	}

	XMLElement* pScale = pData->FirstChildElement("Scale");
	if (pScale)
	{
		m_scale.x = pScale->FloatAttribute("x");
		m_scale.y = pScale->FloatAttribute("y");
	}

	//LOG(Info, "Loaded actor with position: (%.3f, %.3f)", 
	//	m_position.x, m_position.y);
	//LOG(Info, "Loaded actor with scale: (%.2f, %.2f)",
	//	m_scale.x, m_scale.y);

	return true;
}

void crk::TransformComponent::Move(FVec2 delta)
{
	m_isDirty = true;
	m_localOffset += delta;
}
void TransformComponent::Move(f32 deltaX, f32 deltaY)
{
	m_isDirty = true;
	m_localOffset.x += deltaX;
	m_localOffset.y += deltaY;
}

void crk::TransformComponent::SetParent(TransformComponent* pParent)
{
	if (m_pParent)
	{
		m_pParent->RemoveChild(this);
	}

	m_pParent = pParent;
	m_pParent->AddChild(this);
}

void crk::TransformComponent::AddChild(TransformComponent* pChild)
{
	m_pChildren.emplace_back(pChild);
}

void crk::TransformComponent::RemoveChild(TransformComponent* pChild)
{
	for (auto it = m_pChildren.begin(); it != m_pChildren.end(); ++it)
	{
		if (*it == pChild)
		{
			m_pChildren.erase(it);
			return;
		}
	}
}

void crk::TransformComponent::RecalculatePosition()
{
	// TODO: If I implement hierarchical scaling, I might as well use a transform
	if (m_pParent)
		m_position = m_localOffset + m_pParent->m_position;
	else
		m_position = m_localOffset;

	for (auto& pChild : m_pChildren)
	{
		pChild->RecalculatePosition();
	}

	m_isDirty = false;
}

crk::FVec2 crk::TransformComponent::GetPosition()
{
	if (!m_isDirty)
		return m_position;

	// RecalculatePosition recursively walks up the hierarchy calculating new positions and cleaning the dirty flag
	RecalculatePosition();
	return m_position;
}

void crk::TransformComponent::SetPosition(f32 x, f32 y)
{
	m_isDirty = true;
	m_localOffset = FVec2(x, y);
}

void crk::TransformComponent::SetPosition(FVec2 position)
{
	m_isDirty = true;
	m_localOffset = position;
}

int Lua::TransformComponent_Move(lua_State* L)
{
	// 3 arguments:
	//	- component
	//	- x delta
	//	- y delta
	crk::TransformComponent* pTransform = static_cast<crk::TransformComponent*>(lua_touserdata(L, -3));
	f32 xDelta = static_cast<f32>(lua_tonumber(L, -2));
	f32 yDelta = static_cast<f32>(lua_tonumber(L, -1));

	// Pop args off
	lua_pop(L, 3);

	pTransform->Move(xDelta, yDelta);

	return 0;
}

int Lua::TransformComponent_GetPosition(lua_State* L)
{
	crk::TransformComponent* pTransform = static_cast<crk::TransformComponent*>(lua_touserdata(L, -1));
	lua_pop(L, 1);

	lua_newtable(L);

	crk::FVec2 position = pTransform->GetPosition();
	lua_pushnumber(L, position.x);
	lua_setfield(L, -2, "x");

	lua_pushnumber(L, position.y);
	lua_setfield(L, -2, "y");

	return 1;
}
