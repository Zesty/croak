#pragma once
/** \file IComponent.h
 * Base Interface for all components that can be attached to actors 
 */
// Created by Billy Graban

#include <Utils/Typedefs.h>
#include <Utils/TinyXml2/tinyxml2.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class Actor;
	class IGraphics;

/** \class IComponent
 * @brief Base Interface for all components that can be attached to actors 
 */
class IComponent
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //

	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor 
	 * @param pOwner The actor that owns this component
	 * @param name The name of this component
	 */
	IComponent(Actor* pOwner, const char* name);

	/** Default Destructor */
	virtual ~IComponent();

	/** Initialize component using an XML Element
	 * @param pData The data to use when initializing
	 * @return True if initialized successfully
	 */
	virtual bool Init(tinyxml2::XMLElement* pData) = 0;

	/** Get references to other components relevant to this one
	 * Logs error on failure
	 * @return True if successful
	 */
	virtual bool PostInit() { return true; }

	/** Update component data by delta time 
	 * @param deltaSeconds The number of seconds that have elapsed since the last frame
	 */
	virtual void Update(f32 deltaSeconds) { }

	/** Draw the component to the screen
	 * @param pGraphics The graphics object used for rendering
	 * @return True if rendering successful
	 */
	virtual bool Render(IGraphics* pGraphics) { return true; }

	Id GetId() { return m_id; }					/**< Get the component Id (Name hashed) */
	Actor* GetOwner() { return m_pOwner; }		/**< Get the owner of the component */

	/** Hashes the name of the component into a single 32-bit integer */
	static Id HashName(const char* name);

// --------------------------------------------------------------------- //
// Private Member Variables
// --------------------------------------------------------------------- //
protected:
	/** The owner of the component */
	Actor* m_pOwner;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	/** The id of the component (Name hashed)
	 * This doesn't need to be a member variable, feels a little wasteful in terms of copied memory
	 */
	Id m_id;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //

};
}