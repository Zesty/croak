#pragma once
/** \file TransformComponent.h
 * Stores position, rotational, and hierarchical transform data
 */
// Created by Billy Graban

#include "./IComponent.h"

#include <vector>

#include <Utils/CommonMath.h>

struct lua_State;

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class TransformComponent
 * @brief Stores position, rotational, and hierarchical transform data
 */
class TransformComponent
	: public IComponent
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor
	 * @param pOwner The actor that owns this component
	 */
	TransformComponent(Actor* pOwner);

	/** Default Destructor */
	~TransformComponent();

	/** The name of this component (used for hashing)
	 * @return "TransformComponent"
	 */
	static const char* GetName() { return "TransformComponent"; }

	/** Initialize component using an XML Element
	 * @param pData The data to use when initializing
	 * @return True if initialized successfully
	 */
	bool Init(tinyxml2::XMLElement* pData) override;

	/** Moves the position and sets the transform to dirty
	 * @param delta The x/y amount to move the transform
	 */
	void Move(FVec2 delta);

	/** Moves the position and sets the transform to dirty
	 * @param deltaX The x amount to move the transform
	 * @param deltaY The y amount to move the transform
	 */
	void Move(f32 deltaX, f32 deltaY);

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	FVec2 m_position;									/**< Position (in world space) */
	FVec2 m_localOffset;								/**< Offset from parent transform (world offset if no parent) */
	FVec2 m_scale;										/**< Scale of the transform */
	f32 m_angle;										/**< Rotational amount (in degrees) */
	bool m_isDirty;										/**< Flag for recalculating transform hierarchy */

	TransformComponent* m_pParent;						/**< Parent of the transform */
	std::vector<TransformComponent*> m_pChildren;		/**< All children of this transform */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //
	/** Recalculates position and children recursively (cleans the dirty flag) */
	void RecalculatePosition();

public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	FVec2 GetPosition();										/**< Get position (in world space) */
	FVec2 GetScale() const { return m_scale; }					/**< Get scale */
	f32 GetAngle() const { return m_angle; }					/**< Get angle (in degrees) */

	void SetPosition(f32 x, f32 y);								/**< Set position using x and y */
	void SetPosition(FVec2 position);							/**< Set position using a FVec2 */
	void SetScale(f32 x, f32 y) { m_scale = FVec2(x, y); }		/**< Set scale using x and y */
	void SetScale(FVec2 scale) { m_scale = scale; }				/**< Set scale using FVec2 */
	void SetAngle(f32 angle) { m_angle = angle; }				/**< Set angle (in degrees) */

	void Rotate(f32 delta) { m_angle += delta; }				/**< Rotate transform (by degrees) */

	void SetParent(TransformComponent* pParent);				/**< Set parent of transform */
	void AddChild(TransformComponent* pChild);					/**< Add child to children list */
	void RemoveChild(TransformComponent* pChild);				/**< Remove specific child from list */
};
}

/** \namespace Lua Contains all global C functions that connect the Croak Engine to Lua */
namespace Lua
{
	/** Lua binding for TransformComponent::Move(float, float)
	 * @param L The lua state
	 * @return The number of arguments pushed onto the lua stack
	 * @see crk::TransformComponent::Move()
	 */
	int TransformComponent_Move(lua_State* L);

	/** Lua binding for TransformComponent::GetPosition()
	 * @param L The lua state
	 * @return The number of arguments pushed onto the lua stack
	 * @see crk::TransformComponent::GetPosition()
	 */
	int TransformComponent_GetPosition(lua_State* L);
}