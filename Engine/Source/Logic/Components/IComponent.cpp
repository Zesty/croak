#include "IComponent.h"

#include <xhash>

using crk::IComponent;

crk::IComponent::IComponent(Actor* pOwner, const char* name)
	: m_pOwner(pOwner)
	, m_id(HashName(name))
{
}

IComponent::~IComponent()
{
	
}

Id crk::IComponent::HashName(const char* name)
{
	static std::hash<std::string> hasher;
	return static_cast<Id>(hasher(std::string(name)));
}
