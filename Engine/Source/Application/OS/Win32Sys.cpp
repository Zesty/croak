#include "Win32Sys.h"

using crk::Win32Sys;

#include <Application/Window/SDLWindow.h>

std::unordered_map<crk::IOpSys::TextColor, u8> Win32Sys::s_colorMap =
{
	{crk::IOpSys::TextColor::White, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY },
	{crk::IOpSys::TextColor::Grey, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED },
	{crk::IOpSys::TextColor::Green, FOREGROUND_GREEN | FOREGROUND_INTENSITY },
	{crk::IOpSys::TextColor::Cyan, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY },
	{crk::IOpSys::TextColor::Red, FOREGROUND_RED | FOREGROUND_INTENSITY },
	{crk::IOpSys::TextColor::Yellow, FOREGROUND_GREEN | FOREGROUND_RED },
	{crk::IOpSys::TextColor::Purple, FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY},
	{crk::IOpSys::TextColor::Critical, BACKGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY },
	{crk::IOpSys::TextColor::Blue, FOREGROUND_BLUE | FOREGROUND_INTENSITY },
};

Win32Sys::Win32Sys()
	: m_consoleHandle(NULL)
	, m_screenBufferInfo()
	, m_sysTime()
{
	
}

Win32Sys::~Win32Sys()
{
}

std::unique_ptr<crk::IWindow> crk::Win32Sys::CreateSystemWindow(const char* title, u32 width, u32 height)
{
	std::unique_ptr<IWindow> pWindow = std::make_unique<SDLWindow>();
	if (pWindow->Init(title, width, height) == false)
	{
		// TODO: Log Error
		return nullptr;
	}

	if (!m_consoleHandle)
	{
		m_consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
		GetConsoleScreenBufferInfo(m_consoleHandle, &m_screenBufferInfo);
	}

	return pWindow;
}

void crk::Win32Sys::SetTextColor(TextColor color)
{
	SetConsoleTextAttribute(m_consoleHandle, s_colorMap[color]);
}

void crk::Win32Sys::RestoreDefaultTextColor()
{
	SetConsoleTextAttribute(m_consoleHandle, m_screenBufferInfo.wAttributes);
}

const char* crk::Win32Sys::GetFormattedTimestamp()
{
	GetLocalTime(&m_sysTime);
	sprintf_s(m_timestampBuffer, "%02d.%02d.%02d:%03d",
		m_sysTime.wHour, m_sysTime.wMinute, m_sysTime.wSecond, m_sysTime.wMilliseconds);
	
	return m_timestampBuffer;
}

void crk::Win32Sys::EnableLogColors()
{
	m_consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(m_consoleHandle, &m_screenBufferInfo);
}
