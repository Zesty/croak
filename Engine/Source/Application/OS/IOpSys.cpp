#include "IOpSys.h"

#include "Win32Sys.h"

using crk::IOpSys;

IOpSys::IOpSys()
	: m_timestampBuffer()
{
	
}

IOpSys::~IOpSys()
{
	
}

std::unique_ptr<IOpSys> crk::IOpSys::Create()
{
#ifdef _WIN32
	return std::make_unique<Win32Sys>();
#else
	// TODO: Error
	return nullptr;
#endif
}
