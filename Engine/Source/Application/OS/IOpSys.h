#pragma once
/** \file IOpSys.h
 * Interface for the operating system that is being used
 */
// Created by Billy Graban

#include <memory>
#include <unordered_map>

#include <Utils/Typedefs.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class IWindow;
/** \class IOpSys
 * @brief Interface for the operating system that is being used 
 */
class IOpSys
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //

	/** \enum TextColor Possible colors for the console window output */
	enum class TextColor
	{
		White,		/**< The color 'White' */
		Grey,		/**< The color 'Grey' */
		Green,		/**< The color 'Green' */
		Cyan,		/**< The color 'Cyan' */
		Red,		/**< The color 'Red' */
		Blue,		/**< The color 'Blue' */
		Yellow,		/**< The color 'Yellow' */
		Purple,		/**< The color 'Purple' */
		Critical	/**< Sharp contrast color to catch the eye when something has gone horribly wrong */
	};



	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	IOpSys();

	/** Default Destructor */
	virtual ~IOpSys();

	/** Returns the name of the operating system (for logging purposes)
	 * @return The name of the operating system
	 */
	virtual const char* GetSystemName() const = 0;

	/** Create the concrete operating system to be used
	 * @return Unique pointer to the operating system object
	 */
	static std::unique_ptr<IOpSys> Create();

	/** Request window creation
	 * @param title The title of the window 
	 * @param width The width of the window (in pixels)
	 * @param height The height of the window (in pixels)
	 * @return Unique pointer to the window
	 */
	virtual std::unique_ptr<IWindow> CreateSystemWindow(const char* title, u32 width, u32 height) = 0;

	/** Set the console text color
	 * @param color The new color of the text
	 */
	virtual void SetTextColor(TextColor color) = 0;

	/** Set the console text color to whatever it was when the program began */
	virtual void RestoreDefaultTextColor() = 0;

	/** Return a timestamp with the formatting [hour:minute:second:millisecond] */
	virtual const char* GetFormattedTimestamp() = 0;

	/** Turn on log colors */
	virtual void EnableLogColors() = 0;

protected:
	// --------------------------------------------------------------------- //
	// Protected Member Variables
	// --------------------------------------------------------------------- //

	/** Single buffer used for every timestamp serialization */
	char m_timestampBuffer[64];

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}