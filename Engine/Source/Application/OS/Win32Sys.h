#pragma once
/** \file Win32Sys.h
 * Windows OS object 
 */
// Created by Billy Graban

#include ".\IOpSys.h"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class Win32Sys
 * @brief Windows OS object 
 */
class Win32Sys
	: public IOpSys
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	Win32Sys();

	/** Default Destructor */
	~Win32Sys();

	/** Returns the name of the operating system (for logging purposes)
	 * @return "Windows"
	 */
	const char* GetSystemName() const override { return "Windows"; }

	/** Request window creation
	 * @param title The title of the window
	 * @param width The width of the window (in pixels)
	 * @param height The height of the window (in pixels)
	 * @return Unique pointer to the window
	 */
	virtual std::unique_ptr<IWindow> CreateSystemWindow(const char* title, u32 width, u32 height) override;

	/** Set the console text color
	 * @param color The new color of the text
	 */
	virtual void SetTextColor(TextColor color) override;

	/** Set the console text color to whatever it was when the program began */
	virtual void RestoreDefaultTextColor() override;

	/** Return a timestamp with the formatting [hour:minute:second:millisecond] */
	virtual const char* GetFormattedTimestamp() override;

	/** Turn on log colors */
	virtual void EnableLogColors() override;


private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** Handle to the console (used for changing text color) */
	HANDLE m_consoleHandle;

	/** Initial console data (for restoring color) */
	CONSOLE_SCREEN_BUFFER_INFO m_screenBufferInfo;

	/** Current system time */
	SYSTEMTIME m_sysTime;

	/** Lookup table for colors and windows-specific values associated with those colors */
	static std::unordered_map<TextColor, u8> s_colorMap;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}