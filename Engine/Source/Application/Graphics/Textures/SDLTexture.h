#pragma once
/** \file SDLTexture.h
 * Resource holding SDL texture objects 
 */
// Created by Billy Graban

#include ".\ITexture.h"

#include <memory>

#include <SDL/SDL_render.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class SDLTexture
 * @brief Resource holding SDL texture objects 
 */
class SDLTexture
	: public ITexture
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Resource constructor
	 * @param pResource The raw data and filepath to load the texture from
	 */
	SDLTexture(IResource* pResource);

	/** Default constructor
	 * This should only be used by the IFont family for generating textures from strings
	 */
	SDLTexture();

	/** Default Destructor */
	~SDLTexture();

	/** Native pointer retreival
	 * @return Memory address of SDL_Texture
	 */
	void* GetNativeTexture() const override;

	/** Initialize the texture
	 * @param pRenderer The renderer to use when building the texture
	 * @param pSurface The surface to convert into a texture
	 * @return True if successful
	 */
	bool Init(SDL_Renderer* pRenderer, SDL_Surface* pSurface);

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** Unique pointer to the SDL_Texture */
	std::unique_ptr<SDL_Texture, decltype(&SDL_DestroyTexture)> m_pTexture;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //

	/** Use std::move to set internal texture */
	void SetSDLTexture(std::unique_ptr<SDL_Texture, decltype(&SDL_DestroyTexture)> pTexture) { m_pTexture = std::move(pTexture); }

};
}