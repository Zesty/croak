#include "SDLTexture.h"

#include <Utils/Log.h>

using crk::SDLTexture;

SDLTexture::SDLTexture(IResource* pResource)
	: ITexture(pResource)
	, m_pTexture(nullptr, nullptr)
{
}

crk::SDLTexture::SDLTexture()
	: ITexture()
	, m_pTexture(nullptr, nullptr)
{
}

SDLTexture::~SDLTexture()
{
	
}

void* crk::SDLTexture::GetNativeTexture() const
{
	return m_pTexture.get();
}

bool crk::SDLTexture::Init(SDL_Renderer* pRenderer, SDL_Surface* pSurface)
{
	m_pTexture = std::unique_ptr<SDL_Texture, decltype(&SDL_DestroyTexture)>(
		SDL_CreateTextureFromSurface(pRenderer, pSurface), SDL_DestroyTexture);

	if (!m_pTexture)
	{
		LOG(Error, "SDL could not load texture: %s", SDL_GetError());
		return false;
	}

	m_width = pSurface->w;
	m_height = pSurface->h;

	return true;
}
