#pragma once
/** \file ITexture.h
 * Resource Interface for textures
 */
// Created by Billy Graban

#include <Utils/Typedefs.h>
#include <Logic/Resources/IResource.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class ITexture
 * @brief Resource Interface for textures 
 */
class ITexture
	: public IResource
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //

	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Resource constructor
	 * @param pResource The raw data and filepath to load the texture from
	 */
	ITexture(IResource* pResource);

	/** Default constructor
	 * This should only be used by the IFont family for generating textures from strings
	 */
	ITexture();

	/** Default Destructor */
	virtual ~ITexture();

	/** Native pointer retreival
	 * @return Memory address of concrete texture resource
	 */
	virtual void* GetNativeTexture() const = 0;

protected:
	// --------------------------------------------------------------------- //
	// Protected Member Variables
	// --------------------------------------------------------------------- //
	u16 m_width;	/**< Width of texture */
	u16 m_height;	/**< Height of texture */

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	u16 GetWidth() const { return m_width; }			/**< Get width of texture */
	u16 GetHeight() const { return m_height; }			/**< Get height of texture */

	void SetWidth(u16 width) { m_width = width; }		/**< Set width of texture */
	void SetHeight(u16 height) { m_height = height; }	/**< Set height of texture */
};
}