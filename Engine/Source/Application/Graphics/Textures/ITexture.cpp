#include "ITexture.h"

using crk::ITexture;

ITexture::ITexture(IResource* pResource)
	: IResource(pResource->GetName(), pResource->GetData())
	, m_height(0)
	, m_width(0)
{
	
}

crk::ITexture::ITexture()
	: IResource("", std::vector<std::byte>())
	, m_width(0)
	, m_height(0)
{
}

ITexture::~ITexture()
{
	
}