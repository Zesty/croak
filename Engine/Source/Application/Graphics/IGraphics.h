#pragma once
/** \file IGraphics.h
 * Interface for graphics module (Renders to the screen) 
 */
// Created by Billy Graban

#include <memory>
#include <vector>

#include <Utils/Typedefs.h>
#include <Utils/CommonMath.h>
//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class IWindow;
	class SDLRenderer;
	class ITexture;
	class IResource;

	// TODO: Move this to a better place

	/** The number of pixels that represent a meter in world-space */
	constexpr int kPixelsPerMeter = 100;

	/** Starting width of the main window */
	constexpr int kWindowWidth = 600;

	/** Starting height of the main window */
	constexpr int kWindowHeight = 480;

/** \class IGraphics
 * @brief Interface for graphics module (Renders to the screen) 
 */
class IGraphics
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	IGraphics();

	/** Default Destructor */
	virtual ~IGraphics();

	/** Initialize the graphics module and tie it to the window 
	 * @param pWindow The window to tie the graphics to
	 * @return True if successful
	 */
	virtual bool Init(IWindow* pWindow) = 0;

	/** Clear the background to a specified color 
	 * @param red The red component (0-255)
	 * @param green The green component (0-255)
	 * @param blue The blue component (0-255)
	 * @return True if successful
	 */
	virtual bool StartDrawing(u8 red, u8 green, u8 blue) = 0;

	/** Present to the window */
	virtual void EndDrawing() = 0;

	/** Load a texture resource from memory
	 * @param pResource The raw data and filepath to use
	 * @return Shared pointer to the texture resource
	 */
	virtual std::shared_ptr<ITexture> LoadTexture(IResource* pResource) = 0;

	// Draw Textures

	/** Draw entire texture to cover the screen 
	 * @param pTexture Texture resource to draw
	 */
	virtual bool DrawTexture(ITexture* pTexture) = 0;

	/** Draw a rotated texture on a portion of the screen
	 * @param pTexture Texture resource to draw
	 * @param destination Rectangle (in pixel space) to draw the texture at
	 * @param angle Rotation amount (in degrees)
	 * @return True if successful
	 */
	virtual bool DrawTexture(ITexture* pTexture, const FRect& destination, f32 angle = 0.f) = 0;

	/** Draw a portion of a texture on a portion of the screen
	 * @param pTexture Texture resource to draw
	 * @param source Rectangle (in pixel space) to use from the texture
	 * @param destination Rectangle (in pixel space) to draw the texture at
	 * @return True if successful
	 */
	virtual bool DrawTexture(ITexture* pTexture, const FRect& source, const FRect& destination) = 0;

	/** Apply a tint to a texture
	 * This only applies RGB values and ignores alpha
	 * @param pTexture The texture resource to modify
	 * @param color The new tint applied to the texture
	 * @return True if successful
	 */
	virtual bool SetTextureTint(ITexture* pTexture, const FColor& color) = 0;

	/** Apply a transparency to a texture
	 * @param pTexture The texture resource to modify
	 * @param alpha New opacity of the texture (0 is transparent, 1 is opaque)
	 * @return True if successful
	 */
	virtual bool SetTextureAlpha(ITexture* pTexture, float alpha) = 0;

	// Draw Primitives

	/** Draw the outline of a rectangle on the screen 
	 * @param rect Where to draw the rectangle (in pixel space)
	 * @param color The color to draw the outline
	 * @return True if successful
	 */
	virtual bool DrawRect(const FRect& rect, const FColor& color) = 0;

	/** Draw a filled rectangle on the screen 
	 * @param rect Where to draw the rectangle (in pixel space)
	 * @param color The color to fill the rectangle
	 * @return True if successful
	 */
	virtual bool FillRect(const FRect& rect, const FColor& color) = 0;

	/** Draw a line on the screen
	 * @param start The location (in pixel space) of the start of the line
	 * @param end The location (in pixel space) of the end of the line
	 * @param color The color of the line
	 * @return True if successful
	 */
	virtual bool DrawLine(const FVec2& start, const FVec2& end, const FColor& color) = 0;

	/** Draw the outline of a circle on the screen 
	 * @param center The center of the circle (in pixel space)
	 * @param radius The radius of the circle (in pixels)
	 * @param segments The number of line segments used for the circle
	 * @param color The color of the circle's outline
	 * @return True if successful
	 */
	virtual bool DrawCircle(const FVec2& center, f32 radius, u32 segments, const FColor& color) = 0;

	/** Draw an N-sided polygon to the screen
	 * @param points Vector of points (in pixel space) to draw
	 * @param color The color of the polygon
	 */
	virtual bool DrawPolygon(const std::vector<FVec2>& points, const FColor& color) = 0;

	/** Create the concrete graphics system to be used
	 * This can be changed through Preprocessor defines per configuration
	 * @return Unique pointer to the graphics system
	 */
	static std::unique_ptr<IGraphics> Create();

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
};
}