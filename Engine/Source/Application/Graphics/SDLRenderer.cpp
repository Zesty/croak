#include "SDLRenderer.h"

using crk::SDLRenderer;

#include <SDL/SDL_image.h>

#include <Application/Window/SDLWindow.h>
#include <Application/Graphics/Textures/SDLTexture.h>
#include <Utils/Log.h>
#include <Logic/Resources/IResource.h>

SDLRenderer::SDLRenderer()
	: m_pRenderer(nullptr, nullptr)
{
}

SDLRenderer::~SDLRenderer()
{
	
}

bool crk::SDLRenderer::Init(IWindow* pWindow)
{
	// Font module requires TARGETTEXTURE
	SDL_Window* pSDLWindow = reinterpret_cast<SDL_Window*>(pWindow->GetNativeWindow());
	m_pRenderer = std::unique_ptr<SDL_Renderer, decltype(&SDL_DestroyRenderer)>(
		SDL_CreateRenderer(pSDLWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_TARGETTEXTURE),
		&SDL_DestroyRenderer
		);

	if (!m_pRenderer)
	{
		LOG(Error, "Failed to create Renderer: %s", SDL_GetError());
		return false;
	}

	SDL_SetRenderDrawBlendMode(m_pRenderer.get(), SDL_BLENDMODE_BLEND);
	
	return true;
}

bool crk::SDLRenderer::StartDrawing(u8 red, u8 green, u8 blue)
{
	SDL_SetRenderDrawColor(m_pRenderer.get(), red, green, blue, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(m_pRenderer.get());
	
	// TODO: Check for SDL errors
	return true;
}

void crk::SDLRenderer::EndDrawing()
{
	SDL_RenderPresent(m_pRenderer.get());
}

std::shared_ptr<crk::ITexture> crk::SDLRenderer::LoadTexture(IResource* pResource)
{
	const char* filepath = pResource->GetName().c_str();

	if (!m_pRenderer)
	{
		LOG(Error, "Attempted to load texture %s before creating Renderer", filepath);
		return nullptr;
	}

	// No need to use smart pointers here if I clean it up myself
	SDL_RWops* pOps = SDL_RWFromConstMem(pResource->GetData().data(), (int)pResource->GetData().size());
	SDL_Surface* pSurface = IMG_Load_RW(pOps, 0);
	if (!pSurface)
	{
		LOG(Error, "Could not load image: %s", IMG_GetError());
		return nullptr;
	}

	// Load the texture
	SDLTexture* pTexture = new SDLTexture(pResource);
	if (!pTexture->Init(m_pRenderer.get(), pSurface))
	{
		LOG(Error, "Failed to initialize an SDLTexture");
		SDL_FreeSurface(pSurface);
		delete pTexture;

		return nullptr;
	}

	LOG(Resources, "Loaded image: %s", filepath);
	SDL_FreeSurface(pSurface);

	return std::shared_ptr<SDLTexture>(pTexture);
}

bool crk::SDLRenderer::DrawTexture(ITexture* pTexture)
{
	if (pTexture == nullptr)
	{
		LOG(Error, "Tried to draw a texture that does not exist");
		return false;
	}

	SDL_Texture* pToDraw = reinterpret_cast<SDL_Texture*>(pTexture->GetNativeTexture());
	int result = SDL_RenderCopy(m_pRenderer.get(), pToDraw, nullptr, nullptr);
	
	if (result)
	{
		LOG(Error, "Could not render texture: %s", SDL_GetError());
		return false;
	}

	return true;
}

bool crk::SDLRenderer::DrawTexture(ITexture* pTexture, const FRect& dest, f32 angle)
{
	if (pTexture == nullptr)
	{
		LOG(Error, "Tried to draw a texture that does not exist");
		return false;
	}

 	SDL_Rect destRect = ToSDLRect(dest);
	SDL_Texture* pToDraw = reinterpret_cast<SDL_Texture*>(pTexture->GetNativeTexture());
	int result = SDL_RenderCopyEx(m_pRenderer.get(), pToDraw, nullptr, &destRect, angle, nullptr, SDL_RendererFlip::SDL_FLIP_NONE);

	if (result)
	{
		LOG(Error, "Could not render texture: %s", SDL_GetError());
		return false;
	}

	return true;
}

bool crk::SDLRenderer::DrawTexture(ITexture* pTexture, const FRect& source, const FRect& destination)
{
	if (pTexture == nullptr)
	{
		LOG(Error, "Tried to draw a texture that does not exist");
		return false;
	}

	SDL_Rect src = ToSDLRect(source);
	SDL_Rect dest = ToSDLRect(destination);
	SDL_Texture* pToDraw = reinterpret_cast<SDL_Texture*>(pTexture->GetNativeTexture());
	int result = SDL_RenderCopy(m_pRenderer.get(), pToDraw, &src, &dest);

	if (result)
	{
		LOG(Error, "Could not render texture: %s", SDL_GetError());
		return false;
	}

	return true;
}

bool crk::SDLRenderer::SetTextureTint(ITexture* pTexture, const FColor& color)
{
	if (pTexture == nullptr)
	{
		LOG(Error, "Tried to tint a texture that does not exist");
		return false;
	}

	SDL_Color tint = ToSDLColor(color);
	SDL_Texture* pToTint = reinterpret_cast<SDL_Texture*>(pTexture->GetNativeTexture());
	int result = SDL_SetTextureColorMod(pToTint, tint.r, tint.g, tint.b);

	if (result)
	{
		LOG(Error, "Could not tint texture: %s", SDL_GetError());
		return false;
	}

	return true;
}

bool crk::SDLRenderer::SetTextureAlpha(ITexture* pTexture, float alpha)
{
	if (pTexture == nullptr)
	{
		LOG(Error, "Tried to apply alpha to a texture that does not exist");
		return false;
	}

	u8 convertedAlpha = (u8)(alpha * 255);
	SDL_Texture* pToSetAlpha = reinterpret_cast<SDL_Texture*>(pTexture->GetNativeTexture());
	int result = SDL_SetTextureAlphaMod(pToSetAlpha, convertedAlpha);

	if (result)
	{
		LOG(Error, "Could not apply alpha to texture: %s", SDL_GetError());
		return false;
	}

	return true;
}

bool crk::SDLRenderer::DrawRect(const FRect& rect, const FColor& color)
{
	SDL_Color c = ToSDLColor(color);
	int result = SDL_SetRenderDrawColor(m_pRenderer.get(), c.r, c.g, c.b, c.a);
	if (result)
	{
		LOG(Error, "Failed to set SDL Line color: %s", SDL_GetError());
		return false;
	}

	SDL_FRect r = ToSDLRectF(rect);
	result = SDL_RenderDrawRectF(m_pRenderer.get(), &r);

	if (result)
	{
		LOG(Error, "Failed to draw SDL Rect: %s", SDL_GetError());
		return false;
	}

	return true;
}

bool crk::SDLRenderer::FillRect(const FRect& rect, const FColor& color)
{
	SDL_Color c = ToSDLColor(color);
	int result = SDL_SetRenderDrawColor(m_pRenderer.get(), c.r, c.g, c.b, c.a);
	if (result)
	{
		LOG(Error, "Failed to set SDL Line color: %s", SDL_GetError());
		return false;
	}

	SDL_FRect r = ToSDLRectF(rect);
	r.x *= kPixelsPerMeter;
	r.y *= kPixelsPerMeter;
	r.w *= kPixelsPerMeter;
	r.h *= kPixelsPerMeter;
	result = SDL_RenderFillRectF(m_pRenderer.get(), &r);

	if (result)
	{
		LOG(Error, "Failed to draw SDL Rect: %s", SDL_GetError());
		return false;
	}

	return true;
}

bool crk::SDLRenderer::DrawLine(const FVec2& start, const FVec2& end, const FColor& color)
{
	SDL_Color c = ToSDLColor(color);
	int result = SDL_SetRenderDrawColor(m_pRenderer.get(), c.r, c.g, c.b, c.a);
	if (result)
	{
		LOG(Error, "Failed to set SDL Line color: %s", SDL_GetError());
		return false;
	}
	
	result = SDL_RenderDrawLineF(m_pRenderer.get(), 
		start.x * kPixelsPerMeter, start.y * kPixelsPerMeter, end.x * kPixelsPerMeter, end.y * kPixelsPerMeter);
	
	if (result)
	{
		LOG(Error, "Failed to draw SDL Line: %s", SDL_GetError());
		return false;
	}

	return true;
}

bool crk::SDLRenderer::DrawCircle(const FVec2& center, f32 radius, u32 segments, const FColor& color)
{
	m_points.clear();
	m_points.reserve(static_cast<size_t>(segments) + 1);

	f32 segmentRadianStep = f32(M_PI * 2) / segments;
	f32 radians = 0.f;
	SDL_FPoint point;
	for (u32 i = 0; i < segments; ++i)
	{
		point.x = (center.x * kPixelsPerMeter) + sinf(radians) * kPixelsPerMeter * radius;
		point.y = (center.y * kPixelsPerMeter) + cosf(radians) * kPixelsPerMeter * radius;

		m_points.push_back(point);
		radians += segmentRadianStep;
	}

	m_points.push_back(m_points[0]);

	SDL_Color c = ToSDLColor(color);
	SDL_SetRenderDrawColor(m_pRenderer.get(), c.r, c.g, c.b, c.a);

	SDL_RenderDrawLinesF(m_pRenderer.get(), m_points.data(), segments + 1);

	return true;
}

bool crk::SDLRenderer::DrawPolygon(const std::vector<FVec2>& points, const FColor& color)
{
	m_points.clear();
	for (auto& pt : points)
	{
		SDL_FPoint point{ pt.x * kPixelsPerMeter, pt.y * kPixelsPerMeter };
		m_points.push_back(point);
	}

	SDL_Color c = ToSDLColor(color);
	c.r = 255;
	c.g = 0;
	c.b = 255;
	SDL_SetRenderDrawColor(m_pRenderer.get(), c.r, c.g, c.b, c.a);

	SDL_RenderDrawLinesF(m_pRenderer.get(), m_points.data(), static_cast<int>(points.size()));
	return true;
}

SDL_Rect crk::SDLRenderer::ToSDLRect(const FRect& rect)
{
	SDL_Rect result{ (int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height };
	return result;
}

SDL_FRect crk::SDLRenderer::ToSDLRectF(const FRect& rect)
{
	SDL_FRect result{ rect.x, rect.y, rect.width, rect.height };
	return result;
}

SDL_Color crk::SDLRenderer::ToSDLColor(const FColor& color)
{
	// Doing this piecemeal to avoid warning:
	// conversion from 'u8' to 'Uint8' requires a narrowing conversion
	SDL_Color result = { (u8)(color.r * 255), (u8)(color.g * 255), (u8)(color.b * 255), (u8)(color.a * 255) };
	//SDL_Color result;
	//result.r = (u8)(color.r * 255);
	//result.g = (u8)(color.g * 255);
	//result.b = (u8)(color.b * 255);
	//result.a = (u8)(color.a * 255);

	return result;
}
