#pragma once
/** \file SDLRenderer.h
 * Renderer using SDL_Renderer 
 */
// Created by Billy Graban

#include <unordered_map>
#include <string>

#include ".\IGraphics.h"

#include <SDL/SDL_render.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class IResource;

/** \class SDLRenderer
 * @brief Renderer using SDL_Renderer 
 */
class SDLRenderer
	: public IGraphics
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	SDLRenderer();

	/** Default Destructor */
	~SDLRenderer();

	// Inherited via IGraphics

	/** Initialize the SDL_Renderer and tie it to the window
	 * This assumes that the IWindow passed will be an SDL_Window under the hood
	 * @param pWindow The window to tie the graphics to
	 * @return True if successful
	 */
	virtual bool Init(IWindow* pWindow) override;

	/** Clear the background to a specified color
	 * @param red The red component (0-255)
	 * @param green The green component (0-255)
	 * @param blue The blue component (0-255)
	 * @return True if successful
	 */
	virtual bool StartDrawing(u8 red, u8 green, u8 blue) override;

	/** Present to the window */
	virtual void EndDrawing() override;

	/** Load an SDL_Texture resource from memory
	 * @param pResource The raw data and filepath to use
	 * @return Shared pointer to the texture resource
	 */
	virtual std::shared_ptr<ITexture> LoadTexture(IResource* pResource) override;

	// Draw Textures

	/** Draw entire texture to cover the screen
	 * @param pTexture Texture resource to draw
	 */
	virtual bool DrawTexture(ITexture* pTexture) override;

	/** Draw a rotated texture on a portion of the screen
	 * @param pTexture Texture resource to draw
	 * @param destination Rectangle (in pixel space) to draw the texture at
	 * @param angle Rotation amount (in degrees)
	 * @return True if successful
	 */
	virtual bool DrawTexture(ITexture* pTexture, const FRect& destination, f32 angle = 0.f) override;

	/** Draw a portion of a texture on a portion of the screen
	 * @param pTexture Texture resource to draw
	 * @param source Rectangle (in pixel space) to use from the texture
	 * @param destination Rectangle (in pixel space) to draw the texture at
	 * @return True if successful
	 */
	virtual bool DrawTexture(ITexture* pTexture, const FRect& source, const FRect& destination) override;

	/** Apply a tint to a texture
	 * This only applies RGB values and ignores alpha
	 * @param pTexture The texture resource to modify
	 * @param color The new tint applied to the texture
	 * @return True if successful
	 */
	virtual bool SetTextureTint(ITexture* pTexture, const FColor& color) override;

	/** Apply a transparency to a texture
	 * @param pTexture The texture resource to modify
	 * @param alpha New opacity of the texture (0 is transparent, 1 is opaque)
	 * @return True if successful
	 */
	virtual bool SetTextureAlpha(ITexture* pTexture, float alpha) override;

	// Draw Primitives

	/** Draw the outline of a rectangle on the screen
	 * @param rect Where to draw the rectangle (in pixel space)
	 * @param color The color to draw the outline
	 * @return True if successful
	 */
	virtual bool DrawRect(const FRect& rect, const FColor& color) override;

	/** Draw a filled rectangle on the screen
	 * @param rect Where to draw the rectangle (in pixel space)
	 * @param color The color to fill the rectangle
	 * @return True if successful
	 */
	virtual bool FillRect(const FRect& rect, const FColor& color) override;

	/** Draw a line on the screen
	 * @param start The location (in pixel space) of the start of the line
	 * @param end The location (in pixel space) of the end of the line
	 * @param color The color of the line
	 * @return True if successful
	 */
	virtual bool DrawLine(const FVec2& start, const FVec2& end, const FColor& color) override;

	/** Draw the outline of a circle on the screen
	 * @param center The center of the circle (in pixel space)
	 * @param radius The radius of the circle (in pixels)
	 * @param segments The number of line segments used for the circle
	 * @param color The color of the circle's outline
	 * @return True if successful
	 */
	virtual bool DrawCircle(const FVec2& center, f32 radius, u32 segments, const FColor& color) override;

	/** Draw an N-sided polygon to the screen
	 * @param points Vector of points (in pixel space) to draw
	 * @param color The color of the polygon
	 */
	virtual bool DrawPolygon(const std::vector<FVec2>& points, const FColor& color) override;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** Unique pointer to the SDL_Renderer */
	std::unique_ptr<SDL_Renderer, decltype(&SDL_DestroyRenderer)> m_pRenderer;

	/** Prevents allocating points multiple times when drawing circles */
	std::vector<SDL_FPoint> m_points;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //

	/** Helper function for converting rectangles (Truncates floating point values) */
	SDL_Rect ToSDLRect(const FRect& rect);

	/** Helper function for converting rectangles */
	SDL_FRect ToSDLRectF(const FRect& rect);

	/** Helper function for converting colors */
	SDL_Color ToSDLColor(const FColor& color);

public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	SDL_Renderer* GetRenderer() const { return m_pRenderer.get(); }		/**< Returns raw pointer to the SDL_Renderer */
};
}