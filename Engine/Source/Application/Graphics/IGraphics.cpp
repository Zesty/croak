#include "IGraphics.h"

using crk::IGraphics;

#include <Application/Graphics/SDLRenderer.h>
#include <Utils/Log.h>

IGraphics::IGraphics()
{
	
}

IGraphics::~IGraphics()
{
	
}

std::unique_ptr<IGraphics> crk::IGraphics::Create()
{ 
#ifdef _SDL_RENDERER
	return std::make_unique<SDLRenderer>();
#else
	LOG(Error, "Graphics system determined by Preprocessor has not been implemented");
	return nullptr;
#endif
}
