#pragma once
/** \file SDLFontLoader.h
 * Loads fonts using SDL_ttf
 */
// Created by Billy Graban

#include ".\IFontLoader.h"

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class IResource;

/** \class SDLFontLoader
 * @brief Loads fonts using SDL_ttf 
 */
class SDLFontLoader
	: public IFontLoader
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	SDLFontLoader();

	/** Default Destructor */
	~SDLFontLoader();

	/** Initialize SDL_ttf
	 * @return True if successful
	 */
	bool Init() override;

	/** Create the font resource itself
	 * @param pResource Resource to load raw data from
	 * @param ptSize Desired font size
	 * @param pGraphics Graphics system to use when building font
	 * @return Shared pointer to the new font resource
	 */
	std::shared_ptr<IFont> Load(IResource* pResource, int ptSize, IGraphics* pGraphics) override;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}