#include "SDLFont.h"

#include <Utils/Log.h>
#include <Application/Graphics/IGraphics.h>
#include <Application/Graphics/Textures/SDLTexture.h>
#include <Application/Graphics/SDLRenderer.h>

using crk::SDLFont;

SDLFont::SDLFont(IResource* pResource, int ptSize, crk::IGraphics* pGraphics)
	: IFont(pResource, ptSize, pGraphics)
	, m_pFont(nullptr, nullptr)
	, m_pFontAtlas(nullptr, nullptr)
{
	
}

SDLFont::~SDLFont()
{
}

void* crk::SDLFont::GetNativeFont()
{
	return nullptr;
}

std::shared_ptr<crk::ITexture> crk::SDLFont::BuildTextureFromString(const std::string& str)
{
	std::shared_ptr<SDLTexture> pTexture = std::make_shared<SDLTexture>();
	int width;
	int height;
	TTF_SizeText(m_pFont.get(), str.c_str(), &width, &height);

	auto pSDL_Texture = SDL_CreateTexture(m_pRenderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_TARGET,
		width + m_offset, height + m_offset);
	SDL_SetRenderTarget(m_pRenderer, pSDL_Texture);

	SDL_SetTextureBlendMode(pSDL_Texture, SDL_BlendMode::SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(m_pRenderer, 0, 0, 0, 0);
	SDL_RenderClear(m_pRenderer);

	SDL_Rect dest{ 0, 0, 0, 0 };
	for (auto ch : str)
	{
		const Glyph& glyph = m_glyphMap[ch];
		SDL_Rect source{ glyph.x, glyph.y, glyph.w, glyph.h };
		dest.w = source.w;
		dest.h = source.h;

		SDL_RenderCopy(m_pRenderer, m_pFontAtlas.get(), &source, &dest);
		dest.x += glyph.advance;
	}

	SDL_SetRenderTarget(m_pRenderer, nullptr);

	pTexture->SetSDLTexture(std::unique_ptr<SDL_Texture, decltype(&SDL_DestroyTexture)>(pSDL_Texture, SDL_DestroyTexture));
	pTexture->SetWidth(width);
	pTexture->SetHeight(height);

	return pTexture;
}

bool crk::SDLFont::Init(TTF_Font* pFont)
{
	m_pFont = std::unique_ptr<TTF_Font, decltype(&TTF_CloseFont)>(pFont, TTF_CloseFont);
	if (!m_pFont)
	{
		LOG(Error, "Failied to initialize SDLFont: %s", TTF_GetError());
		return false;
	}

	auto pSDLRenderer = static_cast<SDLRenderer*>(GetGraphics());
	m_pRenderer = pSDLRenderer->GetRenderer();

	if (!m_pRenderer)
	{
		LOG(Error, "SDLFont requires an SDL_Renderer to build a Font Atlas");
		return false;
	}

	LOG(Todo, "Consider changing the FontAtlas size dynamically");
	u16 imageSizeInPixels = 512;

	auto pAtlasTexture = SDL_CreateTexture(m_pRenderer, SDL_PIXELFORMAT_ABGR8888, SDL_TextureAccess::SDL_TEXTUREACCESS_TARGET,
		imageSizeInPixels, imageSizeInPixels);
	m_pFontAtlas = std::unique_ptr<SDL_Texture, decltype(&SDL_DestroyTexture)>(pAtlasTexture, SDL_DestroyTexture);

	// Set render target
	int result = SDL_SetRenderTarget(m_pRenderer, m_pFontAtlas.get());
	if (result != 0)
	{
		LOG(Error, "SDL Error: %s", SDL_GetError());
	}

	SDL_SetTextureBlendMode(m_pFontAtlas.get(), SDL_BlendMode::SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(m_pRenderer, 0, 0, 0, 0);
	SDL_RenderClear(m_pRenderer);

	// Atlas x/y
	int startX = 0;
	int startY = 0;

	LOG(Warning, "SDLFont shadow offsets are hardcoded to 4 pixels");
	m_offset = 4;

	// Build Glyphs
	for (char ch = 32; ch < 127; ++ch)
	{
		SDL_Surface* pGlyphSurfaceBack = TTF_RenderGlyph_Blended(m_pFont.get(), ch, SDL_Color{ 0, 0, 0, 255 });
		SDL_Surface* pGlyphSurface = TTF_RenderGlyph_Blended(m_pFont.get(), ch, SDL_Color{ 255, 255, 255, 255 });

		//int minX, minY, maxX, maxY, advance;
		Glyph glyph;
		TTF_GlyphMetrics(m_pFont.get(), ch, &glyph.minX, &glyph.maxX, &glyph.minY, &glyph.maxY, &glyph.advance);
		glyph.w = pGlyphSurface->w + m_offset;
		glyph.h = pGlyphSurface->h + m_offset;

		SDL_Texture* pGlyphTextureBack = SDL_CreateTextureFromSurface(m_pRenderer, pGlyphSurfaceBack);
		SDL_Texture* pGlyphTexture = SDL_CreateTextureFromSurface(m_pRenderer, pGlyphSurface);

		if (startX + pGlyphSurface->w + m_offset >= imageSizeInPixels)
		{
			startX = 0;
			startY += pGlyphSurface->h + m_offset;
		}

		glyph.x = startX;
		glyph.y = startY;
		m_glyphMap.emplace(ch, glyph);

		SDL_Rect dest{ startX + m_offset, startY + m_offset, pGlyphSurface->w, pGlyphSurface->h };
		SDL_FreeSurface(pGlyphSurface);
		SDL_FreeSurface(pGlyphSurfaceBack);

		SDL_RenderCopy(m_pRenderer, pGlyphTextureBack, nullptr, &dest);
		dest.x -= m_offset;
		dest.y -= m_offset;
		SDL_RenderCopy(m_pRenderer, pGlyphTexture, nullptr, &dest);

		startX += dest.w + m_offset;

		SDL_DestroyTexture(pGlyphTexture);
		SDL_DestroyTexture(pGlyphTextureBack);
	}

	SDL_SetRenderTarget(m_pRenderer, nullptr);
	LOG(Resources, "Font Atlas built: %s - %d", GetName().c_str(), GetPointSize());
	return true;
}
