#include "SDLFontLoader.h"

using crk::SDLFontLoader;

#include <SDL/SDL_ttf.h>

#include <Utils/Log.h>
#include <Logic/Resources/IResource.h>
#include <Application/Graphics/Fonts/SDLFont.h>

SDLFontLoader::SDLFontLoader()
{
	
}

SDLFontLoader::~SDLFontLoader()
{
	TTF_Quit();
}

bool crk::SDLFontLoader::Init()
{
	if (TTF_Init() != 0)
	{
		LOG(Error, "Failed to initialize TTF: %s", TTF_GetError());
		return false;
	}

	return true;
}

std::shared_ptr<crk::IFont> crk::SDLFontLoader::Load(IResource* pResource, int ptSize, IGraphics* pGraphics)
{
	SDL_RWops* pOps = SDL_RWFromConstMem(pResource->GetData().data(), (int)pResource->GetData().size());
	TTF_Font* pFont = TTF_OpenFontRW(pOps, 0, ptSize);

	if (!pFont)
	{
		LOG(Error, "Failed to open font: %s", TTF_GetError());
		return nullptr;
	}

	SDLFont* pSDLFont = new SDLFont(pResource, ptSize, pGraphics);
	if (!pSDLFont->Init(pFont))
	{
		LOG(Error, "Failed to initialized SDLFont: %s", TTF_GetError());
		TTF_CloseFont(pFont);
		delete pSDLFont;
		return nullptr;
	}

	LOG(Todo, "Use the correct font resource name (with size) when logging load success");
	LOG(Resources, "Font Loaded: %s", pResource->GetName().c_str());

	return std::shared_ptr<IFont>(pSDLFont);
}
