#include "IFontLoader.h"

#include <Application/Graphics/Fonts/SDLFontLoader.h>
#include <Utils/Log.h>

using crk::IFontLoader;

IFontLoader::IFontLoader()
{
	
}

IFontLoader::~IFontLoader()
{
	
}

std::unique_ptr<IFontLoader> crk::IFontLoader::Create()
{
#ifdef _SDL_FONT_LOADER
	return std::make_unique<SDLFontLoader>();
#else
	LOG(Error, "FontLoader system determined by Preprocessor has not been implemented")
#endif
}
