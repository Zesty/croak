#pragma once
/** \file IFont.h
 * Resource Interface for Fonts 
 */
// Created by Billy Graban

#include <Logic/Resources/IResource.h>

#include <memory>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class IGraphics;
	class ITexture;

/** \class IFont
 * @brief Resource Interface for Fonts 
 */
class IFont
	: public IResource
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor 
	 * @param pResource Data and filepath to use
	 * @param ptSize The size of the font to build
	 * @param pGraphics Graphics module for building the texture atlas
	 */
	IFont(IResource* pResource, int ptSize, IGraphics* pGraphics);

	/** Default Destructor */
	virtual ~IFont();

	/** Native pointer retreival
	 * @return Memory address of concrete font resource
	 */
	virtual void* GetNativeFont() = 0;

	/** Creates a texture containing the text using the font in this object
	 * @param str The string to write to the texture
	 * @return Shared pointer to the new texture
	 */
	virtual std::shared_ptr<ITexture> BuildTextureFromString(const std::string& str) = 0;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** Size of the height in pixels */
	int m_pointSize;

	/** Creates font atalasses */
	IGraphics* m_pGraphics;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	int GetPointSize() const { return m_pointSize; }		/**< Return the point size */
	IGraphics* GetGraphics() const { return m_pGraphics; }	/**< Return the Graphics object */

};
}