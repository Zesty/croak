#pragma once
/** \file IFontLoader.h
 * Interface for loading fonts 
 */
// Created by Billy Graban

#include <memory>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class IFont;
	class IResource;
	class IGraphics;

/** \class IFontLoader
 * @brief Interface for loading fonts 
 */
class IFontLoader
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	IFontLoader();

	/** Default Destructor */
	virtual ~IFontLoader();

	/** Create the concrete FontLoader to use
	 * This can be changed through Preprocessor defines per configuration
	 * @return Unique pointer to the font loader
	 */
	static std::unique_ptr<IFontLoader> Create();

	/** Initialize the loader 
	 * @return True if successful
	 */
	virtual bool Init() = 0;

	/** Create the font resource itself
	 * @param pResource Resource to load raw data from
	 * @param ptSize Desired font size
	 * @param pGraphics Graphics system to use when building font
	 * @return Shared pointer to the new font resource
	 */
	virtual std::shared_ptr<IFont> Load(IResource* pResource, int ptSize, IGraphics* pGraphics) = 0;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}