#pragma once
/** \file SDLFont.h
 * Resource holding SDL_ttf font objects 
 */
// Created by Billy Graban

#include ".\IFont.h"

#include <unordered_map>

#include <SDL/SDL_ttf.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class SDLFont
 * @brief Resource holding SDL_ttf font objects 
 */
class SDLFont
	: public IFont
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor
	 * @param pResource Data and filepath to use
	 * @param ptSize The size of the font to build
	 * @param pGraphics Graphics module for building the texture atlas
	 */
	SDLFont(IResource* pResource, int ptSize, IGraphics* pGraphics);

	/** Default Destructor */
	~SDLFont();

	/** Native pointer retreival
	 * @return Memory address of the SDL_ttf object
	 */
	void* GetNativeFont() override;

	/** Creates a texture containing the text using the font in this object
	 * @param str The string to write to the texture
	 * @return Shared pointer to the new texture
	 */
	std::shared_ptr<ITexture> BuildTextureFromString(const std::string& str) override;

	/** Create the Font Atlas as a single texture in memory
	 * Currently each character will have a black 'shadow' offset by a small amount (2, 2)
	 * @param pFont The font to use when building the atlas
	 * @return True if successful
	 */
	bool Init(TTF_Font* pFont);

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** Unique pointer to the font object */
	std::unique_ptr<TTF_Font, decltype(&TTF_CloseFont)> m_pFont;

	/** Unique pointer to the generated font atlas */
	std::unique_ptr<SDL_Texture, decltype(&SDL_DestroyTexture)> m_pFontAtlas;

	/** Draw textures to the font atlas */
	SDL_Renderer* m_pRenderer;

	/** Offset of shadow glyphs */
	int m_offset;

	/** \struct Glyph Information about each glyph rendered to the atlas */
	struct Glyph
	{
		int advance;	/**< How far to advance the glyph */
		int w;			/**< Width of glyph */
		int h;			/**< Height of glyph */
		int x;			/**< X position in atlas */
		int y;			/**< Y position in atlas */

		// Not sure if I'll ever need to use these
		int minX;		/**< Min x position */
		int maxX;		/**< Max x position */
		int minY;		/**< Min y position */
		int maxY;		/**< Max y position */
	};

	/** Map of chars and glyphs for O(1) access */
	std::unordered_map<char, Glyph> m_glyphMap;
	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //

};
}