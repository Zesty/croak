#include "IFont.h"

//#include <Application/Graphics/IGraphics.h>

using crk::IFont;

IFont::IFont(IResource* pResource, int ptSize, crk::IGraphics* pGraphics)
	: IResource(pResource->GetName(), pResource->GetData())
	, m_pointSize(ptSize)
	, m_pGraphics(pGraphics)
{
	
}

IFont::~IFont()
{
	
}