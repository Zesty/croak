#include "ApplicationLayer.h"

#include <Utils/Log.h>
#include <Logic/Resources/ResourceCache.h>

using crk::ApplicationLayer;

ApplicationLayer::ApplicationLayer()
{
	
}

ApplicationLayer::~ApplicationLayer()
{
	
}

bool crk::ApplicationLayer::Init()
{

// This beautiful monstrosity is undefined at the end of this function
// It allows for verbose logging of failures and successes in the Init of the App
#define LogResult(variable, subject, verb) \
	if(!variable) \
	{ \
		LOG(Error, "Failed to "#verb" "#subject); \
		return false; \
	} \
	else \
	{ \
		LOG(Boot, #subject" "#verb"d successfully"); \
	}

	m_pSystem = IOpSys::Create();
	if (!m_pSystem)
	{
		// Logger has not been set up yet...  cout the error
		LOG_SIMPLE("Failed to create Operating System.  Make sure your OS is supported");
		return false;
	}
	else
	{
		LOG_SIMPLE("OS Object created");
	}

	// Manually get the console window handle in the OS to support color changing logs
	m_pSystem->EnableLogColors();

	LogResult(Log::Get()->Init(m_pSystem.get(), "./Temp/croak.log"), Logger, initialized);

	m_pGameLayer = CreateGameLayer();
	LogResult(m_pGameLayer, GameLayer, create);

	LOG(Boot, "Created game layer for: %s", m_pGameLayer->GetGameName());

	LOG(Todo, "Add EngineSettings so user can modify core values without recompiling");
	LOG(Todo, "Enable grid drawing");
	LOG(Todo, "FONTS!");

	m_screenDimensions.x = kWindowWidth;
	m_screenDimensions.y = kWindowHeight;
	m_pWindow = m_pSystem->CreateSystemWindow(m_pGameLayer->GetGameName(), 
		m_screenDimensions.x, m_screenDimensions.y);
	LogResult(m_pWindow, SystemWindow, create);
	
	LOG(Boot, "Created System Window on OS: %s", m_pSystem->GetSystemName());

	m_pGraphics = IGraphics::Create();
	LogResult(m_pWindow, Graphics, create);
	LogResult(m_pGraphics->Init(m_pWindow.get()), Graphics, initialize);

	// Keyboard
	auto pKeyboard = IKeyboard::Create();
	LogResult(pKeyboard, Keyboard, create);
	LogResult(pKeyboard->Init(), Keyboard, initialize);

	m_pWindow->AttachKeyboard(std::move(pKeyboard));

	// Mouse
	auto pMouse = IMouse::Create();
	LogResult(pMouse, Mouse, create);
	LogResult(pMouse->Init(), Mouse, initialize);

	m_pWindow->AttachMouse(std::move(pMouse));

	LOG(Boot, "Mouse and Keyboard attached to window");

	// Audio
	m_pAudio = IAudio::Create();
	LogResult(m_pAudio, Audio, create);
	LogResult(m_pAudio->Init(), Audio, initialize);

	// Physics
	m_pPhysics = IPhysicsSimulation::Create(FVec2(0, 9.8f));
	LogResult(m_pPhysics, Physics, create);
	LogResult(m_pPhysics->Init(m_pGraphics.get()), Physics, initialize);

	// Font Loader
	m_pFontLoader = IFontLoader::Create();
	LogResult(m_pFontLoader, FontLoader, create);
	LogResult(m_pFontLoader->Init(), FontLoader, initialize);

	// Resource Cache
	LogResult(ResourceCache::Get()->Init(m_pGraphics.get(), m_pAudio.get(), m_pFontLoader.get()), ResourceCache, initialize);

	// Init the game layer last because the views might need access to
	// other important modules.  Also the resource cache must be ready by this time
	LogResult(m_pGameLayer->Init(this), GameLayer, initialize);

	LOG(Boot, "ApplicationLayer Init successful");

#undef LogResult

	return true;
}

void crk::ApplicationLayer::Run()
{
	LOG(Boot, "Beginning main loop");

	using namespace std::chrono;
	time_point<high_resolution_clock> last = time_point<high_resolution_clock>::clock::now();

	// Main loop!
	while (m_pWindow->ProcessEvents())
	{
		time_point<high_resolution_clock> now = time_point<high_resolution_clock>::clock::now();
		auto deltaTime = duration<float>(now - last);
		float deltaSeconds = deltaTime.count();
		//LOG(Info, "Delta Seconds: %.3f", deltaSeconds);
		
		// Update Game (Input)
		m_pGameLayer->Update(deltaSeconds);

		m_pWindow->NextFrame();
		last = now;
	}
}

void crk::ApplicationLayer::Cleanup()
{
	LOG(Info, "Cleaning up application layer");

	m_pGameLayer->Cleanup();
	ResourceCache::Get()->Cleanup();

	m_pGraphics = nullptr;
	m_pWindow = nullptr;
	m_pGameLayer = nullptr;
	m_pPhysics = nullptr;
	m_pAudio = nullptr;

	Log::Get()->Cleanup();

	m_pSystem = nullptr;
}