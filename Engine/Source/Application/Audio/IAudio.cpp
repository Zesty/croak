#include "IAudio.h"

#include "SDLAudio.h"

#include <Utils/Log.h>

using crk::IAudio;

IAudio::IAudio()
{
	
}

IAudio::~IAudio()
{
	
}

std::unique_ptr<IAudio> crk::IAudio::Create()
{
#ifdef _SDL_AUDIO
	return std::make_unique<SDLAudio>();
#else
	LOG(Error, "Audio system determined by Preprocessor has not been implemented");
	return nullptr;
#endif
}
