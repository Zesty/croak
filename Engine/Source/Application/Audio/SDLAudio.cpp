#include "SDLAudio.h"

#include <cassert>

#include <SDL/SDL.h>

#include <Application/Audio/Sound/SDLSound.h>
#include <Application/Audio/Music/SDLMusic.h>
#include <Logic/Resources/IResource.h>
#include <Utils/Log.h>
#include <Logic/Event/Events/PlaySoundEvent.h>
#include <Logic/Event/EventDispatcher.h>

using crk::SDLAudio;

SDLAudio::SDLAudio()
	: m_initialized(false)
{
	
}

SDLAudio::~SDLAudio()
{
	if (m_initialized)
	{
		Mix_CloseAudio();
		Mix_Quit();
		SDL_QuitSubSystem(SDL_INIT_AUDIO);
	}
}

bool crk::SDLAudio::Init()
{
	if (SDL_InitSubSystem(SDL_INIT_AUDIO))
	{
		LOG(Error, "Failed to initialize SDL Audio: %s", Mix_GetError());
		return false;
	}

	m_initialized = true;

	if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024))
	{
		LOG(Error, "Failed to open SDL_mixer audio: %s", Mix_GetError());
		return false;
	}

	int mixFlags = MIX_INIT_MP3 | MIX_INIT_OGG;
	if (Mix_Init(mixFlags) != mixFlags)
	{
		LOG(Error, "Failed to initialize SDL_mixer: %s", Mix_GetError());
		return false;
	}

	EventDispatcher::Get()->AddEventListener(PlaySoundEvent::kEventId,
		[this](IEvent* pEvent)
		{
			assert(pEvent->GetEventId() == PlaySoundEvent::kEventId);
			auto pSoundEvent = static_cast<PlaySoundEvent*>(pEvent);
			PlaySound(pSoundEvent->GetResource());
		});

	return true;
}

bool crk::SDLAudio::PlayMusic(IMusic* pMusicResource)
{
	auto pMusic = Mix_LoadMUS("Assets/Audio/HallOfTheMountainKing.ogg");
	//auto pMusic = static_cast<Mix_Music*>(pMusicResource->GetNativeMusic());
	if (Mix_PlayMusic(pMusic, -1))
	{
		LOG(Error, "Failed to play music file %s: %s", pMusicResource->GetName(), Mix_GetError());
		return false;
	}

	return true;
}

bool crk::SDLAudio::PlaySound(ISound* pSoundResource)
{
	auto pChunk = static_cast<Mix_Chunk*>(pSoundResource->GetNativeSound());
	Mix_PlayChannel(-1, pChunk, 0);
	return true;
}

std::shared_ptr<crk::ISound> crk::SDLAudio::LoadSound(IResource* pResource)
{
	// Load it
	SDL_RWops* pOps = SDL_RWFromConstMem(pResource->GetData().data(), (int)pResource->GetData().size());
	Mix_Chunk* pChunk = Mix_LoadWAV_RW(pOps, 0);

	if(!pChunk)
	{
		LOG(Error, "Failed to load sound %s: %s", pResource->GetName().c_str(), Mix_GetError());
		return nullptr;
	}

	auto pSound = new SDLSound(pResource);
	if (!pSound->Init(pChunk))
	{
		LOG(Error, "Failed to initialize sound: %s", Mix_GetError());
		Mix_FreeChunk(pChunk);
		delete pSound;
		return nullptr;
	}

	LOG(Resources, "Successfully loaded: %s", pResource->GetName().c_str());
	return std::shared_ptr<ISound>(pSound);
}

std::shared_ptr<crk::IMusic> crk::SDLAudio::LoadMusic(IResource* pResource)
{
	// Load raw memory
	SDL_RWops* pOps = SDL_RWFromConstMem(pResource->GetData().data(), (int)pResource->GetData().size());
	Mix_Music* pMusic = Mix_LoadMUS_RW(pOps, 0);

	// Check load success
	if (!pMusic)
	{
		LOG(Error, "Failed to load SDLMusic: %s", Mix_GetError());
		return nullptr;
	}

	// Initialize
	auto pSDLMusic = new SDLMusic(pResource);
	if (!pSDLMusic->Init(pMusic))
	{
		LOG(Error, "Failed to initialize SDLMusic: %s", Mix_GetError());
		Mix_FreeMusic(pMusic);
		delete pSDLMusic;
		return nullptr;
	}

	// Success!
	LOG(Resources, "Successfully loaded: %s", pResource->GetName().c_str());
	return std::shared_ptr<IMusic>(pSDLMusic);
}
