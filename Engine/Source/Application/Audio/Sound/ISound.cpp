#include "ISound.h"

#include <Utils/Log.h>

using crk::ISound;

ISound::ISound(IResource* pResource)
	: IResource(pResource->GetName(), pResource->GetData())
{
}

ISound::~ISound()
{
	
}