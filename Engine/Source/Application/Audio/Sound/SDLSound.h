#pragma once
/** \file SDLSound.h
 * Resource holding SDL_Chunk 
 */
// Created by Billy Graban

#include ".\ISound.h"

#include <memory>
#include <SDL/SDL_mixer.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class SDLSound
 * @brief Resource holding SDL_Chunk 
 */
class SDLSound
	: public ISound
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor 
	 * @param pResource Contains raw data and file location
	 */
	SDLSound(IResource* pResource);

	/** Default Destructor */
	~SDLSound();

	/** Native pointer retreival
	 * @return Memory address of concrete sound resource
	 */
	void* GetNativeSound() override;

	/** Create unique pointer for internal storage
	 * @param pChunk SDL_mixer chunk to store in Resource
	 * @return True if successful
	 */
	bool Init(Mix_Chunk* pChunk);

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** Unique pointer of SDL_mixer chunk */
	std::unique_ptr<Mix_Chunk, decltype(&Mix_FreeChunk)> m_pChunk;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}