#pragma once
/** \file ISound.h
 * Resource Interface for sound effects (not streamed) 
 */
// Created by Billy Graban

#include <Logic/Resources/IResource.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class ISound
 * @brief Resource Interface for sound effects (not streamed) 
 */
class ISound
	: public IResource
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //

	/** Default Constructor 
	 * @param pResource Contains raw data and file location
	 */
	ISound(IResource* pResource);

	/** Default Destructor */
	virtual ~ISound();

	/** Native pointer retreival
	 * @return Memory address of concrete sound resource
	 */
	virtual void* GetNativeSound() = 0;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}