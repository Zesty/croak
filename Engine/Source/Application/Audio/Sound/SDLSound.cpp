#include "SDLSound.h"

#include <Utils/Log.h>

using crk::SDLSound;

SDLSound::SDLSound(IResource* pResource)
	: ISound(pResource)
	, m_pChunk(nullptr, nullptr)
{
	
}

SDLSound::~SDLSound()
{
	
}

void* crk::SDLSound::GetNativeSound()
{
	return m_pChunk.get();
}

bool SDLSound::Init(Mix_Chunk* pChunk)
{
	m_pChunk = std::unique_ptr<Mix_Chunk, decltype(&Mix_FreeChunk)>(pChunk, Mix_FreeChunk);

	if (!m_pChunk)
	{
		LOG(Error, "Failed to initialize SDLSound");
		return false;
	}

	return true;
}