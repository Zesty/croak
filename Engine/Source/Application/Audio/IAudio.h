#pragma once
/** \file IAudio.h
 * Base interface for Audio System 
 */
// Created by Billy Graban

#include <memory>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class IResource;
	class ISound;
	class IMusic;

/** \class IAudio
 * @brief Base interface for Audio System 
 */
class IAudio
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	IAudio();

	/** Default Destructor */
	virtual ~IAudio();

	/** Setup audio system
	 * @return True if successful
	 */
	virtual bool Init() = 0;

	/** Play (stream) a music resource
	 * @param pMusicResource Music to start playing
	 * @return True if successful
	 */
	virtual bool PlayMusic(IMusic* pMusicResource) = 0;

	/** Play a sound resource
	 * @param pSoundResource Sound to play immediately
	 * @return True if successful
	 */
	virtual bool PlaySound(ISound* pSoundResource) = 0;

	/** Convert raw data in resource to a sound resource 
	 * @param pResource The resource to convert
	 * @return Shared pointer to the sound resource
	 */
	virtual std::shared_ptr<ISound> LoadSound(IResource* pResource) = 0;

	/** Convert raw data in resource to a music resource
	 * @param pResource The resource to convert
	 * @return Shared pointer to the music resource
	 */
	virtual std::shared_ptr<IMusic> LoadMusic(IResource* pResource) = 0;

	/** Create the concrete audio system to be used
	 * This can be changed through Preprocessor defines per configuration
	 * @return Unique pointer to the audio system
	 */
	static std::unique_ptr<IAudio> Create();

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}