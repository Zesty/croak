#pragma once
/** \file SDLAudio.h
 * Contains an SDL_chunk  
 */
// Created by Billy Graban

#include ".\IAudio.h"

#include <unordered_map>
#include <string>

#include <SDL/SDL_mixer.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class SDLAudio
 * @brief Contains an SDL_chunk 
 */
class SDLAudio
	: public IAudio
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	SDLAudio();

	/** Default Destructor */
	~SDLAudio();

	/** Setup audio system
	 * Uses default frequency, channels, and format.  Accepts MP3 and OGG.  Listen
	 * for sound events
	 * @return True if successful
	 */
	bool Init() override;

	/** Play (stream) a music resource
	 * @param pMusicResource Music to start playing
	 * @return True if successful
	 */
	bool PlayMusic(IMusic* pMusicResource) override;

	/** Play a sound resource
	 * @param pSoundResource Sound to play immediately
	 * @return True if successful
	 */
	bool PlaySound(ISound* pSoundResource) override;

	/** Convert raw data in resource to a sound resource
	 * @param pResource The resource to convert
	 * @return Shared pointer to the sound resource
	 */
	std::shared_ptr<ISound> LoadSound(IResource* pResource) override;

	/** Convert raw data in resource to a music resource
	 * @param pResource The resource to convert
	 * @return Shared pointer to the music resource
	 */
	std::shared_ptr<IMusic> LoadMusic(IResource* pResource) override;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** Has this module been initialized successfully? */
	bool m_initialized;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}