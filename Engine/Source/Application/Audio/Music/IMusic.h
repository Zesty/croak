#pragma once
/** \file IMusic.h
 * Music Resource Interface 
 */
// Created by Billy Graban

#include <string>
#include <vector>
#include <cstddef>

#include <Logic/Resources/IResource.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class IMusic
 * @brief Music Resource Interface 
 */
class IMusic
	: public IResource
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor
	 *
	 * @param pResource Contains raw data and file location
	 */
	IMusic(IResource* pResource);

	/** Default Destructor */
	virtual ~IMusic();

	/** Native pointer retreival
	 *
	 * @return Memory address of concrete music resource 
	 */
	virtual void* GetNativeMusic() = 0;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}