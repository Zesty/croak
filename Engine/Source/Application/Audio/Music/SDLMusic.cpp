#include "SDLMusic.h"

using crk::SDLMusic;

#include <Utils/Log.h>

SDLMusic::SDLMusic(IResource* pResource)
	: IMusic(pResource)
	, m_pMusic(nullptr, nullptr)
{
	
}

SDLMusic::~SDLMusic()
{
	
}

void* crk::SDLMusic::GetNativeMusic()
{
	return m_pMusic.get();
}

bool crk::SDLMusic::Init(Mix_Music* pMusic)
{
	m_pMusic = std::unique_ptr<Mix_Music, decltype(&Mix_FreeMusic)>(pMusic, Mix_FreeMusic);
	if (!m_pMusic)
	{
		LOG(Error, "Failed to initialize SDLMusic: %s", Mix_GetError());
		return false;
	}

	return true;
}
