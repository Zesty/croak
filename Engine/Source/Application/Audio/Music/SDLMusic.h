#pragma once
/** \file SDLMusic.h
 * Resource holding SDL_Music data 
 */
// Created by Billy Graban

#include ".\IMusic.h"

#include <memory>

#include <SDL/SDL_mixer.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class IResource; /**< Foward declare IResource */

/** \class SDLMusic
 * @brief Resource holding SDL_Music data 
 */
class SDLMusic
	: public IMusic
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor 
	 * @param pResource Contains raw data and file location
	 */
	SDLMusic(IResource* pResource);

	/** Default Destructor */
	~SDLMusic();

	/** Native pointer retreival
	 * @return Memory address of SDL_mixer resource
	 */
	void* GetNativeMusic() override;
	
	/** Create unique pointer that holds the Mix_Music object
	 * @param pMusic SDL_mixer music object
	 * @return True if successful
	 */
	bool Init(Mix_Music* pMusic);

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** Unique pointer to SDL_mixer music object */
	std::unique_ptr<Mix_Music, decltype(&Mix_FreeMusic)> m_pMusic;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}