#pragma once
/** \file IMouse.h
 * Store frame and previous frame mouse inputs 
 */
// Created by Billy Graban

#include <array>

#include <Utils/CommonMath.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class IMouse
 * @brief Store frame and previous frame mouse inputs
 */
class IMouse
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //
	/** \enum Button All buttons stored by the mouse */
	enum Button
	{
		kButtonMouseLeft,	/**< Left mouse button */
		kButtonMouseRight,	/**< Right mouse button */

		kButtonMax			/**< Maximum count of buttons handled by this object */
	};

	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	IMouse();

	/** Default Destructor */
	~IMouse();

	/** Initialize the default mouse state
	 * @return True if successful
	 */
	virtual bool Init();

	/** Store the state of a button
	 * @param button The button whose state has changed
	 * @param isDown True if the button is down
	 */
	virtual void SetButtonState(Button button, bool isDown);

	/** Check if the button was just pressed this frame
	 * @param button The button to check
	 * @return True if the button was just pressed this frame
	 */
	virtual bool ButtonPressed(Button button);

	/** Check if the button was just released this frame
	 * @param button The button to check
	 * @return True if the button was just released this frame
	 */
	virtual bool ButtonReleased(Button button);

	/** Check if the button is down this frame
	 * @param button The button to check
	 * @return True if the button is down
	 */
	virtual bool ButtonIsDown(Button button);

	/** Sets the internal mouse position
	 * @param x The x position of the mouse (in pixel space)
	 * @param y The y position of the mouse (in pixel space)
	 */
	virtual void SetMousePosition(int x, int y);

	/** Copy mouse state into the array that stores the state of the previous frame */
	virtual void NextFrame();

	/** Create the concrete mouse system to be used
	 * @return Unique pointer to the mouse system
	 */
	static std::unique_ptr<IMouse> Create();

protected:
	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	
	/** Bit-array of buttons and their states for this frame */
	std::array<bool, kButtonMax> m_buttonState;

	/** Bit-array of buttons and their states for the previous frame */
	std::array<bool, kButtonMax> m_previousButtonState;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** Position of the mouse on screen (in pixel space) relative to the top-left corner of the window */
	IVec2 m_position;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	IVec2 GetMousePosition() { return m_position; } /**< Return the current mouse position */

};
}