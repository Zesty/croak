#include "IKeyboard.h"

#include <Utils/Log.h>
#include <Logic/Scripting/LuaManager.h>

using crk::IKeyboard;

IKeyboard::IKeyboard()
	: m_keyState({ false })
	, m_previousKeyState({ false })
{
	
}

IKeyboard::~IKeyboard()
{
	
}

bool crk::IKeyboard::Init()
{
	m_keyState = { false };

	// Set up the lua keycode map
	std::string str;
	for (char ch = 'a'; ch <= 'z'; ++ch)
	{
		str = ch;
		m_luaKeycodes.emplace(str, Code((int)Code::kA + (ch - 'a')));
	}

	return true;
}

void crk::IKeyboard::SetKeyState(IKeyboard::Code code, bool isDown)
{
	if (code != Code::kCodeMax)
	{
		m_keyState[(int)code] = isDown;
	}
}

bool crk::IKeyboard::KeyPressed(IKeyboard::Code code)
{
	return (!m_previousKeyState[(int)code] && m_keyState[(int)code]);
}

bool crk::IKeyboard::KeyReleased(IKeyboard::Code code)
{
	return (m_previousKeyState[(int)code] && !m_keyState[(int)code]);
}

bool crk::IKeyboard::KeyIsDown(IKeyboard::Code code)
{
	return (m_keyState[(int)code]);
}

void crk::IKeyboard::NextFrame()
{
	m_previousKeyState = m_keyState;
}

bool crk::IKeyboard::RegisterWithLua(LuaManager* pLua)
{
	pLua->SetGlobalUserData(this, "Keyboard");
	pLua->RegisterFunction("keyIsDown", Lua::KeyIsDown);

	return true;
}

crk::IKeyboard::Code crk::IKeyboard::GetLuaKeycode(std::string string)
{
	auto mapItr = m_luaKeycodes.find(string);
	if (mapItr == m_luaKeycodes.end())
	{
		LOG(Warning, "Lua KeyCode Table could not find entry: %s", string.c_str());
		return Code::kCodeMax;
	}

	return mapItr->second;
}

std::unique_ptr<IKeyboard> crk::IKeyboard::Create()
{
	return std::make_unique<IKeyboard>();
}

int Lua::KeyIsDown(lua_State* L)
{
	crk::IKeyboard* pKeyboard = static_cast<crk::IKeyboard*>(lua_touserdata(L, -2));
	const char* string = lua_tostring(L, -1);

	crk::IKeyboard::Code code = pKeyboard->GetLuaKeycode(string);
	bool isDown = pKeyboard->KeyIsDown(code);

	lua_pushboolean(L, isDown);
	return 1;
}
