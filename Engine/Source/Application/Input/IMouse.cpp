#include "IMouse.h"

using crk::IMouse;

IMouse::IMouse()
	: m_buttonState({ false })
	, m_previousButtonState({ false })
{
	
}

IMouse::~IMouse()
{
	
}

bool crk::IMouse::Init()
{
	m_buttonState = { false };
	return true;
}

void crk::IMouse::SetButtonState(Button button, bool isDown)
{
	if (button != kButtonMax)
	{
		m_buttonState[button] = isDown;
	}
}

bool crk::IMouse::ButtonPressed(Button button)
{
	return (!m_previousButtonState[button] && m_buttonState[button]);
}

bool crk::IMouse::ButtonReleased(Button button)
{
	return (m_previousButtonState[button] && !m_buttonState[button]);
}

bool crk::IMouse::ButtonIsDown(Button button)
{
	return m_buttonState[button];
}

void crk::IMouse::SetMousePosition(int x, int y)
{
	m_position.x = x;
	m_position.y = y;
}

void crk::IMouse::NextFrame()
{
	m_previousButtonState = m_buttonState;
}

std::unique_ptr<IMouse> crk::IMouse::Create()
{
	return std::make_unique<IMouse>();
}
