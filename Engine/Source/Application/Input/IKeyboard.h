#pragma once
/** \file IKeyboard.h
 * Store frame and previous frame keyboard inputs
 */
// Created by Billy Graban

#include <array>
#include <unordered_map>

struct lua_State;

//! \namespace crk Contains all Croak Engine code
namespace crk
{
	class LuaManager;

/** \class IKeyboard
 * @brief Store frame and previous frame keyboard inputs 
 */
class IKeyboard
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //

	/** \enum Code All key codes stored by the keyboard */
	enum class Code
	{
		kA,				/**< The 'A' key */
		kB,				/**< The 'B' key */
		kC,				/**< The 'C' key */
		kD,				/**< The 'D' key */
		kE,				/**< The 'E' key */
		kF,				/**< The 'F' key */
		kG,				/**< The 'G' key */
		kH,				/**< The 'H' key */
		kI,				/**< The 'I' key */
		kJ,				/**< The 'J' key */
		kK,				/**< The 'K' key */
		kL,				/**< The 'L' key */
		kM,				/**< The 'M' key */
		kN,				/**< The 'N' key */
		kO,				/**< The 'O' key */
		kP,				/**< The 'P' key */
		kQ,				/**< The 'Q' key */
		kR,				/**< The 'R' key */
		kS,				/**< The 'S' key */
		kT,				/**< The 'T' key */
		kU,				/**< The 'U' key */
		kV,				/**< The 'V' key */
		kW,				/**< The 'W' key */
		kX,				/**< The 'X' key */
		kY,				/**< The 'Y' key */
		kZ,				/**< The 'Z' key */

		k1,				/**< The '1' key */
		k2,				/**< The '2' key */
		k3,				/**< The '3' key */
		k4,				/**< The '4' key */
		k5,				/**< The '5' key */
		k6,				/**< The '6' key */
		k7,				/**< The '7' key */
		k8,				/**< The '8' key */
		k9,				/**< The '9' key */
		k0,				/**< The '0' key */

		kSpace,			/**< The 'Space' key */
		kArrowRight,	/**< The 'Right Arrow' key */
		kArrowLeft,		/**< The 'Left Arrow' key */
		kArrowUp,		/**< The 'Up Arrow' key */
		kArrowDown,		/**< The 'Down Arrow' key */
		kTilde,			/**< The 'Tilde' key */

		kCodeMax		/**< Maximum count of keys handled by this object */
	};

	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	IKeyboard();

	/** Default Destructor */
	virtual ~IKeyboard();

	/** Initialize the default keyboard state and lua keycode map 
	 * @return True if successful
	 */
	virtual bool Init();

	/** Store the state of a key 
	 * @param code The keycode whose state has changed
	 * @param isDown True if the key is down
	 */
	virtual void SetKeyState(Code code, bool isDown);

	/** Check if the key was just pressed this frame
	 * @param code The keycode to check
	 * @return True if the key was just pressed this frame
	 */
	virtual bool KeyPressed(Code code);

	/** Check if the key was just released this frame
	 * @param code The keycode to check
	 * @return True if the key was just released this frame
	 */
	virtual bool KeyReleased(Code code);

	/** Check if the key is down this frame
	 * @param code The keycode to check
	 * @return True if the key is down
	 */
	virtual bool KeyIsDown(Code code);

	/** Copy keyboard state into the array that stores the state of the previous frame */
	virtual void NextFrame();

	/** Register all member functions with Lua:: namespaced functions 
	 * @param pLua The Lua manager to register the functions with
	 * @return True if successful
	 */
	virtual bool RegisterWithLua(LuaManager* pLua);

	/** Check keycode map for entries
	 * @param string The one letter string to check the map for
	 * @return The code associated with that letter, or kCodeMax if it cannot be found
	 */
	virtual Code GetLuaKeycode(std::string string);

	/** Create the concrete keyboard system to be used
	 * @return Unique pointer to the keyboard system
	 */
	static std::unique_ptr<IKeyboard> Create();

protected:
	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //

	/** Bit-array of keys and their states for this frame */
	std::array<bool, (size_t)Code::kCodeMax> m_keyState;

	/** Bit-array of keys and their states for the previous frame */
	std::array<bool, (size_t)Code::kCodeMax> m_previousKeyState;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	/** All strings (one letter strings) associated with key codes for use with Lua */
	std::unordered_map<std::string, Code> m_luaKeycodes;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};
}

/** \namespace Lua Contains all global C functions that connect the Croak Engine to Lua */
namespace Lua
{
	/** Lua binding for IKeyboard::KeyIsDown
	 * @param L The lua state
	 * @return The number of arguments pushed onto the lua stack 
	 * @see crk::IKeyboard::KeyIsDown()
	 */
	int KeyIsDown(lua_State* L);
}