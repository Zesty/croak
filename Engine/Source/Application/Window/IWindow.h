#pragma once
/** \file IWindow.h
 * Interface for window object 
 */
// Created by Billy Graban

#include <Utils/Typedefs.h>
#include <Application/Input/IKeyboard.h>
#include <Application/Input/IMouse.h>

//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class IWindow
 * @brief Interface for window object 
 */
class IWindow
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	IWindow();

	/** Default Destructor */
	virtual ~IWindow();

	/** Initialize a window with chosen properties 
	 * @param title The title of the window
	 * @param width The width of the window (in pixels)
	 * @param height The height of the window (in pixels)
	 * @return True if successful
	 */
	virtual bool Init(const char* title, u32 width, u32 height) = 0;

	/** Handle all events queued by the operating system
	 * @return True if the game should continue to the next frame
	 */
	virtual bool ProcessEvents() = 0;

	/** Native pointer retreival
	 * @return Memory address of concrete window object
	 */
	virtual void* GetNativeWindow() const = 0;

	/** Step the keyboard and mouse objects forward to the next frame */
	virtual void NextFrame();

	/** Convert a keycode supplied by the operating system to our system */
	virtual IKeyboard::Code ConvertToCode(u32 code) = 0;

	/** Convert a mouse button supplied by the operating system to our system */
	virtual IMouse::Button ConvertToButton(u32 button) = 0;

protected:
	// --------------------------------------------------------------------- //
	// Protected Member Variables
	// --------------------------------------------------------------------- //

	/** Keyboard owned by the window */
	std::unique_ptr<IKeyboard> m_pKeyboard;

	/** Mouse owned by the window */
	std::unique_ptr<IMouse> m_pMouse;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //

	// These may need to virtualized if child classes wish to deal with them differently

	/** Attach keyboard to window */
	void AttachKeyboard(std::unique_ptr<IKeyboard> pKeyboard) { m_pKeyboard = std::move(pKeyboard); }
	
	/** Get the keyboard associated with this window */
	IKeyboard* GetKeyboard() { return m_pKeyboard.get(); }

	/** Attach mouse to window */
	void AttachMouse(std::unique_ptr<IMouse> pMouse) { m_pMouse = std::move(pMouse); }
	
	/** Get the mouse associated with this window */
	IMouse* GetMouse() { return m_pMouse.get(); }

};
}