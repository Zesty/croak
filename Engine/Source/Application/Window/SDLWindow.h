#pragma once
/** \file SDLWindow.h
 * Holds the SDL_Window object 
 */
// Created by Billy Graban

#include ".\IWindow.h"

#include <memory>

#include <SDL/SDL.h>


//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class SDLWindow
 * @brief Holds the SDL_Window object 
 */
class SDLWindow
	: public IWindow
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	SDLWindow();

	/** Default Destructor */
	~SDLWindow();

	// Inherited via IWindow

	/** Initialize a window with chosen properties
	 * V-Sync, Hardware Rendering, and Targeting Rendering are enabled
	 * @param title The title of the window
	 * @param width The width of the window (in pixels)
	 * @param height The height of the window (in pixels)
	 * @return True if successful
	 */
	virtual bool Init(const char* title, u32 width, u32 height) override;

	/** Handle all events queued by the operating system
	 * @return True if the game should continue to the next frame
	 */
	virtual bool ProcessEvents() override;

	/** Native pointer retreival
	 * @return Memory address of SDL_Window
	 */
	virtual void* GetNativeWindow() const override;

	/** Convert a keycode supplied by the operating system to our system */
	virtual IKeyboard::Code ConvertToCode(u32 code) override;

	/** Convert a mouse button supplied by the operating system to our system */
	virtual IMouse::Button ConvertToButton(u32 button) override;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //

	/** True if the SDL_Window initialized successfully */
	bool m_initialized;

	/** Unique pointer to the SDL_Window itself */
	std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)> m_pSDLWindow;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //

};
}