#include "IWindow.h"

using crk::IWindow;

IWindow::IWindow()
{
	
}

IWindow::~IWindow()
{
	
}

void crk::IWindow::NextFrame()
{
	m_pKeyboard->NextFrame();
	m_pMouse->NextFrame();
}
