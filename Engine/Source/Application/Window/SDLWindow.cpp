#include "SDLWindow.h"

#include <Utils/Log.h>

using crk::SDLWindow;

SDLWindow::SDLWindow()
	: m_initialized(false)
	, m_pSDLWindow(nullptr, nullptr)
{
	
}

SDLWindow::~SDLWindow()
{
	// Clean up the memory
	m_pSDLWindow = nullptr;
	if (m_initialized)
	{
		SDL_QuitSubSystem(SDL_INIT_VIDEO);
	}
	SDL_Quit();
}

bool crk::SDLWindow::Init(const char* title, u32 width, u32 height)
{
	if (SDL_InitSubSystem(SDL_INIT_VIDEO))
	{
		// TODO: Log error
		return false;
	}

	m_pSDLWindow = std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)>(
		SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_SHOWN),
		&SDL_DestroyWindow
		);

	if (!m_pSDLWindow)
	{
		// TODO: Log Error
		return false;
	}

	m_initialized = true;
	return true;
}

bool crk::SDLWindow::ProcessEvents()
{
	SDL_Event event;
	if (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT ||
			(event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE))
		{
			return false;
		}
		else if (m_pKeyboard && (event.type == SDL_KEYDOWN || event.type == SDL_KEYUP))
		{
			IKeyboard::Code code = ConvertToCode(event.key.keysym.scancode);
			m_pKeyboard->SetKeyState(code, event.type == SDL_KEYDOWN);
		}
		else if (m_pMouse &&
			(event.type == SDL_MOUSEBUTTONDOWN ||
				event.type == SDL_MOUSEBUTTONUP))
		{
			IMouse::Button button = ConvertToButton(event.button.button);
			m_pMouse->SetButtonState(button, event.type == SDL_MOUSEBUTTONDOWN);
		}
		else if (m_pMouse && event.type == SDL_MOUSEMOTION)
		{
			m_pMouse->SetMousePosition(event.button.x, event.button.y);
		}
	}

	// Keep processing events next frame
	return true;
}

void* crk::SDLWindow::GetNativeWindow() const
{
	return m_pSDLWindow.get();
}

crk::IKeyboard::Code crk::SDLWindow::ConvertToCode(u32 code)
{
	if (code >= SDL_SCANCODE_A && code <= SDL_SCANCODE_Z)
	{
		return static_cast<IKeyboard::Code>(code - SDL_SCANCODE_A + (int)IKeyboard::Code::kA);
	}
	else if (code >= SDL_SCANCODE_1 && code <= SDL_SCANCODE_0)
	{
		return static_cast<IKeyboard::Code>(code - SDL_SCANCODE_1 + (int)IKeyboard::Code::k1);
	}
	else if (code == SDL_SCANCODE_SPACE)
	{
		return IKeyboard::Code::kSpace;
	}
	else if (code == SDL_SCANCODE_UP)
	{
		return IKeyboard::Code::kArrowUp;
	}
	else if (code == SDL_SCANCODE_DOWN)
	{
		return IKeyboard::Code::kArrowDown;
	}
	else if (code == SDL_SCANCODE_RIGHT)
	{
		return IKeyboard::Code::kArrowRight;
	}
	else if (code == SDL_SCANCODE_LEFT)
	{
		return IKeyboard::Code::kArrowLeft;
	}
	else if (code == SDL_SCANCODE_GRAVE)
	{
		return IKeyboard::Code::kTilde;
	}

	LOG(Warning, "Window failed to interpret key press with SDL_SCANCODE: %d", code);
	return IKeyboard::Code::kCodeMax;
}

crk::IMouse::Button crk::SDLWindow::ConvertToButton(u32 button)
{
	if (button == SDL_BUTTON_LEFT)
	{
		return IMouse::kButtonMouseLeft;
	}
	else if (button == SDL_BUTTON_RIGHT)
	{
		return IMouse::kButtonMouseRight;
	}

	LOG(Warning, "Window failed to interpret mouse button press with value: %d", button);
	return IMouse::kButtonMax;
}
