#pragma once
/** \file ApplicationLayer.h
 * Core module for communicating between the operating system and the game layer 
 */
// Created by Billy Graban

#include <memory>

#include <Application/OS/IOpSys.h>
#include <Application/Window/IWindow.h>
#include <Application/Graphics/IGraphics.h>
#include <Application/Graphics/Fonts/IFontLoader.h>
#include <Application/Audio/IAudio.h>
#include <Logic/IGameLayer.h>
#include <Logic/Physics/IPhysicsSimulation.h>


//! \namespace crk Contains all Croak Engine code
namespace crk
{
/** \class ApplicationLayer
 * @brief Core module for communicating between the operating system and the game layer 
 */
class ApplicationLayer
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //

	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	ApplicationLayer();

	/** Default Destructor */
	~ApplicationLayer();

	/** Initializes the application
	 * @return True if all modules initialized successfully
	 */
	bool Init();

	/** Run timing code, update the game layer, and exit when necessary */
	void Run();

	/** Release all resources used by the application */
	void Cleanup();

	/** Create a game layer to be used by the application */
	virtual std::unique_ptr<IGameLayer> CreateGameLayer() = 0;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	std::unique_ptr<IOpSys> m_pSystem;					/**< Owned operating system module */
	std::unique_ptr<IGameLayer> m_pGameLayer;			/**< Owned game layer */
	std::unique_ptr<IWindow> m_pWindow;					/**< Owned window module */
	std::unique_ptr<IGraphics> m_pGraphics;				/**< Owned graphics module */
	std::unique_ptr<IAudio> m_pAudio;					/**< Owned audio module */
	std::unique_ptr<IPhysicsSimulation> m_pPhysics;		/**< Owned physics module */
	std::unique_ptr<IFontLoader> m_pFontLoader;			/**< Owned font loader module */
	
	/** Dimensions of the screen (in pixels) */
	IVec2 m_screenDimensions;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	IGraphics* GetGraphics() const { return m_pGraphics.get(); }			/**< Get the raw graphics pointer */
	IMouse* GetMouse() const { return m_pWindow->GetMouse(); }				/**< Get the raw mouse pointer */
	IKeyboard* GetKeyboard() const { return m_pWindow->GetKeyboard(); }		/**< Get the raw keyboard pointer */
	IAudio* GetAudio() const { return m_pAudio.get(); }						/**< Get the raw audio pointer */
	IGameLayer* GetGameLayer() const { return m_pGameLayer.get(); }			/**< Get the raw game layer pointer */
	IPhysicsSimulation* GetPhysics() const { return m_pPhysics.get(); }		/**< Get the raw physics pointer */
	IFontLoader* GetFontLoader() const { return m_pFontLoader.get(); }		/**< Get the raw font loader pointer */

	IVec2 GetScreenDimensions() const { return m_screenDimensions; }		/**< Get the screen dimensions (in pixels) */
};
}