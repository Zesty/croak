#pragma once
/** \file GameApp.h */
/** TODO: File Purpose */
// Created by Billy Graban

#include <Application/ApplicationLayer.h>


/** \class GameApp */
/** TODO: Class Purpose */
class GameApp : public crk::ApplicationLayer
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	GameApp();

	/** Default Destructor */
	~GameApp();

	std::unique_ptr<crk::IGameLayer> CreateGameLayer() override;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};