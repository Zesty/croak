#include "GameApp.h"

#include <Logic/GameLogic.h>

GameApp::GameApp()
{
	
}

GameApp::~GameApp()
{
	
}

std::unique_ptr<crk::IGameLayer> GameApp::CreateGameLayer()
{
	return std::make_unique<GameLogic>();
}
