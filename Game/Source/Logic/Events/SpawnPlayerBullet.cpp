#include "SpawnPlayerBullet.h"

#include <Lua/lua.hpp>

SpawnPlayerBullet::SpawnPlayerBullet(crk::FVec2 position, std::string xmlFilepath)
	: m_position(position)
	, m_xmlFilepath(xmlFilepath)
{
	
}

SpawnPlayerBullet::~SpawnPlayerBullet()
{
	
}

bool SpawnPlayerBullet::PushLuaTable(lua_State* L) const
{
	lua_newtable(L);

	// Fill out fields
	lua_pushnumber(L, m_position.x);
	lua_setfield(L, -2, "x");

	lua_pushnumber(L, m_position.y);
	lua_setfield(L, -2, "y");

	lua_pushstring(L, m_xmlFilepath.c_str());
	lua_setfield(L, -2, "filepath");

	lua_pushstring(L, GetName());
	lua_setfield(L, -2, "name");

	return true;
}
