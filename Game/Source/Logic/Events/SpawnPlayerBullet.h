#pragma once
/** \file SpawnPlayerBullet.h */
/** TODO: File Purpose */
// Created by Billy Graban

#include <string>

#include <Logic/Event/IEvent.h>
#include <Utils/CommonMath.h>

/** \class SpawnPlayerBullet */
/** TODO: Class Purpose */
class SpawnPlayerBullet
	: public crk::IEvent
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //
	// {7DC70DD5-063D-46C5-A0C9-1E5357420BD2}
	const static EventId kEventId = 0x7DC70DD5;

	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	SpawnPlayerBullet(crk::FVec2 position, std::string xmlFilepath);

	/** Default Destructor */
	~SpawnPlayerBullet();

	EventId GetEventId() const override { return kEventId; }
	const char* GetName() const override { return "SpawnPlayerBullet"; }
	bool PushLuaTable(lua_State* L) const override;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	crk::FVec2 m_position;
	std::string m_xmlFilepath;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	crk::FVec2 GetPosition() const { return m_position; }
	const std::string& GetXmlFilepath() const { return m_xmlFilepath; }

};