#pragma once
/** \file EnemyDeathEvent.h */
/** TODO: File Purpose */
// Created by Billy Graban

#include <Logic/Event/IEvent.h>

namespace crk
{
	class Actor;
}

/** \class EnemyDeathEvent */
/** TODO: Class Purpose */
class EnemyDeathEvent
	: public crk::IEvent
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //
	// {4E6FB01A-FB44-46A7-8E18-C8AE07C69BFC}
	const static EventId kEventId = 0x4E6FB01A;

	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	EnemyDeathEvent(crk::Actor* pEnemy);

	/** Default Destructor */
	~EnemyDeathEvent();

	EventId GetEventId() const override { return kEventId; }
	const char* GetName() const override { return "EnemyDeathEvent"; }
	bool PushLuaTable(lua_State* L) const override;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	crk::Actor* m_pEnemy;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	crk::Actor* GetEnemy() const { return m_pEnemy; }

};