#include "EnemyDeathEvent.h"

#include <Lua/lua.hpp>

EnemyDeathEvent::EnemyDeathEvent(crk::Actor* pEnemy)
	: m_pEnemy(pEnemy)
{
	
}

EnemyDeathEvent::~EnemyDeathEvent()
{
	
}

bool EnemyDeathEvent::PushLuaTable(lua_State* L) const
{
	// Create table
	lua_newtable(L);

	// Add fields
 	lua_pushlightuserdata(L, m_pEnemy);
	lua_setfield(L, -2, "enemy");

	lua_pushstring(L, GetName());
	lua_setfield(L, -2, "name");

	return true;
}
