#pragma once
/** \file GameLogic.h */
/** TODO: File Purpose */
// Created by Billy Graban
#include <Logic/IGameLayer.h>


class GameContactListener;

/** \class GameLogic */
/** TODO: Class Purpose */
class GameLogic : public crk::IGameLayer
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	GameLogic();

	/** Default Destructor */
	~GameLogic();

	const char* GetGameName() const override { return "Project - Croak"; }
	bool Init(crk::ApplicationLayer* pApp) override;
	void SpawnParticles(const char* filepath, crk::FVec2 position, u32 count);
	void Cleanup() override;
	virtual crk::IView* CreateView(const std::string& type) override;
private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	GameContactListener* m_pContactListener;
	u16 m_playerScore;


	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //
	void CreateActorEventHandler(crk::IEvent* pEvent);
	void SpawnBullet(crk::IEvent* pEvent);

public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //


};