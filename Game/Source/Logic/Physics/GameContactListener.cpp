#include "GameContactListener.h"

#include <Utils/Log.h>
#include <Logic/Actor/Actor.h>

GameContactListener::GameContactListener()
{
	
}

GameContactListener::~GameContactListener()
{
	
}

void GameContactListener::BeginContact(b2Contact* pContact)
{
	HandleContact(pContact, true);
}

void GameContactListener::EndContact(b2Contact* pContact)
{
	HandleContact(pContact, false);
}

void GameContactListener::HandleContact(b2Contact* pContact, bool isBegin)
{
	b2Fixture* pFixtureA = pContact->GetFixtureA();
	b2Fixture* pFixtureB = pContact->GetFixtureB();

	b2Body* pBodyA = pFixtureA->GetBody();
	b2Body* pBodyB = pFixtureB->GetBody();

	crk::Actor* pActorA = static_cast<crk::Actor*>(pBodyA->GetUserData());
	crk::Actor* pActorB = static_cast<crk::Actor*>(pBodyB->GetUserData());

	// Some fixtures might not be connected to owning actors
	if (!pActorA || !pActorB)
		return;

	bool isOverlap;
	if (pFixtureA->IsSensor() || pFixtureB->IsSensor())
	{
		isOverlap = true;
	}
	else
	{
		isOverlap = false;
	}

	m_pGameLayer->NotifyContact(pActorA, pActorB, isOverlap, isBegin);
}
