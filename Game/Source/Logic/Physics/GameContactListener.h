#pragma once
/** \file GameContactListener.h */
/** TODO: File Purpose */
// Created by Billy Graban

#include <Logic/Physics/IContactListener.h>
#include <Logic/GameLogic.h>

/** \class GameContactListener */
/** TODO: Class Purpose */
class GameContactListener
	: public crk::IContactListener
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	GameContactListener();

	/** Default Destructor */
	~GameContactListener();


	void BeginContact(b2Contact* pContact) override;
	void EndContact(b2Contact* pContact) override;
private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	GameLogic* m_pGameLayer;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //
	void HandleContact(b2Contact* pContact, bool isBegin);


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //
	void SetGameLayer(GameLogic* pGameLayer) { m_pGameLayer = pGameLayer; }

};