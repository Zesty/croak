#include "GameLogic.h"

#include <cassert>

#include <View/PlayerView.h>
#include <View/EnemyView.h>

#include <Utils/Log.h>
#include <Utils/CommonMath.h>
#include <Logic/Actor/Actor.h>
#include <Logic/Resources/ResourceCache.h>
#include <Logic/Resources/ResourceCache.h>
#include <Logic/Physics/GameContactListener.h>

// Event Handling
#include <Logic/Event/EventDispatcher.h>
#include <Logic/Event/Events/CreateActorEvent.h>
#include <Logic/Event/Events/DestroyActorEvent.h>
#include <Logic/Event/Events/PlaySoundEvent.h>
#include <Logic/Events/SpawnPlayerBullet.h>
#include <Logic/Events/EnemyDeathEvent.h>

// Components
#include <Logic/Components/TransformComponent.h>
#include <Logic/Components/Physics/Box2DPhysicsComponent.h>
#include <Logic/Components/Render/RenderComponent.h>

// Used for spawning particles (could be moved to a particle system later)
#include <Logic/Rendering/Sprite.h>

// Text component procs
#include <Logic/Process/Render/ColorLerpProcess.h>
#include <Logic/Process/Render/ScaleProcess.h>

#include <Application/ApplicationLayer.h>
#include <Application/Audio/Sound/ISound.h>

// TESTING: UI Actor with TextComponent
#include <Logic/Components/Render/TextComponent.h>

GameLogic::GameLogic()
	: m_playerScore(0)
	, m_pContactListener(nullptr)
{

}

GameLogic::~GameLogic()
{
}

bool GameLogic::Init(crk::ApplicationLayer* pApp)
{
	// Used to create and initialize views
	m_pApp = pApp;

	// Set event manager listener
	m_pEventDispatcher = crk::EventDispatcher::Get();

	// Event listeners
	m_pEventDispatcher->AddEventListener(crk::CreateActorEvent::kEventId,
		[this](crk::IEvent* pEvent)
		{
			CreateActorEventHandler(pEvent);
		});

	m_pEventDispatcher->AddEventListener(SpawnPlayerBullet::kEventId,
		[this](crk::IEvent* pEvent)
		{
			SpawnBullet(pEvent);
			auto pSound = crk::ResourceCache::Get()->AcquireResource<crk::ISound>("Assets/Audio/FrogJump.wav");
			crk::EventDispatcher::Get()->QueueEvent(
				std::make_unique<crk::PlaySoundEvent>(pSound.get()));
		});


	if (!crk::IGameLayer::Init(pApp))
	{
		LOG(Error, "Failed to initialize GameLayer");
		return false;
	}

	// Always connect contact listener after parent class init
	m_pContactListener = new GameContactListener();
	m_pContactListener->SetGameLayer(this);
	IGameLayer::SetContactListener(m_pContactListener);

	auto pScore = SpawnActor("Assets/Actors/UI/Score.xml");
	auto pTextComponent = pScore->GetComponent<crk::TextComponent>();
	pTextComponent->UpdateText("Score: 0");

	// HACK: I know the text component won't be destroyed, so I'm passing the raw pointer to the lambda
	m_pEventDispatcher->AddEventListener(EnemyDeathEvent::kEventId, [pTextComponent, this](crk::IEvent* pEvent)
		{
			assert(pEvent->GetEventId() == EnemyDeathEvent::kEventId);
			++m_playerScore;
			pTextComponent->UpdateText("Score: " + std::to_string(m_playerScore));

			auto pSprite = pTextComponent->GetOwner()->GetComponent<crk::RenderComponent>()->GetRenderable<crk::Sprite>();
			crk::FColor originalColor = pSprite->GetOriginalColor();
			crk::FColor targetColor(0.9f, 0.9f, 0.9f, 1.0f);
			f32 duration = 0.15f;
			auto pColorUpProc = std::make_shared<crk::ColorLerpProcess>(pTextComponent->GetOwner(), originalColor, targetColor, duration);
			auto pColorDownProc = std::make_shared<crk::ColorLerpProcess>(pTextComponent->GetOwner(), targetColor, originalColor, duration);

			pColorUpProc->AttachChild(pColorDownProc);
			m_processManager.AttachProcess(pColorUpProc);

			crk::FVec2 startScale(1.0f, 1.0f);
			crk::FVec2 targetScale(1.05f, 1.05f);
			auto pScaleUpProc = std::make_shared<crk::ScaleProcess>(pTextComponent->GetOwner(), startScale, targetScale, duration);
			auto pScaleDownProc = std::make_shared<crk::ScaleProcess>(pTextComponent->GetOwner(), targetScale, startScale, duration);

			pScaleUpProc->AttachChild(pScaleDownProc);
			m_processManager.AttachProcess(pScaleUpProc);

		});

	auto pPlayer = SpawnActor("Assets/Actors/Player.xml");

	auto pSatellite = SpawnActor("Assets/Actors/Satellite.xml");
	auto pChildTransform = pSatellite->GetComponent<crk::TransformComponent>();
	pChildTransform->SetParent(pPlayer->GetComponent<crk::TransformComponent>());

	// Run the main file
	luaL_dofile(m_luaManager.GetState(), "Assets/Scripts/First.lua");

	return true;
}

void GameLogic::SpawnParticles(const char* filepath, crk::FVec2 position, u32 count)
{
	for (u32 i = 0; i < count; ++i)
	{
		std::shared_ptr<crk::Actor> pParticle = SpawnActor(filepath);
		crk::TransformComponent* pTransform = pParticle->GetComponent<crk::TransformComponent>();
		pTransform->SetPosition((f32)position.x, (f32)position.y);
		float scale = m_logicRng.RollRandomFloatInRange(0.02f, 0.1f);
		pTransform->SetScale(scale, scale);

		crk::Sprite* pSprite = pParticle->GetComponent<crk::RenderComponent>()->GetRenderable<crk::Sprite>();

		f32 greyScale = m_logicRng.RollRandomFloatZeroToOne();
		crk::FColor tint{ greyScale, greyScale, greyScale, 1.f };
		tint.r = m_logicRng.RollRandomFloatInRange(0.4f, 1.0f);
		pSprite->SetTint(tint);
	}
}

void GameLogic::Cleanup()
{
	IGameLayer::Cleanup();
	
	// This must happen before IGameLayer cleans itself up or deleting fixtures will crash the execution on close
	delete m_pContactListener;
}

crk::IView* GameLogic::CreateView(const std::string& type)
{
	crk::IView* pView = nullptr;
	if (type == "Player")
	{
		pView = new PlayerView();
	}
	else if (type == "Enemy")
	{
		pView = new EnemyView();
	}
	else
	{
		LOG(Error, "Unkown view type: %s", type.c_str());
		return false;
	}

	if (!pView->Init(m_pApp))
	{
		LOG(Error, "Failed to initialize view of type: %s", type.c_str());
		delete pView;
		return false;
	}

	AddView(std::unique_ptr<crk::IView>(pView));

	return pView;
}

void GameLogic::CreateActorEventHandler(crk::IEvent* pEvent)
{
	assert(pEvent->GetEventId() == crk::CreateActorEvent::kEventId);

	// Cast the event to CreateActorEvent
	auto pCreateActor = static_cast<crk::CreateActorEvent*>(pEvent);

	// Create the actual actor
	auto pResource = crk::ResourceCache::Get()->AcquireResource(pCreateActor->GetXmlFilepath());
	auto pActor = m_actorFactory.CreateActor(pResource.get());

	if (!pActor)
		return;

	crk::IView* pView = nullptr;
	if (!pCreateActor->IsEnemy())
	{
		// Create a player view
		auto pPlayerView = new PlayerView();
		pPlayerView->SetControlledActor(pActor);
		pView = pPlayerView;
	}
	else
	{
		// Create an enemy view
		auto pEnemyView = new EnemyView();
		pEnemyView->SetControlledActor(pActor);
		pView = pEnemyView;
	}

	LOG(Todo, "Figure out how to remove m_pApp from IGameLayer");
	// Initialize the view
	if (!pView->Init(m_pApp))
	{
		LOG(Error, "Failed to initialize view");
		return;
	}

	// Add actor to list
	AddView(std::unique_ptr<crk::IView>(pView));
	m_actorsToAdd.emplace_back(pActor);
	//m_actors[pActor->GetId()] = pActor;
}

void GameLogic::SpawnBullet(crk::IEvent* pEvent)
{
	assert(pEvent->GetEventId() == SpawnPlayerBullet::kEventId);
	auto pSpawnEvent = static_cast<SpawnPlayerBullet*>(pEvent);

	auto pActor = SpawnActor(pSpawnEvent->GetXmlFilepath().c_str());
	auto pBody = pActor->GetComponent<crk::Box2DPhysicsComponent>();
	pBody->SetPosition(pSpawnEvent->GetPosition());

	pBody->SetBeginOverlap([this, pBody](crk::Actor* pOther)
		{
			if (pOther->GetTag() == "Enemy")
			{
				SpawnParticles("Assets/Actors/TestParticle.xml", pOther->GetComponent<crk::TransformComponent>()->GetPosition(), 30);
				pOther->Destroy();
				pBody->GetOwner()->Destroy();

				auto pDispatcher = crk::EventDispatcher::Get();
				pDispatcher->QueueEvent(std::make_unique<crk::PlaySoundEvent>(
					crk::ResourceCache::Get()->AcquireResource<crk::ISound>("Assets/Audio/Explosion.wav").get()));
				pDispatcher->QueueEvent(std::make_unique<EnemyDeathEvent>(pBody->GetOwner()));
			}
		});
}
