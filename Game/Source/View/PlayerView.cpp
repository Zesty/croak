#include "PlayerView.h"

#include <Utils/Log.h>
#include <Utils/CommonMath.h>
#include <Application/ApplicationLayer.h>
#include <Application/Audio/Sound/ISound.h>
#include <Application/Audio/Music/IMusic.h>
#include <Logic/Resources/ResourceCache.h>
#include <Logic/Components/Physics/Box2DPhysicsComponent.h>
#include <Logic/Components/TransformComponent.h>
#include <Logic/Events/SpawnPlayerBullet.h>
#include <Logic/GameLogic.h>
#include <Logic/Scripting/LuaManager.h>

// TESTING: Enemy object
#include <Logic/Event/Events/CreateActorEvent.h>

PlayerView::PlayerView()
	: m_pMouse(nullptr)
	, m_pKeyboard(nullptr)
	, m_pGraphics(nullptr)
	, m_pAudio(nullptr)
	, m_pGameLayer(nullptr)
	, m_drawDebug(false)
{
	
}

PlayerView::~PlayerView()
{
}

bool PlayerView::Init(crk::ApplicationLayer* pApp)
{
	m_pMouse = pApp->GetMouse();
	m_pKeyboard = pApp->GetKeyboard();
	m_pGraphics = pApp->GetGraphics();
	m_pAudio = pApp->GetAudio();
	m_pGameLayer = pApp->GetGameLayer();

	LOG(Todo, "Check for errors in PlayerView::Init()");

	return true;
}

void PlayerView::UpdateInput()
{
	using crk::IKeyboard;
	using crk::IMouse;

#ifndef _ENGINE_SHIPPING
	if (m_pKeyboard->KeyPressed(IKeyboard::Code::kTilde))
	{
		m_drawDebug = !m_drawDebug;
		m_pGameLayer->SetDrawFPS(m_drawDebug);
	}
#endif

	if (m_pMouse->ButtonPressed(IMouse::kButtonMouseLeft))
	{
		crk::IVec2 pos = m_pMouse->GetMousePosition();

		auto pTransform = m_pActor->GetComponent<crk::TransformComponent>();
		auto playerPos = pTransform->GetPosition();
		//LOG(Info, "Spawning laser at: (%.3f, %.3f)", playerPos.x, playerPos.y);

		crk::EventDispatcher* pDispatcher = crk::EventDispatcher::Get();
		pDispatcher->TriggerEvent(std::make_unique<SpawnPlayerBullet>(playerPos, "Assets/Actors/PlayerLaser.xml"));
	}

	LOG(Todo, "PlayMusicEvent");
}

void PlayerView::ViewScene()
{
	m_pGraphics->StartDrawing(32, 32, 32);

	// Draw grid lines first if in debug draw
	if (m_drawDebug)
	{
		LOG(Todo, "Draw grid lines based on PPM and window width/height");
		crk::FVec2 start(0, 0);
		crk::FVec2 end(0, 5);
		crk::FColor color(0, 0.7f, 0, 0.7f);
		for (int y = 0; y < 6; ++y)
		{
			start.x = (f32)y;
			end.x = (f32)y;
			m_pGraphics->DrawLine(start, end, color);
		}

		start.x = 0;
		end.x = 6;
		for (int x = 0; x < 5; ++x)
		{
			start.y = (f32)x;
			end.y = (f32)x;
			m_pGraphics->DrawLine(start, end, color);
		}
	}

	auto actors = m_pGameLayer->GetActorsOnScreen();
	//LOG(Info, "Actors on screen: %d", actors.size());
	for (auto& actor : actors)
	{
		actor->Render(m_pGraphics);
	}

	if (m_drawDebug)
	{
		LOG(Todo, "Don't draw all collisions all the time");
		auto* pPhysics = m_pGameLayer->GetPhysicsSim();
		pPhysics->DrawDebug();
	}

	m_pGraphics->EndDrawing();
}