#pragma once
/** \file PlayerView.h */
/** TODO: File Purpose */
// Created by Billy Graban

#include <View/IView.h>

#include <memory>

// TESTING: Tiled renderer
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <Application/Graphics/SDLRenderer.h>

namespace crk
{
	class ApplicationLayer;
	class IAudio;
	class IGraphics;
	class IKeyboard;
	class IMouse;
	class IGameLayer;

	class IFont;
}

/** \class PlayerView */
/** TODO: Class Purpose */
class PlayerView
	: public crk::IView
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	PlayerView();

	/** Default Destructor */
	~PlayerView();

	// Inherited via IView
	virtual bool Init(crk::ApplicationLayer* pApp) override;
	virtual void UpdateInput() override;
	virtual void ViewScene() override;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //
	crk::IAudio*		m_pAudio;
	crk::IKeyboard*		m_pKeyboard;
	crk::IMouse*		m_pMouse;
	crk::IGraphics*		m_pGraphics;
	crk::IGameLayer*	m_pGameLayer;

	bool m_drawDebug;
	crk::IFont* m_pFpsFont;
	u32 m_lastFps;

	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //
	void TEST_LoadTiledMap();
	void TEST_DrawTiledMap();

	SDL_Texture* m_pTilesetTexture;
	crk::SDLRenderer* m_pTestRenderer;

public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //





};