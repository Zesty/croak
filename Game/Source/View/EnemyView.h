#pragma once
/** \file EnemyView.h */
/** TODO: File Purpose */
// Created by Billy Graban

#include <View/IView.h>

/** \class EnemyView */
/** TODO: Class Purpose */
class EnemyView
	: public crk::IView
{
public:
	// --------------------------------------------------------------------- //
	// Public Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Public Member Functions
	// --------------------------------------------------------------------- //
	/** Default Constructor */
	EnemyView();

	/** Default Destructor */
	~EnemyView();

	// Inherited via IView
	virtual bool Init(crk::ApplicationLayer* pApp) override;

	virtual void UpdateInput() override;

	virtual void ViewScene() override;

private:
	// --------------------------------------------------------------------- //
	// Private Member Variables
	// --------------------------------------------------------------------- //


	// --------------------------------------------------------------------- //
	// Private Member Functions
	// --------------------------------------------------------------------- //


public:
	// --------------------------------------------------------------------- //
	// Accessors & Mutators
	// --------------------------------------------------------------------- //





};