#include <VLD/vld.h>

#include <Application/GameApp.h>
#include <Utils/Log.h>

int main()
{
	GameApp app;
	
	if (!app.Init())
	{
		app.Cleanup();
		return 1;
	}

	LOG(Boot, "Game is starting to execute");
	app.Run();
	app.Cleanup();

	return 0;
}