C.R.O.A.K Engine

Creative Repository Of Awesome Krafts

12 Oct 2019
- In the process of using XML to load Processes as well as Components.  Doing this allows me to make my Actors much
more dynamic, but at the cost of not being able to grab specific processes to place callbacks on certain events.  Perhaps
in the future I can store a list of processes in each Actor.
- Eventually I'd like to allow designers to write Lua code in the XML that would execute when a process received certain
events.
- I want to completely remove the AnimationComponent, as all it is doing is creating the AnimationProcess and passing
along the IGameLayer.  On the other hand keeping it means I can get the AnimationComponent of any Actor and then modify
the AnimationProcess manually.  If however I decide to implement the previous idea of GetProcess<> in Actor then
AnimationComponent is completely useless
- I need to add Squirrel3 RNG to this project
- The engine's random numbers should be based on a single seed that can be replicated across all platforms
- Each XML Element will have attributes that can be read
	- Those attributes (if not found) may be found in a Child Element named <Random>.  This will allow the code
	to implement both hand-tuned values and ranges that the designer might find appropriate
- Work back a bit and make sure that the previous XML files are valid.  Test Processes in multitude and in congruently.  It
seems like a lot of work but a simple error message is easier than debugging!

13 Oct 2019
- Got simple particles working, but there are quite a few serious flaws.  The first and foremost is that when testing 100
particles it means that my engine hits the disk 100 separate times.
- I need to rework my XML loading to allow random values to be passed in any time.  I think that means instead of relying
on attributes which are very easy to read, I'll need to have each important value be it's own element and then have a 
child element named Range or simply a value to read